/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.price;

import java.awt.Color;
import java.util.Date;

/**
 * It represents a electric period-based tariff.
 * It is used by the simulator to show the price of the energy during the simulation.
 * 
 *  @author Nuria Cuartero-Soler
 */
public interface Tariff {
	/**
	 * Returns the current time period
	 * @param date current date
	 * @return current period in the tariff
	 */
	public abstract String getPeriod(Date date);
	
	/**
	 * Returns the price of energy of the specified period
	 * @param period current period
	 * @return Price of the energy in the period, null if the period does not exist
	 */
	public abstract double getEnergyPrice(String period);
	
	/**
	 * Returns the chosen color to print the period in the simulation panel 
	 * @param period current period
	 * @return the color 
	 */
	public abstract Color getPeriodColor(String period);

	/**
	 * Fine to be applied when exporting energy. It may depend on the period
	 * 
	 * @return the costs in € per kWh
	 */
	public abstract double getFineForExporting(String period);
}
