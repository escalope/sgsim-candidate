/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.grid; 

import java.io.Serializable;

import org.w3c.dom.Node;

import mired.ucm.simulator.orders.Order;

/**
 * Class that represents an element (consumer, generator or battery) of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public abstract class ElementEMS implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/** Type*/	
	private TypesElement type; 
	
	/** Name */
	private String name; 	
	
	/** Cost */
	private double cost; 
	
	/** 0 (no control), 1 (on/off), 2 (set control) */
	private int controlType; 
	
	/** CT (transformation center) where the element belongs*/
	private String ct;
	
	/** Transformer where the element belongs*/
	private String transformer;
	
	/** Phases that the element is connected (ABCN)*/
	private String phases;
	
	/** Element disabled (true or false) */
	private boolean disabled;
	
	/** XML node that represents the element (only stored in case the element is disabled in order to be able to enable it again) */
	private Node xmlNode;
	
	/** Last order given to the element */
	private Order lastOrder;
	
	/** Maximum power supported (W)*/
	private double maxPower; 
	
	/** Current power (W)*/
	private double currentPower; 
	
	/** Tells if it is supplied by other transformer */
	private boolean bypassSupplied;
	
	/** Indicate to which other element is connected */
	private String connectedTo;
	
	/**
	 * Class constructor
	 * @param type
	 * @param control 0 (no control), 1 (on/off), 2 (set control)
	 * @param name
	 * @param phases ABCN
	 * @param nameCT
	 * @param nameTrafo
	 * @param pmax
	 * @param connectedTo
	 */
	ElementEMS(TypesElement type, int control, String name, String phases, String nameCT, String nameTrafo, double pmax, String connectedTo){
		maxPower = pmax;
		currentPower = maxPower; // By default
		this.type=type;
		controlType=control;
		this.name=name;
		this.phases=phases;
		ct=nameCT;
		transformer=nameTrafo;
		disabled=false;
		xmlNode=null;
		setLastOrder(null);
		bypassSupplied=false;
		this.connectedTo=connectedTo;
	}
	

	/**
	 * Gets the type of the element
	 * @return type
	 */
	public TypesElement getType() {
		return type;
	}

	/**
	 * Sets the type of the element
	 * @param type
	 */
	public void setType(TypesElement type) {
		this.type = type;
	}

	/**
	 * Gets the name of the element
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the element
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the control of the element
	 * @return 0 (no control), 1 (on/off), 2 (set control)
	 */
	public int getControlType(){
		return controlType;
	}

	/**
	 * Sets the control of the element
	 * @param control 0 (no control), 1 (on/off), 2 (set control)
	 */
	public void setControlType(int control) {
		this.controlType = control;
	}

	/**
	 * Gets the cost of the element
	 * @return cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * Sets the cost of the element
	 * @param cost
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	/**
	 * Gets the CT (transformation center) where the element belongs
	 * @return the CT name
	 */
	public String getCT() {
		return ct;
	}

	/**
	 * Sets the CT (transformation center) where the element belongs
	 * @param ct name of the CT
	 */
	public void setCT(String ct) {
		this.ct = ct;
	}

	/**
	 * Gets the transformer where the element belongs
	 * @return name of transformer
	 */
	public String getTransformer() {
		return transformer;
	}

	/**
	 * Sets the transformer where the element belongs
	 * @param transformer name of transformer
	 */
	public void setTransformer(String transformer) {
		this.transformer = transformer;
	}

	/**
	 * Gets the phases of the element
	 * @return phases
	 */
	public String getPhases() {
		return phases;
	}

	/**
	 * Sets the phases of the element
	 * @param phases ABCN
	 */
	public void setPhases(String phases) {
		this.phases = phases;
	}

	/**
	 * Returns if the element is disabled
	 * @return true if the element is disabled, false if not
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Sets if the element is disabled
	 * @param disabled true if the element is disabled, false if not
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * Gets the XML node that defines the element
	 * @return XMLNode
	 */
	public Node getXmlNode() {
		return xmlNode;
	}

	/**
	 * Sets the XML node that defines the element
	 * @param xmlNode
	 */
	public void setXmlNode(Node xmlNode) {
		this.xmlNode = xmlNode;
	}

	/**
	 * Gets the last order given to the element
	 * @return the last order
	 */
	public Order getLastOrder() {
		return lastOrder;
	}

	/**
	 * Sets the last order given to the element
	 * @param lastOrder
	 */
	public void setLastOrder(Order lastOrder) {
		this.lastOrder = lastOrder;
	}

	/**
	 * Gets the maximum power supported by the element
	 * @return maximum power in W
	 */
	public double getMaxPower() {
		return maxPower;
	}

	/**
	 * Sets the maximum power supported by the element
	 * @param maxPower maximum power in W
	 */
	public void setMaxPower(double maxPower) {
		this.maxPower = maxPower;
	}

	/**
	 * Gets the current power of the element
	 * @return current power in W
	 */
	public double getCurrentPower() {
		return currentPower;
	}

	/**
	 * Sets the current power of the element
	 * @param currentPower current power in W
	 */
	public void setCurrentPower(double currentPower) {
		this.currentPower = currentPower;
	}
	
	/**
	 * Returns if the element is supplied by other transformer
	 * @return true if the element is supplied by other transformer, false if not
	 */
	public boolean isBypassSupplied() {
		return bypassSupplied;
	}

	/**
	 * Sets if the element is supplied by other transformer
	 * @param bypassSupplied true if the element is supplied by other transformer, false if not
	 */
	public void setBypassSupplied(boolean bypassSupplied) {
		this.bypassSupplied = bypassSupplied;
	}

	/**
	 * Returns the element to which is connected
	 * @return the name of the element
	 */
	public String getConnectedTo() {
		return connectedTo;
	}

	/**
	 * Sets the element to which is connected
	 * @param connectedTo name of the element 
	 */
	public void setConnectedTo(String connectedTo) {
		this.connectedTo = connectedTo;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		boolean isEquals = true;
		
        try {
            final ElementEMS other = (ElementEMS)obj;
            
            if(!this.getName().equals(other.getName())) return false;
            if(!this.getType().equals(other.getType())) return false;
            if(this.getControlType()!=other.getControlType()) return false;
            if(!this.getCT().equals(other.getCT())) return false;
            if(!this.getTransformer().equals(other.getTransformer())) return false;
            if(!this.getPhases().equals(getPhases())) return false;

        } catch (Exception e) {
            isEquals = false;
        }

        return isEquals;
	}
	
}
