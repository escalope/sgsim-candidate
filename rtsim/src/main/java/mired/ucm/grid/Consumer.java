/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.grid;

/**
 * Class that represents a consumer of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public class Consumer extends ElementEMS{

	private static final long serialVersionUID = 1L;

	/**
	 * Class constructor
	 * @param type type of element
	 * @param name name of the element
	 * @param phases phases
	 * @param nameCT name of the CT which belongs to
	 * @param nameTrafo name of the transformer which belongs to
	 * @param control type of control (0 no control, 1 on/off control, 2 set control)
	 * @param pmax maximum power in W
	 * @param connectedTo element connected to
	 */
	public Consumer(TypesElement type, String name, String phases, String nameCT,String nameTrafo, int control, double pmax, String connectedTo){
		super(type,control,name,phases,nameCT,nameTrafo,pmax,connectedTo);
	}
	
	public String toString()
	{
		String s="* ELEMENT *\n";
		s=s+"Type: "+getType()+"\n" +
				"Name: "+getName()+"\n" +
				"Control: "+getControlType()+"\n" +
				"Current consumption: "+getCurrentPower()+"\n";
		return s;	
	}
}
