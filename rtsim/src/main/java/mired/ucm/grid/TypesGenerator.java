package mired.ucm.grid;

/**
 * Types of generators considered
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public enum TypesGenerator {
	SOLAR_GENERATOR,
	WIND_GENERATOR;
	
	/**
	 * Checks if the given String match any enum value name
	 * @param type the string to check
	 * @return true if the string matches, false if not
	 */
	public static boolean isAValidType(String type) {
        for (TypesGenerator generatorType : TypesGenerator.values()) {
            if (generatorType.toString().equals(type)){
                return true;
            }
        }
        return false;
    }
}
