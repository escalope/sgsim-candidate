/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package mired.ucm.grid;

/**
 * Class that represents a battery of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public class Battery extends ElementEMS{

	private static final long serialVersionUID = 1L;

	/** Max energy that can be stored */
	protected double maxEnergy; 
	
	/** Current stored energy */
	protected double energy; 
	
	/** Type of battery */
	protected TypesBattery batteryType;
	
	/** Response time of the battery (secs) */
	protected double responseTime;
	
	/** Lifetime: max number of cycles of the battery */
	protected int maxNumberOfCycles;
	
	/** Age: number of cycles already done by the battery */
	protected int currentCycles;
	
	/** Max % of power for charging */
	protected int maxChargePercentage;
	
	/** Min % of power for discharging */
	protected int minChargePercentage;
	
	/** Max temperature that the battery cannot increase */
	protected double maxTemperature;
	
	/** Current temperature of the battery */
	protected double temperature;
	
	/**
	 * Class constructor
	 * @param type type of element
	 * @param name name of the battery
	 * @param phases phases
	 * @param nameCT name of the CT which belongs to
	 * @param nameTrafo name of the transformer which belongs to
	 * @param control type of control (0 no control, 1 on/off control, 2 set control)
	 * @param emax maximum energy in Wh
	 * @param energy current energy in Wh
	 * @param batteryType type of battery
	 * @param pmax maximum power in W
	 * @param connectedTo element connected to
	 */
	public Battery(TypesElement type,String name, String phases, String nameCT, String nameTrafo, int control, double emax, 
			double energy,TypesBattery batteryType, double pmax, String connectedTo)
	{
		super(type,control,name,phases,nameCT,nameTrafo,pmax,connectedTo);
		maxEnergy=emax;
		this.energy=energy;		
		this.batteryType=batteryType;
		responseTime=0;
		maxNumberOfCycles=Integer.MAX_VALUE;
		currentCycles=0;
		maxChargePercentage=100;
		minChargePercentage=0;
		maxTemperature=45;
		temperature=0;
	}
	
	public double getMaxEnergy() {
		return maxEnergy;
	}

	public void setMaxEnergy(double e_Max) {
		maxEnergy = e_Max;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public TypesBattery getBatteryType() {
		return batteryType;
	}

	public void setBatteryType(TypesBattery type) {
		this.batteryType = type;
	}

	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	public int getMaxNumberOfCycles() {
		return maxNumberOfCycles;
	}

	public void setMaxNumberOfCycles(int maxNumberOfCycles) {
		this.maxNumberOfCycles = maxNumberOfCycles;
	}

	public int getCurrentCycles() {
		return currentCycles;
	}

	public void setCurrentCycles(int currentCycles) {
		this.currentCycles = currentCycles;
	}

	public int getMaxChargePercentage() {
		return maxChargePercentage;
	}

	public void setMaxChargePercentage(int maxChargePercentage) {
		this.maxChargePercentage = maxChargePercentage;
	}

	public int getMinChargePercentage() {
		return minChargePercentage;
	}

	public void setMinChargePercentage(int minChargePercentage) {
		this.minChargePercentage = minChargePercentage;
	}

	public double getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(double maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public String toString(){
		String s="* ELEMENT *\n";
		s=s+"Type: "+getType()+"\n" +
				"Name: "+getName()+"\n" +
				"Control: "+getControlType()+"\n" +
				"Capacity: "+maxEnergy+"\n" +
				"Current Energy: "+energy+"\nP_Max: "+getMaxPower()+"\n" +
				"Battery Type: "+getBatteryType()+"\n";
		return s;	
	}
	
}
