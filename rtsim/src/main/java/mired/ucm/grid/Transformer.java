/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.grid;

import org.w3c.dom.Node;

/**
 * Class that represents a transformer of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public class Transformer {
	/** Name*/
	private String name;
	
	/** CT (transformation center) to which it belongs */
	private String ct;
	
	/** Is enabled (true) or disabled (false)*/
	private boolean enabled;
	
	/**
	 *XML node that represents the element 
	 *(only stored in case the element is disabled in order to be able to enable it again) 
	 */
	private Node xmlNode;   
							
	/**
	 * Class constructor
	 * @param name
	 * @param ct transformation center which belongs to
	 */
	public Transformer(String name, String ct) {
		this.name=name;
		this.ct=ct;
		this.enabled=true;
		this.xmlNode=null;
	}
	
	/**
	 * Class constructor
	 * @param name
	 * @param ct transformation center which belongs to
	 * @param enabled if enabled by default (true) or not (false)
	 */
	public Transformer(String name, String ct, boolean enabled) {
		this.name=name;
		this.ct=ct;
		this.enabled=enabled;
		this.xmlNode=null;
	}

	/**
	 * Returns the name
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the CT which the transformer belongs
	 * @return the name of the CT
	 */
	public String getCt() {
		return ct;
	}

	/**
	 * Sets the CT which the transformer belongs
	 * @param ct
	 */
	public void setCt(String ct) {
		this.ct = ct;
	}

	/**
	 * Returns if the transformer is enabled
	 * @return true if the transformer is enabled, false if not
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets if the transformer is enabled
	 * @param available true if the transformer is enabled, false if not
	 */
	public void setEnabled(boolean available) {
		this.enabled = available;
	}

	/**
	 * Returns the XML node which defines the transformer
	 * @return the XML node
	 */
	public Node getXmlNode() {
		return xmlNode;
	}

	/**
	 * Sets the XML node which defines the transformer
	 * @param xmlNode
	 */
	public void setXmlNode(Node xmlNode) {
		this.xmlNode = xmlNode;
	}
}
