/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.grid;

/**
 * Class that represents generator of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public class Generator extends ElementEMS{
	
	private static final long serialVersionUID = 1L;

	/** Subtype: SOLAR, WIND*/
	private TypesGenerator subType; 

	/** Element is activated by default (true) or not (false) */
	private boolean activated; 
	
	/**
	 * Class constructor
	 * @param type
	 * @param name
	 * @param phases
	 * @param nameCT
	 * @param nameTrafo
	 * @param control
	 * @param subtype
	 * @param pmax
	 * @param connectedTo
	 * @param active
	 */
	public Generator(TypesElement type, String name, String phases, String nameCT, String nameTrafo,int control, 
			TypesGenerator subtype,double pmax, String connectedTo,boolean active){
		super(type,control,name,phases,nameCT,nameTrafo,pmax,connectedTo);		
		subType=subtype;
		setActivated(active);
	}
	
	public String toString()
	{
		String s="* ELEMENT *\n";
		s=s+"Type: "+getType()+"\n" +
				"Name: "+getName()+"\n" +
				"Control: "+getControlType()+"\n" +
				"Generation: "+getCurrentPower()+"\n"+"Maximum generation: "+getMaxPower()+"\n";
		return s;	
	}
	
	
	/**
	 * Returns the type of generator (wind, solar, etc.)
	 * @return the type
	 */
	public TypesGenerator getSubType(){
		return subType;
	}

	/**
	 * Get if the element is activated (true) or not (false)
	 * @return activated (true) or not (false)
	 */
	public boolean isActivated() {
		return activated;
	}

	/**
	 *  Set if the element is activated (true) or not (false)
	 * @param activated (true) or not (false)
	 */
	public void setActivated(boolean active) {
		this.activated = active;
	}
}
