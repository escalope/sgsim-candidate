/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.monitor;

import java.util.Date;

import mired.ucm.simulator.orders.Action;

/**
 * Class that represents an event produced during the simulation
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 *
 */
public abstract class Event extends Action{

	private static final long serialVersionUID = 1L;
	protected boolean enable;
	
	/**
	 * Class constructor
	 * @param enable if the events enable (true) or disable (false)
	 * @param deviceName
	 * @param timestamp
	 */
	public Event(boolean enable, String deviceName, Date timestamp){
		super(deviceName, timestamp);
		this.enable=enable;
	}

	/**
	 * Get if the element is enabled
	 * @return true if enabled, false if disabled
	 */
	public boolean isEnabled() {
		return enable;
	}

	/**
	 * Change state of the event
	 * @param enabled true for enabling, false for disabling
	 */
	public void setAvaliable(boolean enabled) {
		this.enable = enabled;
	}
	
	@Override
	public String toString() {
		String message;
		if(isEnabled()){
			message="Enable "+getDeviceName()+" at "+getTimestamp();
		}else{
			message="Disable "+getDeviceName()+" at "+getTimestamp();
		}
		return message;
	}
	
}
