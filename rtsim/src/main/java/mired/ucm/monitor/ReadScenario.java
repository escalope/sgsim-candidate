/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.monitor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.grid.TypesElement;
import mired.ucm.grid.TypesGenerator;
import mired.ucm.properties.PATHS;

/**
 * Class to read the scenario of consumption, sun and wind for 24 hours simulation. Each parameter is specified in percentage (%).
 * It is possible to specify different values for different times of the day. 
 *
 *@author Nuria Cuartero-Soler
 *@author Sandra Garcia-Rodriguez
 */
public class ReadScenario {
	
	private int[] counter;
	
	private static final int CONSUMPTION=0;
	private static final int SUN=1;
	private static final int WIND=2;
	
	private double[][] consumption_sun_wind;
	private Date[] hours;
	private Date initialClock; // Clock at the beginning of the simulation
	
	/**
	 * Class constructor
	 * @param simtime Initial clock at the beginning of the simulation
	 * @param paths PATHS
	 */
	public ReadScenario(Date simtime, PATHS paths){		
		initialClock = simtime;
		readScenario(paths);
		counter = new int[3];
		counter[CONSUMPTION]=0;
		counter[SUN]=0;
		counter[WIND]=0;
	}


	/**
	 * Reads the csv file that contains the scenario to simulate
	 * @param paths PATHS
	 */
	private void readScenario(PATHS paths){
		try{
			BufferedReader csvBf = new BufferedReader(new FileReader(paths.getScenarioFile())); // Loads the csv file	
			Vector<String> lines = new Vector<String>();
			String line;
			while(csvBf.ready()) { // Reading csv content
				line=csvBf.readLine();
				if(line.length()>0 && line.charAt(0)!='#'){ // Discard comments
					lines.add(line);
				}
			}
			csvBf.close(); // Closes file
			
			if(lines.size()>0){ // If data read
				consumption_sun_wind=new double[lines.size()][3];
				hours = new Date[lines.size()];
				String[] tokens,tokensHour;
				
				Calendar calendar = new GregorianCalendar();
				for(int i=0; i<consumption_sun_wind.length;i++){
					tokens = (lines.get(i)).split(";");
					for(int j=0;j<consumption_sun_wind[i].length;j++){ // Getting values
						consumption_sun_wind[i][j]=Double.parseDouble(tokens[j+1])/100; // +1 because hours are not stored	
					}				
					calendar.setTime(initialClock);
					tokensHour = tokens[0].split(":"); // Getting time
					calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(tokensHour[0]) );
					calendar.set(Calendar.MINUTE, Integer.parseInt(tokensHour[1]) );
					calendar.set(Calendar.SECOND, Integer.parseInt(tokensHour[2]) );
					hours[i]=calendar.getTime(); // Saving time				
				}
			}else{ // if no data provided, default data
				System.err.println("WARNING: No data for scenario provided");
				consumption_sun_wind=new double[1][3];
				consumption_sun_wind[0][CONSUMPTION]=0;
				consumption_sun_wind[0][SUN]=0;
				consumption_sun_wind[0][WIND]=0;
			}
		
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the power consumption (in kW) of the non-controllable consumers in the net for the next period
	 * @param elementos The elements of the network
	 * @return Power consumed in the next hour (in kW)
	 */
	public double getNextConsumption(Collection<ElementEMS> elementos){
		double consumo=0;

		for(ElementEMS element:elementos){
			if(element.getType() == TypesElement.CONSUMER && element.getControlType()==0){ // If there are consumers and no controllable
				consumo+= element.getMaxPower()*consumption_sun_wind[counter[CONSUMPTION]][CONSUMPTION];
			}
		}
		counter[CONSUMPTION]++;
		counter[CONSUMPTION]=counter[CONSUMPTION]%consumption_sun_wind.length;
		return consumo/1000;
	}
	
	/**
	 * Get the % of sun in the next hour
	 * @return % of sun
	 */
	public double getNextSun(){
		double sol=consumption_sun_wind[counter[SUN]][SUN];
		counter[SUN]++;
		counter[SUN]=counter[SUN]%consumption_sun_wind.length;
		return sol*100; //para darselo en porcentaje
	}
	
	/**
	 * Get the % of wind in the next hour
	 * @return % of wind
	 */
	public double getNextWind(){
		double viento=consumption_sun_wind[counter[WIND]][WIND];
		counter[WIND]++;
		counter[WIND]=counter[WIND]%consumption_sun_wind.length;
		return viento*100; //para darselo en porcentaje;
	}

	/**
	 * Gets the real consumption/generation (in W) of the specific element at this specific time, regarding the scenario simulated.
	 * @param element the element
	 * @param clock current simulation time
	 * @return W consumed/generated by the element
	 */
	public double getGenerationOrLoad(ElementEMS element, Date clock){
		
		double demanda=0;
		int posicion;
		
		posicion = findPosition(clock);
		
		if(element.getType() == TypesElement.CONSUMER){ // If consumer
			demanda+= element.getMaxPower()*consumption_sun_wind[posicion][CONSUMPTION];	
		}else if(element.getType() == TypesElement.GENERATOR){ // If generator
			if( ((Generator)element).getSubType().equals(TypesGenerator.SOLAR_GENERATOR)){
				demanda+= element.getMaxPower()*consumption_sun_wind[posicion][SUN];
			}else if( ((Generator)element).getSubType().equals(TypesGenerator.WIND_GENERATOR)){
				demanda+= element.getMaxPower()*consumption_sun_wind[posicion][WIND];
			}
		}
			
		return demanda;
	}
	
	
	/**
	 * Gets the power consumption (in kW) of the non-controllable consumers in the net for the specified time
	 * @param elementos the elements of the network
	 * @param clock current simulation time
	 * @return power consumed (in kW)
	 */
	public double getConsumption(Collection<ElementEMS> elementos, Date clock){
		int posicion = findPosition(clock);			
		double consumo=0;

		for(ElementEMS element:elementos){
			if(element.getType() == TypesElement.CONSUMER && element.getControlType()==0){ // If there are consumers and no controllable
				consumo+= element.getMaxPower()*consumption_sun_wind[posicion][CONSUMPTION];
			}
		}

		return consumo/1000;
	}

	/**
	 * Gets the % of consumption for the specified time, regarding the scenario simulated.
	 * @param clock simulation time
	 * @return the % of consumption
	 */
	public double getConsumptionPercentage(Date clock){
		int posicion = findPosition(clock);		
		return consumption_sun_wind[posicion][CONSUMPTION]*100;
	}
	
	/**
	 * Gets the % of sun for the specified time, regarding the scenario simulated.
	 * @param clock simulation time
	 * @return the % of sun
	 */
	public double getSunPercentage(Date clock){
		int posicion = findPosition(clock);		
		return consumption_sun_wind[posicion][SUN]*100;
	}
	
	/**
	 * Gets the % of wind for the specified time, regarding the scenario simulated.
	 * @param clock simulation time
	 * @return the % of wind
	 */
	public double getWindPercentage(Date clock){
		int posicion = findPosition(clock);		
		return consumption_sun_wind[posicion][WIND]*100;
	}
	
	/**
	 * Gets the position according to the specific time
	 * @param clock simulation time
	 * @return position of the consumption_sun_wind array corresponding to this clock
	 */
	private int findPosition(Date clock){		
		int pos=-1;
		if(hours!=null && hours.length > 0){		
			for(int i=0;i<hours.length-1 && pos==-1;i++){
				// Checks if a new day has started with respect to the initial clock
				Calendar cal1 = Calendar.getInstance();
				Calendar cal2 = Calendar.getInstance();
				cal1.setTime(clock); // Current clock
				cal2.setTime(hours[i]);
				if(cal1.get(Calendar.DAY_OF_MONTH)!=cal2.get(Calendar.DAY_OF_MONTH)){ // If day of the month is different in both dates
					cal1.set(Calendar.DAY_OF_MONTH, cal2.get(Calendar.DAY_OF_MONTH)); // The day is updated in current clock in order to compare dates (hours) correctly
					clock=cal1.getTime();
				}
				
				// Looking for the position corresponding to the current hour
				if(hours[i].equals(clock) || (hours[i].before(clock) && hours[i+1].after(clock)) ){
					pos = i;
				}
			}
			
			// If no position found, getting last position
			if(pos == -1 && (hours[hours.length-1].equals(clock) || hours[hours.length-1].before(clock))){
				pos = hours.length-1;
			}
		}
		
		if(pos==-1){ // If no scenario provided
			pos=0;
		}
		
		return pos;
	}
	
}
