/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.monitor;

import java.util.Date;

/**
 * Class that represents an element fall event produced during the simulation
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 */
public class EventElementFalls extends Event {

	private static final long serialVersionUID = -6653234685281044164L;

	/**
	 * Class constructor
	 * @param available
	 * @param nameElement
	 * @param timestamp
	 */
	public EventElementFalls(boolean available, String nameElement,
			Date timestamp) {
		super(available, nameElement, timestamp);
	}

}
