/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.properties;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;


/**
 * Class that creates a temporary folder for every simulation run.
 * It creates all necessary files for each simulation.
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 */
public class PATHS implements Serializable{

	private static final long serialVersionUID = 1L;
	protected static final String DEFAULT_TEMP_FOLDER = "target/processors";
	protected static final String SUBSTATION_METER_FILE = "substationMeter.csv";
	protected static final String SUBSTATION_METER_NAME = "substationMeter";
	protected static final String LINE_LOSSES_FILE_NAME = "UGLine_Losses.csv";
	protected static final String TRANSFORMATION_LOSSES_FILE_NAME = "Trans_Losses.csv";
	protected static final String NET_FILE_NAME = "network.glm";
	public static boolean debugMode=true;
	protected String gridLabVersion;
	protected String gridLabPath;
	protected String netXmlFile;
	protected String scenarioFile;
	protected String currentRootNetFolder;
	protected String configurationFileName;
	
	/**
	 * Constructor of the class
	 * @param configurationFile Path of the glm file with the configuration of the network to simulate
	 * @param netXmlFile Path of the XML file which defines the network to simulate
	 * @param scenarioFile Path of the scenario file to feed the simulator
	 * @param gridLabPath Path of the folder which contains GridLAB-D
	 * @throws IOException
	 */
	public PATHS(String configurationFile, String netXmlFile, String scenarioFile, String gridLabPath) throws IOException{
		chooseGridLabVersion(); // Chooses the correct GridLAB-D version to execute
		File tempFolder = createTempDirAndCopyNetFolderContent(configurationFile); // Creates a temporary directory and copy the configuration file inside
		this.currentRootNetFolder=tempFolder.getAbsolutePath(); 
		this.netXmlFile = netXmlFile;
		this.scenarioFile=scenarioFile;
		this.gridLabPath=gridLabPath;
	}
	
	/**
	 * Chooses the correct version of GridLAB-D depending on the system architecture
	 */
	private void chooseGridLabVersion(){
		if(System.getProperty("os.arch").contains("64")){
			gridLabVersion = "gridlab3.0/bin/gridlabd ";
		}else{
			gridLabVersion = "gridlabd32/gridlabd ";
		}
	}
	
	/**
	 * Create a temporary directory where the needed files are copied
	 * @param configurationFile File to copy in the new directory
	 * @return the new file
	 * @throws IOException
	 */
	private synchronized File createTempDirAndCopyNetFolderContent(String configurationFile) throws IOException {
		File targetFolder=new File(DEFAULT_TEMP_FOLDER); // Default folder to create temporary files
		if (!targetFolder.exists()){ 			
			targetFolder.mkdirs(); // Creates the folder if does not exist
		}
		final File tempDirName=File.createTempFile("tempcomp", "net", new File(DEFAULT_TEMP_FOLDER)); // Creating net folder
		String path=tempDirName.getAbsolutePath()+"dir";
		final File tempDir=new File(path);
		tempDir.mkdir();
		tempDirName.delete();
		File confFile = new File(configurationFile);
		configurationFileName=confFile.getName(); // Saving configuration file name
		FileUtils.copyFileToDirectory(confFile, tempDir); // Copy configuration file inside
		return tempDir;
	}
	
	/**
	 * Returns the bin file to execute GridLAB-D
	 * @return the path
	 */
	public String getGridLab(){
		return gridLabPath+gridLabVersion;
	}
	
	/**
	 * Returns the folder which contains GridLAB-D
	 * @return the path
	 */
	public String getGridLabPath(){
		if (gridLabVersion.indexOf("bin")>=0)
		return gridLabPath+gridLabVersion.substring(0,gridLabVersion.indexOf("bin"));
		else
		return gridLabPath+gridLabVersion.substring(0,gridLabVersion.indexOf("gridlabd"));
	}
	
	/**
	 * Returns the path of the folder which contains the network simulated in GridLAB-D
	 * @return the path
	 */
	public String getNetFolder(){
		return currentRootNetFolder+"/net/";
	}
	
	/**
	 * Returns the path of the temporary folder created ./rtsim/src/main/java/mired/ucm/properties/PATHS.java
to run the simulation
	 * @return the path
	 */
	public String getRootNetFolder(){
		return currentRootNetFolder;
	}
	
	/**
	 * Returns the path of the glm file with the network model
	 * @return the path
	 */
	public String getGlmModel(){
		return  getNetFolder()+NET_FILE_NAME;
	}
	
	/**
	 * Returns the path of the folder which contains the output files generated by GridLAB-D
	 * @return the path
	 */
	public String getOutputs(){
		return getNetFolder()+"outputs/";
	}

	

	/**
	 * Returns the path of the line losses file generated by GridLAB-D
	 * @return the path
	 */
	public String getLinesLosses(){
		return getOutputs()+LINE_LOSSES_FILE_NAME;
	}
	
	/**
	 * Returns the path of the transformer losses file generated by GridLAB-D
	 * @return the path
	 */
	public String getTransLosses(){
		return getOutputs()+TRANSFORMATION_LOSSES_FILE_NAME;
	}
	
	/**
	 * Returns the path of the configuration file of the network
	 * @return the path
	 */
	public String getConfiguration(){
		return currentRootNetFolder+"/"+configurationFileName;
	}
	
	/**
	 * Returns the path of the substation meter file
	 * @return the path
	 */
	public String getSubstationMeterFile(){
		return  getOutputs()+SUBSTATION_METER_FILE;
	}
	
	/**
	 * Returns the name of the substation meter file
	 * @return the name (without the extension)
	 */
	public String getSubstationMeterName(){
		return  SUBSTATION_METER_NAME;
	}

	/**
	 * Returns the path folder where the players files are 
	 * @return the path
	 */
	public String getPlayers(){

		return getNetFolder()+"players/";
	}
	
	/**
	 * Returns the path of the temporary XML file created for the simulation  
	 * @return the path
	 */
	public String getXmlTemp(){
		return currentRootNetFolder+"/xml/temp.xml";
	}
	
	/**
	 * Returns the path of the folder which contains the tempora./rtsim/src/main/java/mired/ucm/properties/PATHS.java
ry XML file created for the simulation 
	 * @return the path
	 */
	public String getXmlTempPath(){
		return currentRootNetFolder+"/xml/";
	}
	
	/**
	 * Returns the name of the the temporary XML file created for the simulation 
	 * @return XML file name
	 */
	public String getXmlTempFileName(){
		return "temp";
	}
	
	/**
	 * Return the path of the network XML source file
	 * @return the path
	 */
	public String getNetXmlSrcFile() {
		return netXmlFile;
	}

	/**
	 * Set the path of the network XML source file
	 * @param networkXmlSourceFile Path of the XML file which defines the network
	 */
	public void setNetXmlSrcFile(String networkXmlSourceFile) {
		this.netXmlFile = networkXmlSourceFile;
	}

	/**
	 * Returns the path of the scenario file
	 * @return the path
	 */
	public String getScenarioFile() {
		return scenarioFile;
	}

	/**
	 * Set the path of the scenario file
	 * @param scenarioFile The path
	 */
	public void setScenarioFile(String scenarioFile) {
		this.scenarioFile = scenarioFile;
	}

	/**
	 * Remove the temporary files created for the simulation
	 */
	public void removeTempFiles() {
		try {
			FileUtils.deleteDirectory(new File(this.currentRootNetFolder));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
