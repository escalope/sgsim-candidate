/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class to read a properties file
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class PropertiesReader {
	
	private Properties properties;
	
	/**
	 * Constructor of the class
	 * @param file path of the properties file
	 */
	public PropertiesReader(String file){
		properties = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(file);
			properties.load(in);
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor of the class
	 * @param inStream the input stream
	 */
	public PropertiesReader(InputStream inStream){
		properties = new Properties();
		try {
			properties.load(inStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the property
	 * @param property
	 * @return
	 */
	public String getProperty(String property){
		String prop = properties.getProperty(property);
		try{
			if(prop==null)
				throw new PropertyNotFoundException("Property "+property+" not found");
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
		
		return prop;
			
	}
	
	public class PropertyNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

		public PropertyNotFoundException() {
			super();
		}

		public PropertyNotFoundException(String message, Throwable cause) {
			super(message, cause);
		}

		public PropertyNotFoundException(String message) {
			super(message);
		}

		public PropertyNotFoundException(Throwable cause) {
			super(cause);
		}

	}

}
