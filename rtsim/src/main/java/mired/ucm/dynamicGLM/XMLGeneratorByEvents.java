/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.dynamicGLM;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import mired.ucm.properties.PATHS;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class that generates a XML file which represents a new grid structure.
 * Changes in grid structure are notifying by events.
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class XMLGeneratorByEvents {
	
	/** The xml document */
	private Document xmlDocument;
	
	/** The xml source file	*/
	private String xmlSource;
	
	/** Path where to create the new xml file	*/
	private String pathForNewXmlFiles;
	
	/** Name of the new xml file (without extension) */
	private String nameForNewXmlFiles;
	
	/**
	 * Constructor of the class
	 * @param paths PATHS variable to get the paths of the files
	 */
	public XMLGeneratorByEvents(PATHS paths) {
		this.xmlSource=paths.getXmlTemp();
		this.pathForNewXmlFiles =  paths.getXmlTempPath();
		this.nameForNewXmlFiles = paths.getXmlTempFileName();
		this.xmlDocument = null;
	}
	
	/**
	 * Constructor of the class
	 * @param xmlSourceFile the xml source file
	 * @param pathForNewFiles path where create the new xml file
	 * @param newFileName name of the new xml file (without xml extension)
	 */
	public XMLGeneratorByEvents(String xmlSourceFile, String pathForNewFiles, String newFileName) {
		this.xmlSource=xmlSourceFile;
		this.pathForNewXmlFiles = pathForNewFiles;
		this.nameForNewXmlFiles = newFileName;
		this.xmlDocument = null;
	}
	
	/**
	 * Simulates a power cut in MT and removes all the CTs implicated
	 * @param cts the names of the CTs to remove
	 * @return true if success, false in other case
	 */
	public boolean cutMtEvent(String[] cts) {
		boolean completed=true;
		Node ctRemoved=null;
		
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			
			for(String ctName:cts) {
				Element ct = getCtElement(ctName);
				if(ct==null){
					throw new ParserConfigurationException("Error: "+ctName+" does not exist");
				}
				
				ctRemoved=removeCt(ct.getParentNode(),ct,ctName);
				if(ctRemoved==null) {
					completed=false;
				}
			}
			
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
		} catch (ParserConfigurationException e) {
			completed=false;
			e.printStackTrace();
		} catch (SAXException e) {
			completed=false;
			e.printStackTrace();
		} catch (IOException e) {
			completed=false;
			e.printStackTrace();
		}
		
		return completed;
	}
	
	
	/**
	 * Removes a transformer from the XML Document
	 * @param transformerName name of the transformer to remove
	 * @return true if success, false in other case
	 */
	public Node disableTransformerEvent(String transformerName) {
		Node disabled=null;
		
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			Element trafo = getTransformerElement(transformerName);
			disabled=XMLUtilities.removeElement(trafo.getParentNode(),trafo);
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return disabled;
	}
	
	/**
	 * Enables a disabled transformer restoring its XML node
	 * @param transformerNode The XML node to restore
	 * @param parentCtName The name of the CT which the transformer belongs
	 * @return the restored node, or null if the node has not been restored
	 */
	public Node enableTransformerEvent(Node transformerNode, String parentCtName){
		Node addedNode=null;
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			
			Element ct = this.getCtElement(parentCtName);
			NodeList components=ct.getElementsByTagName("elements");
			Element transformers = (Element) components.item(0);
			if(transformers==null) {
				throw new ParserConfigurationException();
			}
			
			Node newNode=xmlDocument.importNode(transformerNode, true); // Creates a copy of the node
			addedNode=transformers.appendChild(newNode); // Adds the node in the document
			
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return addedNode;
	}
	
	/**
	 * Removes a component from the xml Document
	 * @param elementName element to remove
	 * @param parentTransformerName parent transformer 
	 * @return the node removed if success, null in other case
	 */
	public Node disableElementEvent(String elementName, String parentTransformerName) {
		
		boolean elementFound=false;
		Node elementRemoved=null;
		Element component=null;
		
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			Element trafo = getTransformerElement(parentTransformerName);
			NodeList components=trafo.getElementsByTagName("components");
			component = (Element) components.item(0);
			if(component==null) {
				throw new ParserConfigurationException("Transformer "+parentTransformerName+" does not have elements");
			}
			
			NodeList elements = component.getChildNodes();
			for(int i=0; i<elements.getLength() && !elementFound;i++) {
				Node elementNode = elements.item(i);
				if (elementNode.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element)elementNode;
					if(XMLUtilities.compareProperty(element, "name", elementName)) {
						elementFound=true;
						elementRemoved=XMLUtilities.removeElement(element.getParentNode(), element);
					}
				}
			}
			
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
			
		} catch (ParserConfigurationException e) {
			elementRemoved=null;
			e.printStackTrace();
		} catch (SAXException e) {
			elementRemoved=null;
			e.printStackTrace();
		} catch (IOException e) {
			elementRemoved=null;
			e.printStackTrace();
		}
		
		return elementRemoved;
		
	}
	
	/**
	 * Enable an element adding the corresponding node to the document
	 * @param elementNode node to add
	 */
	public Node enableElementEvent(Node elementNode, String parentTransformerName){
		Node addedNode=null;
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			Element trafo = getTransformerElement(parentTransformerName);
			NodeList components=trafo.getElementsByTagName("components");
			Element component = (Element) components.item(0);
			if(component==null) {
				throw new ParserConfigurationException();
			}
			
			Node newNode=xmlDocument.importNode(elementNode, true); // Creates a copy of the node
			addedNode=component.appendChild(newNode); // Adds the node in the document
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return addedNode;
	}
	
	/**
	 * Updates the Energy value in a XML battery node
	 * @param node node to update
	 * @param energy the new value of energy
	 */
	public void updateBatteryNode(Node node, String energy){
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element elementNode = (Element)node;
			NodeList list=elementNode.getElementsByTagName("Energy");
			for(int i=0; i<list.getLength();i++) {
				Node energyNode = list.item(i);
				energyNode.setTextContent(energy);
			}
		}
	}
	
	/**
	 * Transfers the elements of one transformer to another
	 * @param transformerFrom Name of the origin transformer
	 * @param transformerTo Name of the destination transformer
	 */
	public void transferElementsToAnotherTransformer(String transformerFrom, String transformerTo) {
		Element transFrom=null, transTo=null;

		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			transFrom=getTransformerElement(transformerFrom);
			transTo=getTransformerElement(transformerTo);
			
			Element componentsTransFrom = (Element)transFrom.getElementsByTagName("components").item(0); // Getting the "components" node of the origin transformer
			Element componentsTransTo = (Element)transTo.getElementsByTagName("components").item(0); // Getting the "components" node of the destination transformer
			
			NodeList elementsTransFrom=componentsTransFrom.getChildNodes(); // Getting the elements of the origin transformer
			
			// Adding elements from one transformer to another
			for(int i=0; i<elementsTransFrom.getLength(); i++) {
				Node node = elementsTransFrom.item(i); // Gets the node
				Node newNode=xmlDocument.importNode(node, true); // Creates a copy
				Node appendedNode=componentsTransTo.appendChild(newNode); // Adds the node to the destination transformer
				
				// Mark the node as transferred
				if(appendedNode.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) appendedNode;
					Element transferredElement = xmlDocument.createElement("transferred");
					transferredElement.appendChild(this.xmlDocument.createTextNode("transferred"));
					e.appendChild(transferredElement);
				}
			}
			
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles); // Write document in the xml file
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Remove the transferred elements of a transformer
	 * @param transformerName The name of the transformer to remove the elements 
	 */
	public void removeTransferredElements(String transformerName){
		Element transformer=null;
		
		try {
			xmlDocument=XMLUtilities.loadXMLDoc(xmlSource);
			transformer=getTransformerElement(transformerName);
			Element componentsTrans = (Element)transformer.getElementsByTagName("components").item(0); // Getting the "components" of the transformer
			NodeList elementsTrans=componentsTrans.getChildNodes(); // Getting the elements of the origin transformer
			
			// Looking for transferred elements
			for(int i=0; i<elementsTrans.getLength(); i++) {
				Node node = elementsTrans.item(i); // Gets the node
				
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element eNode = (Element) node;
					Element transferredElement = (Element)eNode.getElementsByTagName("transferred").item(0);
					if(transferredElement!=null){
						XMLUtilities.removeElement(node.getParentNode(), node); // If the element has the tag "transferred", remove
					}
				}
			}
			
			XMLUtilities.createXmlFile(xmlDocument, pathForNewXmlFiles, nameForNewXmlFiles);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets a transformer element from the Document
	 * @param transformerName The name of the transformer
	 * @return The transformer element if found, null in other case
	 */
	private Element getTransformerElement(String transformerName){

		Element eTransformer = null;
		NodeList transformers = xmlDocument.getElementsByTagName("transformer"); //Getting a NodeList of transformers
		
		for (int i = 0; i < transformers.getLength(); i++) {  //For every transformer node
			Node trafo = transformers.item(i);
			if (trafo.getNodeType() == Node.ELEMENT_NODE) {
				eTransformer = (Element) trafo;
				if(XMLUtilities.compareProperty(eTransformer,"name",transformerName)) //If it is the transformer I am looking for, return
					return eTransformer;
			}
		}
		
		return null;
	}
	
	/**
	 * Gets a CT element from the Document
	 * @param ctName the CT name
	 * @return The CT element if found, null in other case
	 */
	private Element getCtElement(String ctName){

		Element eCt = null;
		NodeList cts = xmlDocument.getElementsByTagName("gridnode"); //Getting a NodeList of CTs
		
		for (int i = 0; i < cts.getLength(); i++) {  //For every CT node
			Node ct = cts.item(i);
			if (ct.getNodeType() == Node.ELEMENT_NODE) {
				eCt = (Element) ct;
				if(XMLUtilities.compareProperty(eCt,"name",ctName)) {//If it is the CT I am looking for, return
					return eCt;
				}
			}
		}

		return null;
	}
	
	/**
	 * Removes a CT of its parent node and the connections associated with it
	 * @param parent
	 * @param childCt
	 * @return true if CT and its connections are removed, false in other case
	 */
	private Node removeCt(Node parent, Node childCt, String ctName) {
		Node ctRemoved=null;
		ctRemoved=XMLUtilities.removeElement(parent, childCt); // Removes the CT
		
		if(parent.getNodeType() == Node.ELEMENT_NODE) {
			Element eParent = (Element) parent;
			NodeList connections = eParent.getElementsByTagName("connection"); // Getting the connections of the grid
			
			for(int i=0; i<connections.getLength(); i++) { // For every
				Element connection = (Element) connections.item(i);
				String cta = connection.getElementsByTagName("nodea").item(0).getTextContent(); // Getting first node of the connection
				String ctb= connection.getElementsByTagName("nodeb").item(0).getTextContent(); // Getting second node of the connection
				
				if(cta.equals(ctName) || ctb.equals(ctName)) {// If the connection implicates the CT removed, the connection is removed too
					Node connectionRemoved=XMLUtilities.removeElement(parent,connection);
					if(connectionRemoved==null) {
						ctRemoved=null; // If connections are no removed, the action is not completed
					}
				}
			}
		}
		
		return ctRemoved;
	}

	/**
	 * Gets the path for new xml files.
	 *
	 * @return the path for new xml files
	 */
	public String getPathForNewXmlFiles() {
		return pathForNewXmlFiles;
	}

	/**
	 * Sets the path for new xml files.
	 *
	 * @param pathForNewXmlFiles the path for new xml files
	 */
	public void setPathForNewXmlFiles(String pathForNewXmlFiles) {
		this.pathForNewXmlFiles = pathForNewXmlFiles;
	}

	/**
	 * Gets the name for new xml files.
	 *
	 * @return the name for new xml files
	 */
	public String getNameForNewXmlFiles() {
		return nameForNewXmlFiles;
	}

	/**
	 * Sets the name for new xml files.
	 *
	 * @param nameForNewXmlFiles the name for new xml files
	 */
	public void setNameForNewXmlFiles(String nameForNewXmlFiles) {
		this.nameForNewXmlFiles = nameForNewXmlFiles;
	}

}
