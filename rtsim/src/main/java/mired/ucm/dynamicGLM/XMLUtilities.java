/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.dynamicGLM;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import mired.ucm.properties.PATHS;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Class which contains basic utilities to manage XML documents
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class XMLUtilities {
	
	/**
	 * Loads a XML file in a Document
	 *
	 * @param xmlFile the xml file
	 * @param document the xml Document
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws SAXException the sAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @return the loaded document
	 */
	public static Document loadXMLDoc(String xmlFile) throws ParserConfigurationException, SAXException, IOException{
		Document document=null;
		File fXmlFile = new File(xmlFile);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		document = dBuilder.parse(fXmlFile);
		document.getDocumentElement().normalize(); //optional, but recommended
		return document;
	}
	
	/**
	 * Compares if the property of an XML element is equal to the given value
	 * @param element
	 * @param property
	 * @param value
	 * @return true if the property is equal, false in other case
	 */
	public static boolean compareProperty(Element element, String property, String value) {
		return element.getElementsByTagName(property).item(0).getTextContent().trim().equals(value);
	}
	
	/**
	 * Removes a XML node of its parent node
	 * @param parent
	 * @param child
	 * @return The node removed if success, null in other case
	 */
	public static Node removeElement(Node parent, Node child) {
		Node removed=null;
		try {
			removed=parent.removeChild(child);
		}catch(DOMException ex) {
			removed=null;
			System.err.println(ex.getMessage());
		}
		return removed;
	}
	
	/**
	 * Creates the new XML file from a XML document
	 * @param xmlDocument Source document from create the new xml file
	 * @param filePath Location to save the new file
	 * @param fileName Name of the new file (without .xml extension)
	 */
	public static void createXmlFile(Document xmlDocument, String filePath, String fileName) {
		// write the content into xml file
		
		File xml = new File(filePath);
		xml.mkdirs(); //Creates the directories if do not exist
		
		String xmlFile = filePath+fileName+".xml";
		
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(xmlDocument);
			StreamResult result = new StreamResult(new File(xmlFile));
			transformer.transform(source, result);
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		if(PATHS.debugMode) {System.out.println("Generated "+xmlFile);}
		
	}
	
	/**
	 * Makes a copy of a file. Replace the file if existing.
	 * @param source Path of source file
	 * @param target Path of target file
	 */
	public static void copyFile(String source, String target) {
				
		try {
			Path sourcePath = Paths.get(source);
			Path targetPath = Paths.get(target);
			Files.copy(sourcePath, targetPath, REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Validates an XML file against a XSD
	 * @param xsdPath
	 * @param xmlPath
	 * @return true if validation is successful, false in other case 
	 */
	public static boolean validateXMLSchema(String xsdPath, String xmlPath){
        
        try {
            SchemaFactory factory = 
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }catch(SAXException e){
        	e.printStackTrace();
            return false;
		}
        return true;
    }

}
