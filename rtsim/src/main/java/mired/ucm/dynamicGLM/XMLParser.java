/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.dynamicGLM;

import javax.xml.parsers.ParserConfigurationException;

import mired.ucm.grid.Battery;
import mired.ucm.grid.Consumer;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.grid.TypesElement;
import mired.ucm.grid.TypesBattery;
import mired.ucm.grid.Transformer;
import mired.ucm.grid.TypesGenerator;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.NetworkElementsStatus;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Class that parses a XML file which represents the grid. 
 * This class uses GLMWriter to create the GLM file used by GridLAB-D based on the XML data.
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class XMLParser {
	
	protected Document doc;
	protected GLMWriter glmwriter;
	protected PATHS paths;
	private NetworkElementsStatus nes;
		
	/**
	 * Constructor of the class
	 * @param paths PATHS
	 */
	public XMLParser(PATHS paths){
		this.paths=paths;
	}

	/**
	 * Constructor of the class (for simulation)
	 * @param XMLfile XML file which defines the network
	 * @param netElemStatus NetworkElementsStatus
	 * @param paths PATHS
	 * @param starttime start time of simulation
	 */
	public XMLParser(String XMLfile, NetworkElementsStatus netElemStatus, PATHS paths, String starttime){
		try {
			this.nes = netElemStatus;
			this.paths=paths;
			
			// Validates the XML file
			boolean validXml=XMLUtilities.validateXMLSchema(this.getClass().getResource("/xml/networkSchema.xsd").getPath(), XMLfile);
			if(validXml){
				doc=XMLUtilities.loadXMLDoc(XMLfile); // Loads the XML file
				glmwriter= new GLMWriter(paths,starttime); // Creates the GLM file
			}
						
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e){
			e.printStackTrace();		
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Constructor of the class (only to parse, not for simulation)
	 * @param XMLfile XML file which defines the network
	 * @param paths PATHS
	 */
	public XMLParser(String XMLfile, PATHS paths){
		try {
		
			nes = null;
			this.paths=paths;
			doc=XMLUtilities.loadXMLDoc(XMLfile); // Loads the XML file
			glmwriter= new GLMWriter(paths); // Creates the GLM file
						
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e){
			e.printStackTrace();		
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	
	/**
	 * Parsers the content of the XML file and creates a GLM file with the model of the network
	 */
	public void loadModel(){	
		try {
				// Reading XML data and writing in the GLM file
				parseClock(); // Reading clock data and printing the header
				parseSubstation(); // Printing the substation
				parseCTs(); // Printing the transformation centers (CTs)
				parseConnections(); // Printing connections between CTs
				glmwriter.writeLossesCollector(); // Printing the losses collectors
				glmwriter.closeGLMFile(); // Closing the GLM file
			
			} catch (IOException e) {
		    	e.printStackTrace();
		    }
	}
	
	/**
	 * Reads the clock tags from the XML file and writes the header of the GLM file
	 */
	protected void parseClock()
	{
		String starttime,stoptime;
		Node clock = doc.getElementsByTagName("clock").item(0);
		if (clock.getNodeType() == Node.ELEMENT_NODE) {
			// Reading clock tags
			Element eclock=(Element)clock;
			starttime=eclock.getElementsByTagName("starttime").item(0).getTextContent();
			stoptime=eclock.getElementsByTagName("stoptime").item(0).getTextContent();
			
			// Writing header
			try {
				glmwriter.writeHead(starttime,stoptime);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Reads the connections between CTs and writes them in the GLM file
	 * @throws IOException
	 */
	protected void parseConnections() throws IOException {
		String cta,ctb,length,phases,configuration;
		NodeList conexiones = doc.getElementsByTagName("connection"); // Getting list of connections
		glmwriter.writeComment("CTs Connections"); // Writting comment
	 
		for (int i = 0; i < conexiones.getLength(); i++) {
			Node conexion = conexiones.item(i); // Getting next connection
			
			if (conexion.getNodeType() == Node.ELEMENT_NODE) {	 
				Element con = (Element) conexion;
				
				// Writing elements of the connection
				cta=con.getElementsByTagName("nodea").item(0).getTextContent();
				ctb=con.getElementsByTagName("nodeb").item(0).getTextContent();
				length=con.getElementsByTagName("length").item(0).getTextContent();
				phases=con.getElementsByTagName("phases").item(0).getTextContent();
				configuration=con.getElementsByTagName("configuration").item(0).getTextContent();
				
				glmwriter.writeConnection(cta,ctb,length,phases,configuration); // Writing connection in the GLM file
			}
		} 
	}
	
	/**
	 * Parses the substation elements and writes them in the GLM file
	 * @throws IOException 
	 */
	protected void parseSubstation() throws IOException{
		String name,phases,in_nominal_voltage,out_nominal_voltage,trans_configuration,line_configuration;
		Node substation = doc.getElementsByTagName("substation").item(0); // Getting substation node
		if (substation.getNodeType() == Node.ELEMENT_NODE) {
			// Reading substation elements
			Element eSubstation=(Element)substation;
			name=eSubstation.getElementsByTagName("name").item(0).getTextContent();
			phases=eSubstation.getElementsByTagName("phases").item(0).getTextContent();
			in_nominal_voltage=eSubstation.getElementsByTagName("in_nominal_voltage").item(0).getTextContent();
			out_nominal_voltage=eSubstation.getElementsByTagName("out_nominal_voltage").item(0).getTextContent();
			trans_configuration=eSubstation.getElementsByTagName("trans_configuration").item(0).getTextContent();
			line_configuration=eSubstation.getElementsByTagName("line_configuration").item(0).getTextContent();
			
			glmwriter.writeSubstation(name,phases,in_nominal_voltage,out_nominal_voltage,trans_configuration,line_configuration); // Writing substation in the GLM file
		}
		
		
	}
	
	/**
	 * Reads CTs properties and their elements and writes them in the GLM file
	 * @throws IOException
	 */
	public void parseCTs() throws IOException{
		String nameCT,phases,in_nominal_voltage;
		NodeList cts = doc.getElementsByTagName("gridnode"); // Getting list of CTs
		
		for (int i = 0; i < cts.getLength(); i++) {
			Node ct = cts.item(i); // Getting next CT
			if (ct.getNodeType() == Node.ELEMENT_NODE) {
				Element eCt = (Element) ct;
	 
				// Getting CT elements
				nameCT=eCt.getElementsByTagName("name").item(0).getTextContent(); 
				phases=eCt.getElementsByTagName("phases").item(0).getTextContent();
				in_nominal_voltage=eCt.getElementsByTagName("in_nominal_voltage").item(0).getTextContent();
				
				glmwriter.writeComment(nameCT); // Writing comment
				glmwriter.writeNode(nameCT,phases,in_nominal_voltage,false); // Writing node to connect the transformer
				
				NodeList transformers = eCt.getElementsByTagName("transformer"); // Getting transformers of the CT
				parseTransformers(transformers,nameCT,phases); // Parse transformer elements
			}
		}
	}
	
	/**
	 * Parses the transformers of a CT and their elements and writes them in the GLM file
	 * @param transformers node list of transformers
	 * @param nameCT name of the CT which the transformer belongs
	 * @param phases phases
	 * @throws IOException
	 */
	protected void parseTransformers(NodeList transformers,String nameCT, String phases) throws IOException {
		String meterName,transformerName,out_nominal_voltage,configuration;
		
		for (int j = 0; j < transformers.getLength(); j++) {
			Node transformer = transformers.item(j); // Getting next transformer
			if (transformer.getNodeType() == Node.ELEMENT_NODE)  {
				// Getting transformer elements
				Element eTransformer = (Element) transformer;
				transformerName=eTransformer.getElementsByTagName("name").item(0).getTextContent();
				configuration=eTransformer.getElementsByTagName("configuration").item(0).getTextContent();
				out_nominal_voltage=eTransformer.getElementsByTagName("out_nominal_voltage").item(0).getTextContent();
				
				// Writing the transformer in the GLM file
				meterName="meter"+transformerName;
				glmwriter.writeCT(nameCT,meterName,transformerName,phases,out_nominal_voltage,configuration);
				
				// Adds the transformer to the simulation elements
				if(nes!=null){
					nes.addTransformer(new Transformer(transformerName,nameCT));
				}
				 
				// Reading loads associated with the transformer 
				NodeList loads=eTransformer.getElementsByTagName("load");
				parseLoads(loads, meterName,nameCT,transformerName);
				
				// Reading batteries associated with the transformer 
				NodeList batteries=eTransformer.getElementsByTagName("battery");
				parseBatteries(batteries, meterName,nameCT,transformerName);
				
			}
		} 
	}

	/**
	 * Reads a load and its elements and write them in the GLM file
	 * @param loads node list of loads
	 * @param parent parent of the load in the GLM file
	 * @param nameCT name of the CT which the load belongs
	 * @param nameTrafo name of the transformer which the load belongs
	 * @throws IOException
	 */
	protected void parseLoads(NodeList loads, String parent, String nameCT, String nameTrafo) throws IOException{
		String name,phases,nominal_voltage,wire_length,line_configuration,type,connected_to;
		int control;
		double max_power;
		boolean generatePlayer=true;
		
		for (int k = 0; k < loads.getLength(); k++) {
			Node load = loads.item(k); // Getting next load
			if (load.getNodeType() == Node.ELEMENT_NODE) {
				// Getting load elements
				Element eLoad=(Element)load;
				name=eLoad.getElementsByTagName("name").item(0).getTextContent();
				phases=eLoad.getElementsByTagName("phases").item(0).getTextContent();
				nominal_voltage=eLoad.getElementsByTagName("nominal_voltage").item(0).getTextContent();
				wire_length=eLoad.getElementsByTagName("wire_length").item(0).getTextContent();
				line_configuration=eLoad.getElementsByTagName("configuration").item(0).getTextContent();
				type=eLoad.getElementsByTagName("type").item(0).getTextContent();
				control=Integer.parseInt( eLoad.getElementsByTagName("control").item(0).getTextContent() );
				max_power=Double.parseDouble( eLoad.getElementsByTagName("max_power").item(0).getTextContent() );
				connected_to=eLoad.getElementsByTagName("connected_to").item(0).getTextContent();
				
				// If the load element is a generator, the max power has negative sign
				if(TypesGenerator.isAValidType(type.trim())){
					max_power= (-1)*max_power;
				}
				
				// Adds the load to the simulation elements
				if(nes!=null) {
					ElementEMS e=null;
					if((type.trim()).contains(TypesElement.CONSUMER.toString())){ // It is a load
						e=new Consumer(TypesElement.CONSUMER,name,phases,nameCT,nameTrafo,control,max_power,connected_to);
					}else if(TypesGenerator.isAValidType(type.trim())){	// It is a generator
						boolean generadorActive;
						if(control==0){
							generadorActive=true;
						}else{
							generadorActive=false;
						}
						e=new Generator(TypesElement.GENERATOR,name,phases,nameCT,nameTrafo,control,TypesGenerator.valueOf(type),max_power,connected_to,generadorActive);
					}else{
						System.err.println("ERROR: Type of element "+type+" not valid");
					}

					nes.addElement(e);
					generatePlayer=true; //Generates player files
				}else{
					generatePlayer=false;
				}
				
				// Writes the load element if the glm file
				if(connected_to.equals(nameTrafo)){ // If the element is connected to the transformer, connect to its meter
					glmwriter.writeLoad(parent,name,phases,nominal_voltage,wire_length,line_configuration,max_power,generatePlayer);
				}else{ // else, connect to the specified element
					glmwriter.writeLoad(connected_to,name,phases,nominal_voltage,wire_length,line_configuration,max_power,generatePlayer);
				}
			}
		}
	}
	
	
	/**
	 * Reads a load and its elements and write them in the GLM file
	 * @param batteries node list of batteries
	 * @param parent parent of the battery in the GLM file
	 * @param nameCT name of the CT which the battery belongs
	 * @param nameTrafo name of the transformer which the load belongs
	 * @throws IOException
	 */
	protected void parseBatteries(NodeList batteries, String parent, String nameCT, String nameTrafo)throws IOException {
		String name;
		String phases;
		String nominal_voltage;
		String v_max,i_max,p_max,e_max,energy;
		String wire_length;
		String line_configuration;
		String base_efficiency;
		String connected_to;
		int control;
		String type;
		TypesBattery batteryType=null;
		boolean generatePlayer=true;
		
		for (int k = 0; k < batteries.getLength(); k++) 
		{
			// Getting next battery
			Node battery = batteries.item(k);
			if (battery.getNodeType() == Node.ELEMENT_NODE)
			{
				// Reading battery properties
				Element eBattery=(Element)battery;
				name = eBattery.getElementsByTagName("name").item(0).getTextContent();
				v_max=eBattery.getElementsByTagName("V_Max").item(0).getTextContent();
				i_max=eBattery.getElementsByTagName("I_Max").item(0).getTextContent();
				p_max=eBattery.getElementsByTagName("P_Max").item(0).getTextContent();
				e_max=eBattery.getElementsByTagName("E_Max").item(0).getTextContent();
				energy=eBattery.getElementsByTagName("Energy").item(0).getTextContent();
				phases=eBattery.getElementsByTagName("phases").item(0).getTextContent();
				nominal_voltage=eBattery.getElementsByTagName("nominal_voltage").item(0).getTextContent();
				wire_length=eBattery.getElementsByTagName("wire_length").item(0).getTextContent();
				line_configuration=eBattery.getElementsByTagName("configuration").item(0).getTextContent();
				base_efficiency=eBattery.getElementsByTagName("base_efficiency").item(0).getTextContent();
				control=Integer.parseInt(eBattery.getElementsByTagName("control").item(0).getTextContent() );	
				type=eBattery.getElementsByTagName("type").item(0).getTextContent();
				connected_to=eBattery.getElementsByTagName("connected_to").item(0).getTextContent();
				try {
					batteryType= TypesBattery.valueOf(type);
				} catch (IllegalArgumentException e) {
					batteryType=null;
					System.err.println("ERROR: Type of battery "+type+" not valid");
				} catch (NullPointerException e1) {
					batteryType=null;
				}
				
				// If simulation, load elements
				if(nes!=null) {
					ElementEMS e = new Battery(TypesElement.BATTERY,name,phases,nameCT,nameTrafo,control,Double.parseDouble(e_max),Double.parseDouble(energy),batteryType,Double.parseDouble(p_max),connected_to);
					e.setCurrentPower(0); // Default current power
					nes.addElement(e);
				}

				float maxPower = Float.parseFloat(p_max);
				maxPower = (float) (maxPower + 1); // +1 to be able to charge/discharge at maximum power
				p_max = String.valueOf(maxPower);
				
				if(nes!=null) {
					generatePlayer=true; //Generate players files
				}else{
					generatePlayer=false;
				}
				// Writes battery element in the glm file
				if(connected_to.equals(nameTrafo)){ // If the element is connected to the transformer, connect to its meter
					glmwriter.writeBattery(parent,name,v_max,i_max,p_max,e_max,energy,base_efficiency,phases,nominal_voltage,wire_length,line_configuration,generatePlayer);
				}else{ // else, connect to the specified element
					glmwriter.writeBattery(connected_to,name,v_max,i_max,p_max,e_max,energy,base_efficiency,phases,nominal_voltage,wire_length,line_configuration,generatePlayer);
				}
				
			}
		}
	}

}
