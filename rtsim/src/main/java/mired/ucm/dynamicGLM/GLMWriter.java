/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.dynamicGLM;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import mired.ucm.properties.PATHS;
import mired.ucm.properties.PropertiesReader;

/**
 * Class that creates the GLM file which defines the grid for executing GridLAB-D
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class GLMWriter {
	protected BufferedWriter bw;
	protected PropertiesReader prop;
	protected PATHS paths;
	
	//Properties
	protected String default_line_length;
	protected String recorder_interval;
	protected String recorder_path;
	protected String recorder_properties;
	protected String player_path;
	protected String configuration_file;
	protected String starttime;
	protected String substationMeter;
	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 * @throws FileNotFoundException
	 */
	public GLMWriter(PATHS paths) throws FileNotFoundException{
		this.paths=paths;
		
		// Reading properties
		prop = new PropertiesReader(getClass().getResourceAsStream("/properties/dynamicglm.properties"));
		default_line_length = prop.getProperty("default_line_length");
		recorder_interval = prop.getProperty("recorder.interval");
		recorder_properties = prop.getProperty("recorder.properties");
		recorder_path = paths.getOutputs();
		player_path = paths.getPlayers();
		configuration_file = paths.getConfiguration();
		substationMeter = paths.getSubstationMeterName();
		starttime = prop.getProperty("default_starttime");
				
		// Create needed directories if they do not exist
		File outputsFolder = new File(recorder_path);
		File playersFolder = new File(player_path);
		outputsFolder.mkdirs();
		playersFolder.mkdirs();
		
		bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(paths.getGlmModel()))); // Generates .glm file 
	}
	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 * @param startTime Start time of simulation, for player files
	 * @throws FileNotFoundException
	 */
	public GLMWriter(PATHS paths, String startTime) throws FileNotFoundException{
		this(paths);
		starttime = startTime;
	}
	
	/**
	 * Writes a comment in the GLM file
	 * @param comment comment to write
	 * @throws IOException
	 */
	public void writeComment(String comment) throws IOException{
		bw.write("// "+comment+"\n\n");
	}
	
	/**
	 * Writes an include in the GLM file
	 * @param file the file to include
	 * @throws IOException
	 */
	public void writeInclude(String file) throws IOException{
		bw.write("#include \""+file+"\";\n\n");
	}
	
	/**
	 * Writes the header of GLM file
	 * @param starttime timestamp to start the simulation
	 * @param stoptime timestamp to stop the simulation
	 * @throws IOException
	 */
	public void writeHead(String starttime,String stoptime) throws IOException{
		writeComment("Network generated by SGSim framework");
		bw.write("clock {\n" +
				 "\ttimezone GMT0;\n" +
				 "\tstarttime '"+starttime+"';\n" +
				 "\tstoptime '"+stoptime+"';\n" +
				 "}\n\n");
		
		bw.write("module tape;\n" +
				 "module generators;\n" +
				 "module powerflow {\n" +
				 "\tsolver_method NR;\n" +
				 "}\n\n");
		
		writeInclude(configuration_file);
	}
	
	/**
	 * Writes a connection between two elements
	 * @param from first element for connection
	 * @param to second element for connection
	 * @param length line length
	 * @param configuration line configuration
	 * @throws IOException
	 */
	public void writeConnection(String from,String to,String length,String phases,String configuration) throws IOException{
		if(length.equalsIgnoreCase("-1")) {
			length=default_line_length; //If line length is not known (-1) put default value
		}
		
		bw.write("object underground_line {\n" +
				 "\tname "+from+"_"+to+";\n" +
				 "\tphases "+phases+";\n" +
				 "\tfrom "+from+";\n" +
				 "\tto "+to+";\n" +
				 "\tlength "+length+" m;\n" +
				 "\tconfiguration "+configuration+";\n" +
				 "\tgroupid Distribution_Line;\n" +
				 "}\n\n");
	}
	
	/**
	 * Writes a meter node
	 * @param name name of the meter
	 * @param phases phases
	 * @param nominal_voltage nominal voltage
	 * @param writeRecorder if the meter includes a recorder (true) or not (false)
	 * @throws IOException
	 */
	public void writeMeter(String name, String phases, String nominal_voltage,boolean writeRecorder) throws IOException{
		bw.write("object meter {\n" +
				"\tname "+name+";\n" +
				"\tphases "+phases+";\n" +
				"\tnominal_voltage "+nominal_voltage+";\n");
		if(writeRecorder){
			bw.write("\tobject recorder {\n" +
				"\t\tfile "+recorder_path+name+".csv;\n" +
				"\t\tinterval "+recorder_interval+";\n" +
				"\t\tproperty "+recorder_properties+";\n" +
				"\t};\n");
				
		}
		bw.write("}\n\n");
	}
	
	/**
	 * Writes a node
	 * @param name name of the node
	 * @param phases phases
	 * @param nominal_voltage nominal voltage
	 * @param swing if the node includes a swing bus (true) or not (false)
	 * @throws IOException
	 */
	public void writeNode(String name, String phases, String nominal_voltage,boolean swing) throws IOException{
		bw.write("object node {\n" +
				"\tname "+name+";\n" +
				"\tphases "+phases+";\n" +
				"\tnominal_voltage "+nominal_voltage+";\n");
		
		if(swing) {bw.write("\tbustype SWING;\n");} // Node with SWING bus
				
		bw.write("}\n\n");
	}
	
	/**
	 * Writes a transformer
	 * @param name name of the transformer
	 * @param from source node
	 * @param to destination node
	 * @param phases phases
	 * @param configuration configuration
	 * @throws IOException
	 */
	public void writeTransformer(String name,String from,String to,String phases,String configuration) throws IOException{
		bw.write("object transformer {\n" +
				"\tname "+name+";\n" +
				"\tgroupid Distribution_Trans;\n" +
				"\tfrom "+from+";\n" +
				"\tto "+to+";\n" +
				"\tphases "+phases+";\n" +
				"\tconfiguration "+configuration+";\n" +
				"}\n\n");
	}
	
	/**
	 *Write the entry substation composed by : Node SWING + Meter + Transformer + Meter + Node
	 * @throws IOException
	 */
	public void writeSubstation(String name,String phases,String in_nominal_voltage,String out_nominal_voltage,String transConfiguration, String lineConfiguration) throws IOException{
		writeComment("Substation"); // Comment
		writeNode("substationNode",phases,in_nominal_voltage,true); // First node (SWING)
		writeConnection("substationNode", substationMeter, "1", phases, lineConfiguration); // Connection node-meter
		writeMeter(substationMeter,phases,in_nominal_voltage,true); // Meter with recorder
		writeTransformer("substationTransformer",substationMeter,substationMeter+"II",phases,transConfiguration);// Transformer
		writeMeter(substationMeter+"II",phases,out_nominal_voltage,true); // Meter with recorder
		writeConnection(substationMeter+"II", name, "1", phases, lineConfiguration); // Connection meter - node
		writeNode(name,phases,out_nominal_voltage,false); // Node to connect to other elements
	}
	
	/**
	 * Write a TC (transformation center) composed by transformer + meter
	 * @param nodeFrom source node
	 * @param meterName name of the meter
	 * @param transformerName name of the transformer
	 * @param phases phases
	 * @param out_nominal_voltage output nominal voltage
	 * @param configuration configuration
	 * @throws IOException
	 */
	public void writeCT(String nodeFrom,String meterName,String transformerName,String phases,String out_nominal_voltage,String configuration) throws IOException {	
		writeTransformer(transformerName,nodeFrom,meterName,phases,configuration);
		writeMeter(meterName,phases,out_nominal_voltage,true);
	}
	
	/**
	 *  Writes a connection to a load element composed by : Connection line + Meter + Load
	 * @param parent
	 * @param name
	 * @param phases
	 * @param nominal_voltage
	 * @param wire_lenght
	 * @param line_configuration
	 * @throws IOException
	 */
	public void writeLoad(String parent,String name,String phases,String nominal_voltage,String wire_lenght,String line_configuration,double value,boolean generatePlayer) throws IOException
	{
		String meterLoad="meter"+name; // Set meter name
		writeComment(name); // Write comment
		writeConnection(parent,meterLoad,wire_lenght,phases,line_configuration); // Write connection
		writeMeter(meterLoad,phases,nominal_voltage, true); // Write meter
		
		// Write a load object
		bw.write("object load {\n" +
				"\tname "+name+";\n" +
				"\tparent "+meterLoad+";\n" +
				"\tphases "+phases+";\n" +
				"\tnominal_voltage "+nominal_voltage+";\n\n");
		
		// Writer players
		phases=phases.replace("N","").trim();
		for(int f=0; f<phases.length();f++){
			bw.write("\tobject player{\n" +
					"\t\tproperty constant_power_"+phases.charAt(f)+";\n"+
					"\t\tfile "+player_path+name+"_"+phases.charAt(f)+".player;\n" +
					"\t};\n\n");
			
			if(generatePlayer) { // Create player files
				generatePlayer(player_path+name+"_"+phases.charAt(f)+".player","constant_power_"+phases.charAt(f),0);
			}
		}
		
		bw.write("}\n\n"); // Close file
	}
	
	/**
	 * Writes a connection to a battery composed by: Line connection + Meter + Battery
	 * @param parent
	 * @param name
	 * @param v_max
	 * @param i_max
	 * @param p_max
	 * @param e_max
	 * @param energy
	 * @param phases
	 * @param nominal_voltage
	 * @param wire_lenght
	 * @param line_configuration
	 * @throws IOException
	 */
	public void writeBattery(String parent,String name,String v_max,String i_max,String p_max,String e_max,String energy,String base_efficiency,String phases,String nominal_voltage,String wire_lenght,String line_configuration, boolean generatePlayer) throws IOException{
		String meterBattery="meter"+name; // Set meter name
		
		writeComment(name); // Write comment
		writeConnection(parent,meterBattery,wire_lenght,phases,line_configuration); // Write connection
		writeMeter(meterBattery,phases,nominal_voltage,true); // Write meter
		
		// Write battery object
		bw.write("object battery {\n" +
				"\tname "+name+";\n" +
				"\tparent "+meterBattery+";\n" +
				"\tgenerator_status ONLINE;\n" +
				"\tgenerator_mode CONSTANT_PQ;\n" +
				"\tV_Max "+v_max+";\n" +
				"\tI_Max "+i_max+";\n" +
				"\tP_Max "+p_max+";\n" +
				"\tE_Max "+e_max+";\n" +
				"\tEnergy "+energy+";\n" +
				"\tbase_efficiency "+base_efficiency+";\n\n");
		
		// Write player
		bw.write("\tobject player{\n" +
				"\t\tproperty scheduled_power;\n"+
				"\t\tfile "+player_path+name+".player;\n" +
				"\t};\n\n");
	
		// Write recorder
		bw.write("\tobject recorder{\n" +
				"\t\tfile "+recorder_path+name+".recorder;\n" +
				"\t\tinterval "+recorder_interval+";\n" +
				"\t\tproperty Energy,battery_state;\n" +
				"\t};\n\n");							
		
		bw.write("}\n\n"); // Close file

		if(generatePlayer) { // Create player files
			generatePlayer(player_path+name+".player","scheduled_power",0);
		}
	}
	
	/**
	 * Writes collectors for line and transformation losses
	 * @throws IOException
	 */
	public void writeLossesCollector() throws IOException{
		String interval=prop.getProperty("losses_collector_interval");
		writeComment("Losses collector"); // Write comment
		bw.write("object collector {\n"
				+ "\tgroup \"class=underground_line AND groupid=Distribution_Line\";\n"
				+ "\tproperty sum(power_losses.real);\n"
				+ "\tinterval "+interval+";\n"
				+ "\tfile \""+recorder_path+"UGLine_Losses.csv\";\n"
				+ "}\n\n");
		
		bw.write("object collector {\n"
				+ "\tgroup \"class=transformer AND groupid=Distribution_Trans\";\n"
				+ "\tproperty sum(power_losses.real);\n"
				+ "\tinterval "+interval+";\n"
				+ "\tfile \""+recorder_path+"Trans_Losses.csv\";\n"
				+ "}\n\n");
	}	

	/**
	 * Close the GLM file
	 * @throws IOException
	 */
	public void closeGLMFile() throws IOException{
			bw.close();
	}
	
	/**
	 * Generates a .player file by default
	 * @param file path of the player file to generate
	 * @param property property to read from the player
	 */
	public void generatePlayer(String file, String property, double value){
		try {
			BufferedWriter b = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,false)));
			b.write("#TIMESTAMP, "+property+"\n"+starttime+", "+value);
			b.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}