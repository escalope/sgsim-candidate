/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

/**
 * Sensors that it is possible to read from GridLAB-D outputs
 * @author Nuria Cuartero-Soler
 *
 */
public enum SensorIDS {
	LOSSES,
	MEASURED_ENERGY,
	POWER_PEAK,
    POWER_DEMAND,
    REACTIVE_ENERGY,
    REACTIVE_POWER,
	CURRENT_A_REAL,
	CURRENT_A_IMAG,
	CURRENT_B_REAL,
	CURRENT_B_IMAG,
	CURRENT_C_REAL,
	CURRENT_C_IMAG,
	VOLTAGE_A_REAL,
	VOLTAGE_A_IMAG,
	VOLTAGE_B_REAL,
	VOLTAGE_B_IMAG,
	VOLTAGE_C_REAL,
	VOLTAGE_C_IMAG,	
}
