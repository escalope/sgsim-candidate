/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.w3c.dom.Node;

import mired.ucm.dynamicGLM.XMLGeneratorByEvents;
import mired.ucm.grid.Battery;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.grid.Transformer;
import mired.ucm.grid.TypesElement;
import mired.ucm.properties.PATHS;

/**
 * Class which represents the state of the elements of the network during the simulation.
 * @author Nuria Cuartero-Soler
 *
 */
public class NetworkElementsStatus {
	
	/** Elements of the network */
	private Set<ElementEMS> elements; 
	
	/** Transformers of the network*/
	private Set<Transformer> transformers; 
	
	/**
	 * Constructor of the class
	 */
	public NetworkElementsStatus() {
		elements=new HashSet<ElementEMS>();
		transformers = new HashSet<Transformer>();
	}
	
	/**
	 * Returns a Set with the transformers of the network
	 * @return
	 */
	public synchronized Set<Transformer> getTransformers() {
		return transformers;
	}
	
	/**
	 * Returns a Set with the names of the transformers of the network
	 * @return
	 */
	public synchronized Set<String> getTransformersNames(){
		Set<String> names = new HashSet<String>();
		for(Transformer trans:getTransformers()){
			names.add(trans.getName());
		}
		return names;
	}
	
	/**
	 * Add a new transformer
	 * @param transformer Name of the transformer
	 */
	public synchronized void addTransformer(Transformer transformer) {
		transformers.add(transformer);
	}
	
	/**
	 * Returns the Transformer with the given name
	 * @param transformerName
	 * @return the Transformer if it exits, null if not
	 */
	public synchronized Transformer getTransformerByName(String transformerName) {
		for(Transformer trafo:getTransformers()) {
			if(trafo.getName().equals(transformerName))
				return trafo;
		}
		
		return null;
	}

	/**
	 * Returns a set with the elements of the network
	 * @return
	 */
	public synchronized Set<ElementEMS> getElements() {
		return elements;
	}

	/**
	 * Returns a set with the controllable elements of the network
	 * @return
	 */
	public synchronized Set<ElementEMS> getControllableElements() {
		Set<ElementEMS> controllable=new HashSet<ElementEMS>();
		
		for(ElementEMS e:getElements()) {
			if(e.getControlType()!=0) {
				controllable.add(e);
			}
		}
		
		return controllable;
	}
	
	/**
	 * Returns a Set with the controllable generators type 2 (SetControl) of the network
	 * @return
	 */
	public synchronized Set<ElementEMS> getControllableGeneratorsType2() {
		Set<ElementEMS> controllable=new HashSet<ElementEMS>();
		
		for(ElementEMS e:getElements()) {
			if(e instanceof Generator && e.getControlType()==2) {
				controllable.add(e);
			}
		}
		
		return controllable;
	}
	
	/**
	 * Returns a set with the elements of the network which belong to a transformer
	 * @param transformerName
	 * @return the elements
	 */
	public synchronized Set<ElementEMS> getElementsByTransformer(String transformerName) {
		Set<ElementEMS> elementsByTransformer=new HashSet<ElementEMS>();
		
		for(ElementEMS e:getElements()) {
			if(e.getTransformer().equals(transformerName)) {
				elementsByTransformer.add(e);
			}
		}
		
		return elementsByTransformer;
	}
	
	/**
	 * Returns a set with the controllable elements of the network which belong to a transformer
	 * @param transformerName
	 * @return
	 */
	public synchronized Set<ElementEMS> getControllableElementsByTransformer(String transformerName) {
		Set<ElementEMS> elementsByTransformer=new HashSet<ElementEMS>();
		
		for(ElementEMS e:getElements()) {
			if(e.getTransformer().equals(transformerName) && e.getControlType()!=0) {
				elementsByTransformer.add(e);
			}
		}
		
		return elementsByTransformer;
	}
	
	/**
	 * Returns the element with the specified name
	 * @param elementName
	 * @return The element, or null if no element found with this name
	 */
	public synchronized ElementEMS getElementByName(String elementName) {
		for(ElementEMS e:getElements()) {
			if(e.getName().equals(elementName))
				return e;
		}
		
		return null;
	}
	
	/**
	 * Returns the available elements of the grid
	 * @return available elements
	 */
	public synchronized Set<ElementEMS> getAvailableElements() {
		Set<ElementEMS> availableElements = new HashSet<ElementEMS>();
		for(ElementEMS el: getElements()){
			if(!el.isDisabled()){ //If enabled
				availableElements.add(el);
			}
		}

		return  availableElements;
	}
	
	/**
	 * Returns the available elements which belong to a specified transformer
	 * @param transformerName The name of the transformer
	 * @return available elements of the transformer
	 */
	public synchronized Set<ElementEMS> getAvailableElementsByTransformer(String transformerName) {
		Set<ElementEMS> els = getElementsByTransformer(transformerName);
		Set<ElementEMS> availableElements = new HashSet<ElementEMS>();
		for(ElementEMS el: els){
			if(!el.isDisabled()){ //If enabled
				availableElements.add(el);
			}
		}

		return  availableElements;
	}
	
	/**
	 * Get the elements names of a transformer
	 * @return the names of the elements
	 */
	public synchronized Vector<String> getElementsNamesByTransformer(String transformerName) {
		Vector<String> listElements = new Vector<String>();
		Set<ElementEMS> elementos = getElementsByTransformer(transformerName);
		if(!elementos.isEmpty()){
			for(ElementEMS e:elementos) {
				listElements.add(e.getName());
			}
		}
		return listElements;
	}
	
	/**
	 * Get the generators of the grid
	 * @return All the elements which are generators
	 */
	public synchronized Set<ElementEMS> getGenerators(){
		Set<ElementEMS> generetors = new HashSet<ElementEMS>();
		for(ElementEMS element:getElements()){
			if(element instanceof Generator) {
				generetors.add(element);
			}
		}
		
		return generetors;
	}
	
	/**
	 * Get the available transformers names of the network
	 * @return available transformers 
	 */
	public synchronized Set<String> getAvailableTransformersNames() {
		Set<Transformer> trafos = getTransformers();
		Set<String> namesTrafos = new HashSet<String>();
		for(Transformer t: trafos){		
			if(t.isEnabled()){ //If enabled
				namesTrafos.add(t.getName());
			}
		}
		
		return namesTrafos;
	}
	
	
	/**
	 * Adds a new element
	 * @param e
	 */
	public synchronized void addElement(ElementEMS e) {
		if(e!=null) elements.add(e);
	}
	
	/**
	 * Removes an element
	 * @param e
	 */
	public synchronized void removeElement(ElementEMS e) {
		elements.remove(e);
	}

	/**
	 * Adds all the elements contained in the Collection
	 * @param newElements
	 */
	public synchronized void addElements(Collection<ElementEMS> newElements) {
		for(ElementEMS e:newElements) {
			if(e!=null) {
				addElement(e);
			}
		}
	}
	
	/**
	 * Removes all the elements contained in the Collection
	 * @param newElements
	 */
	public synchronized void removeElements(Collection<ElementEMS> oldElements) {
		for(ElementEMS e:oldElements) {
			removeElement(e);
		}
	}
	
	/**
	 * Returns the element with the given name, only if it is a controllable element
	 * @param elementName
	 * @return the element if is controllable, null in other case
	 */
	public synchronized ElementEMS isAControllableElement(String elementName) {
		for(ElementEMS e:getElements()) {
			if(e.getName().equals(elementName) && e.getControlType()!=0) {
				return e;
			}
		}
		
		return null;
	}
	
	/**
	 * Disable an element of the network
	 * @param elementName Name of the element
	 * @return true if success, false in other case
	 */
	public synchronized boolean disableElementEvent(String elementName, PATHS paths) {
		Node disabledNode=null;
		XMLGeneratorByEvents xgbe = new XMLGeneratorByEvents(paths);
		ElementEMS e = this.getElementByName(elementName);
		
		if(e!=null) {
			disabledNode=xgbe.disableElementEvent(e.getName(), e.getTransformer());
			if(disabledNode!=null) {
				e.setDisabled(true); //Set element disabled
				e.setXmlNode(disabledNode); // Save the XML node
				// Disable all the elements connected to this one
				for(ElementEMS connectedElement:getElementsConnectedTo(elementName)){
					disableElementEvent(connectedElement.getName(),paths);
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Enable an element of the network
	 * @param elementName Name of the element
	 * @return true if success, false in other case
	 */
	public synchronized boolean enableElementEvent(String elementName, PATHS paths) {
		Node enabledNode=null;
		XMLGeneratorByEvents xgbe = new XMLGeneratorByEvents(paths);
		ElementEMS e = this.getElementByName(elementName);
		
		// If the element exists
		if(e!=null){
			enabledNode=xgbe.enableElementEvent(e.getXmlNode(), e.getTransformer()); //Include the node of the element in the XML file
			if(enabledNode!=null) {
				e.setDisabled(false); // Enable element
				e.setXmlNode(null); // Remove node
				// Enable all the elements connected to this one
				for(ElementEMS connectedElement:getElementsConnectedTo(elementName)){
					enableElementEvent(connectedElement.getName(),paths);
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Disables a transformer of the network
	 * @param trafoName Name of the transformer to disable
	 * @return true if success, false in other case
	 */
	public synchronized boolean disableTransformerEvent(String trafoName, PATHS paths) {
		Node disabledNode=null;
		XMLGeneratorByEvents xgbe = new XMLGeneratorByEvents(paths);
		boolean specialSituation=false;
		Transformer trafo = getTransformerByName(trafoName);
		
		if(trafo!=null && trafo.isEnabled()) {
			disabledNode=xgbe.disableTransformerEvent(trafoName); // Disable transformer
			
			if(disabledNode!=null) {
				trafo.setEnabled(false); // Set disabled state
				if(!specialSituation) disableElementsOfTransformer(trafo.getName()); //Disable elements
				trafo.setXmlNode(disabledNode); //Save XML node to restore
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Enable a transformer of the network
	 * @param trafoName Name of the transformer
	 *  to enable
	 * @return true if success, false in other case
	 */
	public synchronized boolean enableTransformerEvent(String trafoName, PATHS paths) {
		Node enabledNode=null;
		XMLGeneratorByEvents xgbe = new XMLGeneratorByEvents(paths);
		boolean specialSituation=false;
		Transformer trafo = getTransformerByName(trafoName);
		
		if(trafo!=null && !trafo.isEnabled()){
			enabledNode=xgbe.enableTransformerEvent(trafo.getXmlNode(), trafo.getCt()); //Enable the transformer restoring the XML node
			if(enabledNode!=null) {
				trafo.setEnabled(true); //Set enable state
				if(!specialSituation) enableElementsOfTransformer(trafo.getName()); //Enable elements
				trafo.setXmlNode(null); 
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Disables the elements of the specified transformer
	 * @param transformerName
	 */
	public synchronized void disableElementsOfTransformer(String transformerName){
		
		Set<ElementEMS> elements = this.getElementsByTransformer(transformerName);
		for(ElementEMS e:elements) {
			e.setDisabled(true);
		}
	}
	
	/**
	 * Enables the elements of the specified transformer, except for the elements which were disabled before the transformer was
	 * @param transformerName
	 */
	public synchronized void enableElementsOfTransformer(String transformerName){
		
		Set<ElementEMS> elements = this.getElementsByTransformer(transformerName);
		for(ElementEMS e:elements) {
			if(e.getXmlNode()==null){ // If the element was not disabled before, enable
				e.setDisabled(false);
			}
		}
	}
	
	/**
	 * Change the transformer which supplies the elements of a specified transformer
	 * @param transfromerFrom Transformer of origin
	 * @param transformerTo New transformer which supplies the elements
	 * @param isABypass Indicates if is a bypass connection (true) or if the elements will be supplied by its original transformer (false)
	 */
	private void supplyElementsByOtherTransformer(String transfromerFrom, String transformerTo, boolean isABypass) {
		if(isABypass){
			for(ElementEMS e:getElements()){ // Change of the elements
				if(e.getTransformer().equals(transfromerFrom)){
					e.setTransformer(transformerTo);
					e.setBypassSupplied(isABypass);
				}
			}
		}else{
			for(ElementEMS e:getElements()){ // Change only the elements with a bypass active
				if(e.getTransformer().equals(transfromerFrom) && e.isBypassSupplied()){
					e.setTransformer(transformerTo);
					e.setBypassSupplied(isABypass);
				}
			}
		}
	}
	
	/**
	 * Calculates the sum of all the generation of the grid, including generators and batteries
	 * @return The ABSOLUTE VALUE of the sum of all generation in W
	 */
	public double calculateGenerationSum(){
		double generation = 0;
		
		for(ElementEMS element:getElements()) {
			if(!element.isDisabled()){
				if(element instanceof Generator && ((Generator) element).isActivated()){
					generation+=Math.abs(element.getCurrentPower());
				}else if(element instanceof Battery && element.getCurrentPower()<0 && ((Battery)element).getEnergy()>0){
					generation+=Math.abs(element.getCurrentPower());
				}
			}
		}
		
		return generation;
	}
	
	/**
	 * Return the elements connected to a specified element of the network.
	 * @param parentElement Parent element 
	 * @return A set of elements which are connected to the specified parent.
	 */
	public Set<ElementEMS> getElementsConnectedTo(String parentElement){
		Set<ElementEMS> connectedElements = new HashSet<ElementEMS>();
		for(ElementEMS e:getElements()){
			if(e.getConnectedTo().equals(parentElement)){
				connectedElements.add(e);
			}
		}
		return connectedElements;
	}
	
	/**
	 * Return the names of the elements connected to a specified element of the network.
	 * @param parentElement Parent element 
	 * @return A set with the names of the elements which are connected to the specified parent.
	 */
	public Set<String> getElementsNamesConnectedTo(String parentElement){
		Set<String> connectedElements = new HashSet<String>();
		for(ElementEMS e:getElements()){
			if(e.getConnectedTo().equals(parentElement)){
				connectedElements.add(e.getName());
			}
		}
		return connectedElements;
	}
	
}
