/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

import java.util.Date;
import java.util.Hashtable;

import mired.ucm.monitor.Event;
import mired.ucm.simulator.NetworkElementsStatus;
import mired.ucm.simulator.NetworkStatus;

/**
 * Interface to listen what is happening in the network 
 * 
 * @author Nuria Cuartero-Soler
 * @author Jorge J. Gomez-Sanz
 *
 */
public interface NetworkListener {
	/**
	 * Receives information about sensors values in a specific timestamp 
	 * @param sensorValues
	 * @param timestamp
	 */
	public void processStatus(Hashtable<SensorIDS, Float> sensorValues,Date timestamp);
	
	/**
	 * Indicates that an overvoltage situation has been produced at the specified time
	 * @param timestamp
	 */
	public void overvoltageEvent(Date timestamp);
	
	/**
	 * Receives network status for a new cycle of simulation
	 * @param ns network status
	 * @param nes network elements' status
	 * @param timestamp timestamp
	 */
	public void processCycleStatus(NetworkStatus ns, NetworkElementsStatus nes, Date timestamp);
	
	/**
	 * Receives information about an event generated in simulation
	 * @param event
	 */
	public void processEvent(Event event);
}
