/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.clients;

import java.util.Calendar;
import java.util.Vector;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.monitor.Event;
import mired.ucm.simulator.orders.Order;

/**
 * Simulator Client interface
 * @author Nuria Cuartero-Soler
 *
 */
public interface Client {
	/**
	 * Returns the name of the client
	 * @return the name
	 */
	public String getName();
	
	/**
	 * Registers a listener
	 * @param listener
	 * @return true if success, false in other case
	 */
	public boolean registerListener(NetworkListener listener);
	
	/**
	 * Unregisters a listener
	 * @param listener
	 * @return true if success, false in other case
	 */
	public boolean unregisterListener(NetworkListener listener);
	
	/**
	 * Returns the registered listeners  
	 * @return the listeners
	 */
	public Vector<NetworkListener> getListeners();
	
	/**
	 * Tasks to perform in every cycle of simulation (only used for passive clients).
	 * This method returns the orders to be executed at this simulation time.
	 * @param simtime current simulation time
	 * @return the orders to be executed by the simulator
	 */
	public Vector<Order> runCycle(Calendar simtime);
	
	/**
	 * Notifies an event produced in simulation
	 * @param event the event
	 * @return the orders to be executed in response to the new event
	 */
	public Vector<Order> processEvent(Event event);
}
