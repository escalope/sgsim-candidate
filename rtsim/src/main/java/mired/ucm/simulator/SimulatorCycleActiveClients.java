package mired.ucm.simulator;

import java.util.Calendar;
import java.util.Set;
import java.util.Vector;

import mired.ucm.grid.ElementEMS;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.orders.SetControl;
import mired.ucm.simulator.orders.SwitchOn;

/**
 * Class which simulates the behavior of a smart grid using cycles of simulation.
 * It simulates the advance of the time specifying the number of units of a cycle.
 * @author Nuria Cuartero-Soler
 *
 */
public class SimulatorCycleActiveClients extends SimulatorCycle{
	
	/**
	 * Constructor of the class
	 * @param simtime start time of simulation
	 * @param simulator simulator
	 * @param TIME_UNIT time unit of the cycles
	 * @param TIME_UNIT_AMOUNT time amount of the cycles
	 * @param gridlabActions
	 */
	public SimulatorCycleActiveClients(Calendar simtime, Simulator simulator, int TIME_UNIT,int TIME_UNIT_AMOUNT,NetworkElementsStatus nes,
			boolean intelligence,PATHS paths,ReadScenario rs, GridlabActions gridlabActions){
		super(simtime,simulator, TIME_UNIT, TIME_UNIT_AMOUNT, nes, intelligence, paths, rs, gridlabActions);
	}

	/**
	 * Run the cycles of the simulation. In every cycle, the system updates the current scenario 
	 * and notify listeners the new network status.
	 */
	public void run(){
		
		try {
			while (!stop) {
				if (!pause) {
					
					simulator.checkForNewGridlabCycle(simtime.getTime()); // Ask simulator for a new GridLAB-D cycle
					askForEvents(simtime.getTime()); // Ask for events
					setNetworkStatus(simtime.getTime(), new NetworkStatus()); // Sets a new NetworkStatus at the current time
					simulator.loadScenario(simtime.getTime()); // Loads the current scenario
					
					// If intelligence OFF, apply default orders in the first cycle
					if(!intelligence && firstCycle){ 
						Set<ElementEMS> generators=nes.getGenerators(); // Getting the generators of the network
						Vector<Order> orders=new Vector<Order>(); // Orders to apply
						// Applying default orders to generators
						for(ElementEMS element:generators) {
							if(element.getControlType()==1) {
								orders.add(new SwitchOn(element.getName(),simtime.getTime(),rs.getGenerationOrLoad(element, simtime.getTime()),paths));
							}else if(element.getControlType()==2) {
								orders.add(new SetControl(element.getName(),simtime.getTime(),element.getMaxPower(),paths));
							}
						}
						
						// Sending orders to simulator
						for (Order order:orders){
							simulator.queueOrder(order);
						}
						
						// Waiting for orders to be executed
						while (!simulator.getPendingOrders().isEmpty()){
							Thread.sleep(100);
						}
						
						firstCycle = false;
					}
					
					
					updateNetworkStatus(simtime.getTime()); // Once the orders have been executed (if needed) the network status is updated
					notifyStatus(getNetworkStatus(simtime.getTime()),simtime.getTime()); // Notify new status to listeners
					simtime.add(timeUnit, timeAmount); // Advance to the next cycle
					
				} else { // If pause
					Thread.sleep(100);
				}
			}
			
			if(stop) {
				simulator.stop(); // Stops simulator
			}
			
			if(PATHS.debugMode){System.out.println("Stopping SimulatorCycle");} // Log
		
		} catch (GridlabException e2) {
			e2.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
