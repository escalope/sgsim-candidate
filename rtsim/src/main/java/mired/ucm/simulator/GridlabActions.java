/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package mired.ucm.simulator;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.regex.PatternSyntaxException;

import mired.ucm.properties.PATHS;

/**
 * Class to run and synchronize GridLAB-D actions such as execute it, read meter sensors and modify the glm file.
 *
 * @author Nuria Cuartero-Soler
 */
public class GridlabActions implements Serializable{
	private static final long serialVersionUID = 1L;
	private PATHS paths;
	private String gridlabSubstationMeter; // Output file of the substation meter
	private String gridlabLineLosses; // Output file of line losses
	private String gridlabTransformationLosses; // Output file of transformation losses
	private String gridlabOutputsPath; // Path of GridLAB-D output files
	private String grilabdGlmFile; // glm file to execute GridLAB-D
	private String gridlabdOutput; // GridLAB-D output file
	private SimpleDateFormat sdf;
	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 */
	public GridlabActions(PATHS paths) {
		this.paths=paths;
		gridlabSubstationMeter = paths.getSubstationMeterFile();
		gridlabLineLosses =paths.getLinesLosses();
		gridlabTransformationLosses = paths.getTransLosses();
		gridlabOutputsPath = paths.getOutputs();
		grilabdGlmFile = paths.getGlmModel();
		gridlabdOutput = paths.getNetFolder()+"gridlabOutput.txt";
		sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //GridLAB-D date format;
	}
	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 * @param subsationMeter substation meter file
	 * @param lineLosses line losses output file
	 * @param transfLosses transformation losses output file
	 * @param outputPath path of the output folder of GridLAB-D
	 */
	public GridlabActions(PATHS paths,String subsationMeter, String lineLosses, String transfLosses, String outputPath) {
		this.paths=paths;
		gridlabSubstationMeter = subsationMeter;
		gridlabLineLosses = lineLosses;
		gridlabTransformationLosses = transfLosses;
		gridlabOutputsPath = outputPath;
		grilabdGlmFile = paths.getGlmModel();
		gridlabdOutput = paths.getNetFolder()+"gridlabOutput.txt";
		sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //GridLAB-D date format;
	}
	

	/**
	 * Executes GridLAB-D.
	 * An exception is thrown if the execution fails.
	 * @param gridlabdCommand Command to execute
	 * @throws GridlabException
	 */
	public synchronized String execGridlab(String gridlabdCommand) throws GridlabException{
		Process process=null;
		String gridlabdTextOutput=null;
		String errorOutput=null;
		String standardOutput=null;

		try {
			process = Runtime.getRuntime().exec(gridlabdCommand, new String[]{ //Run GridLAB-D
					"GRIDLABD_FOLDER="+paths.getGridLabPath()
			});
			process.waitFor(); // Wait the process to finish
			
			// Reading execution outputs
			standardOutput=readInputStreamAsString(process.getInputStream());
			errorOutput=readInputStreamAsString(process.getErrorStream());
			gridlabdTextOutput=standardOutput+errorOutput;
			
			// Print output into a file
			printOutputToFile(gridlabdTextOutput);
			errorOutput=errorOutput.replace("WARNING  [INIT] : gnuplot.conf(7): variable 'PATH' not found",""); // Remove not relevant messages
			
			if(errorOutput.contains("FATAL") || errorOutput.contains("ERROR") || errorOutput.contains("No such file or directory")){ // Check for errors
				throw new GridlabException("while executing "+gridlabdCommand+"\n"+errorOutput);
			}else if(errorOutput.contains("WARNING") && !errorOutput.contains("%")){ // Check for warnings
				System.err.println("GRIDLAB-D: "+errorOutput);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			if(process!=null) process.destroy();
		}
		
		return gridlabdTextOutput;
	}
	
	/**
	 * Gets the substation sensors of the GridLAB-D output in the current simulation time
	 * @param simtime current simulation time
	 * @return the substation sensors values, or an empty hashtable if no sensors found for this simulation time
	 */
	public synchronized Hashtable<SensorIDS,Float> getSubstationSensors(Date simtime) { 
		String []data=null;
		Hashtable<SensorIDS,Float> sensors = new Hashtable<SensorIDS,Float>();
		String line=null;
		BufferedReader br=null;
		boolean dataFound=false;
		int index=0;
		
		try{
			br= new BufferedReader (new FileReader(gridlabSubstationMeter)); // Open meter file
	
			// Read current sensors 
			while (br.ready() && !dataFound) {
			     line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) {// If is not a comment or an empty line
					data=line.split(",");  // Split fields by commas
					index=0;
					String timestamp=data[index++]; // Get the timestamp
					if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
					Date lineDate=sdf.parse(timestamp); // Convert timestamp into date
					
					String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
					simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
													  // (if this step is not done, the comparison is not right)
					int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 
					
					if(comp>=0 || (comp<0 && !br.ready())) { // If read date is equal or later than current date
															// or it is a previous date but the are no more data recorded
						parseSensors(sensors,data);
						dataFound=true;
					} //if date found
			     } // if it is not a comment   
			} // while there are data to read
			
			// If no data found, get the last data read
			if(!dataFound && data!=null) {
				parseSensors(sensors,data);
			}
			
		}catch (NumberFormatException nfe){
			System.err.println("error reading line "+line);
			System.err.println(nfe);
		}catch(IOException e){	
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sensors;
	}
	
	/**
	 * Parse sensors read from a meter file
	 * @param sensors hastable to store the sensors
	 * @param vector from read the sensors values
	 * @param index index to start reading the values
	 */
	private void parseSensors(Hashtable<SensorIDS,Float> sensors,String []data){
		int index=1;
		// REAL ENERGY (measured_real_energy(Wh))
		sensors.put(SensorIDS.MEASURED_ENERGY, Float.parseFloat(data[index++]));
		
		// POWER PEAK (measured_demand(W))
		sensors.put(SensorIDS.POWER_PEAK, Float.parseFloat(data[index++]));
		
		// REAL POWER (measured_real_power(W))
		sensors.put(SensorIDS.POWER_DEMAND, Float.parseFloat(data[index++]));
		
		// REACTIVE ENERGY (measured_reactive_energy(W))
		sensors.put(SensorIDS.REACTIVE_ENERGY, Float.parseFloat(data[index++]));
		
		// REACTIVE POWER (measured_reactive_power(W))
		sensors.put(SensorIDS.REACTIVE_POWER, Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE A, REAL PART (measured_current_A.real(A))
		sensors.put(SensorIDS.CURRENT_A_REAL, Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE A, IMAGINARY PART (measured_current_A.imag(A))
		sensors.put(SensorIDS.CURRENT_A_IMAG, Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE B, REAL PART (measured_current_B.real(A))
		sensors.put(SensorIDS.CURRENT_B_REAL,  Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE B, IMAGINARY PART (measured_current_B.imag(A))
		sensors.put(SensorIDS.CURRENT_B_IMAG, Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE C, REAL PART (measured_current_C.real(A))
		sensors.put(SensorIDS.CURRENT_C_REAL, Float.parseFloat(data[index++]));
		
		// CURRENT IN PHASE C, IMAGINARY PART (measured_current_C.imag(A))
		sensors.put(SensorIDS.CURRENT_C_IMAG, Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE A, REAL PART (measured_voltage_A.real(V))
		sensors.put(SensorIDS.VOLTAGE_A_REAL,  Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE A, IMAGINARY PART (measured_voltage_A.imag(V))
		sensors.put(SensorIDS.VOLTAGE_A_IMAG, Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE B, REAL PART  (measured_voltage_B.real(V))
		sensors.put(SensorIDS.VOLTAGE_B_REAL,  Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE B, IMAGINARY PART (measured_voltage_B.imag(V))
		sensors.put(SensorIDS.VOLTAGE_B_IMAG, Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE C, REAL PART (measured_voltage_C.real(V))
		sensors.put(SensorIDS.VOLTAGE_C_REAL,  Float.parseFloat(data[index++]));
		
		// VOLTAGE IN PHASE C, IMAGINARY PART (measured_voltage_C.imag(V))
		sensors.put(SensorIDS.VOLTAGE_C_IMAG, Float.parseFloat(data[index++]));
	}
	
	/**
	 * Gets the losses sensor (LOSSES) of the output of GridLAB-D in the current simulation time.
	 * It is calculated adding line and transformation losses of the grid
	 * @param simtime current simulation time
	 * @return the grid losses value
	 */
	public float getLossesSensor(Date simtime) { 
		float losses=getLineLosses(simtime)+getTransformationLosses(simtime);
		return losses;	
	}
	
	/**
	 * Gets the grid line losses of the GridLAB-D output
	 * @param simtime current simulation time
	 * @return the grid line losses value
	 */
	private synchronized float getLineLosses(Date simtime){
		BufferedReader br=null;
		String line=null;
		String []data=null;
		boolean dataFound=false;
		float lineLosses=0;
		
		try{
			br= new BufferedReader (new FileReader(gridlabLineLosses)); // Open losses file
			
			// Read current sensors
			while (br.ready() && !dataFound) {
			     line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) {// If is not a comment or an empty line
			    	 
						data=line.split(",");  // Split fields by commas
						
						String timestamp=data[0]; // Get the timestamp
						if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
						Date lineDate=sdf.parse(timestamp); // Convert timestamp into date
						
						String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
						simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
						// (if this step is not done, the comparison is not right)
						
						int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 
															  
						if(comp>=0 || (comp<0 && !br.ready())) { /* If read date is equal or later than current date
																    or it is a previous date but the are no more data recorded */
							lineLosses=Float.parseFloat(data[1]); // LINE LOSSES in all the grid (power_losses (W))
							dataFound=true;
						}
			     }// if it is not a comment		
			}// while there are data to read
			
			// If no data found, get the last data read
			if(!dataFound && data!=null && data.length>=2) {
				lineLosses=Float.parseFloat(data[1]);
			}
			
		}catch(IOException e){
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	
		return lineLosses;
	}
	
	/**
	 * Gets the grid transformation losses of the GridLAB-D output
	 * @param simtime current simulation time
	 * @return the grid transformation losses value
	 */
	private synchronized float getTransformationLosses(Date simtime){
		BufferedReader br=null;
		String line=null;
		String []data=null;
		boolean dataFound=false;
		float transformationLosses=0;
		
		try{
			br= new BufferedReader (new FileReader(gridlabTransformationLosses)); // Open losses file
			
			// Read current sensors
			while (br.ready() && !dataFound) {
				
			     line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) { // If is not a comment or an empty line
			    	 
						data=line.split(",");  // Split fields by commas
						
						String timestamp=data[0]; // Get the timestamp
						if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
						Date lineDate=sdf.parse(timestamp); // Convert timestamp into a date
						
						String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
						simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
														  // (if this step is not done, the comparison is not right)
						
						int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 
						
						if(comp>=0 || (comp<0 && !br.ready())) { /* If read date is equal or later than current date
																    or it is a previous date but the are no more data recorded */
							transformationLosses=Float.parseFloat(data[1]); // TRANSFORMATION LOSSES in all the grid (power_losses (W))
							dataFound=true;
						} //if date found
			     } // if it is not a comment		
			}// while there are data to read
			
			// If no data found, get the last data read
			if(!dataFound && data!=null && data.length>=2) {
				transformationLosses=Float.parseFloat(data[1]);
			}
			
		}catch(IOException e){
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	
		return transformationLosses;
	}
	
	/**
	 * Checks if there is overvoltage, reading the GridLAB-D output
	 * @param output GridLAB-D output
	 * @return true if there is overvoltage, false in other case
	 */
	public synchronized boolean checkOvervoltage(String output)
	{
		boolean overvoltage=false;
		String lines[];
		String timestamp;
		lines=output.split("\n"); // Split lines
		
		for(String line:lines)
		{
			if(line.contains("WARNING") && line.contains("%") && !overvoltage)
			{
				//Getting the time
				int open=line.indexOf("[");
				int close=line.indexOf("]");
				timestamp=line.substring(open+1, close);
				
				if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim();
				
				// Notifying overvoltage
				//try {
				String messages[] = line.split(":");
				System.err.println(line/*messages[messages.length-1]*/);
				//	networkStatus.get(sdf.parse(timestamp)).overvoltageDetected(sdf.parse(timestamp));
				//} catch (ParseException e) {
				//	e.printStackTrace();
				//}
				overvoltage=true;
			}	
		}
		
		return overvoltage;
	}
	
	/**
	 * Gets the sensors of the specified transformer reading the the GridLAB-D outputs in the current simulation time
	 * @param simtime current simulation time
	 * @return the transformer sensors values, or an empty hashtable if no sensors found for this simulation time
	 * @throws SensorContainerNotFound if the transformer does not have any sensors
	 */
	public synchronized Hashtable<SensorIDS,Float> getTransformerSensors(String transformerName, Date simtime) throws SensorContainerNotFound{
		String []data=null;
		Hashtable<SensorIDS,Float> sensors = new Hashtable<SensorIDS,Float>();
		String line=null;
		BufferedReader br=null;
		boolean dataFound=false;
		int index=0;
		String meter = gridlabOutputsPath+"meter"+transformerName+".csv";
		
		File[] validNames = new File(gridlabOutputsPath).listFiles(new FileFilter(){
			@Override
			public boolean accept(File pathname) {
				if (pathname.getName().endsWith(".csv") && pathname.getName().startsWith("meter"))
					return true;
				else
					return false;
			}
			
		});
		StringBuffer validTransformerNames=new StringBuffer();
		for (File vfile:validNames){		
			validTransformerNames.append(vfile.getName().substring(5,vfile.getName().indexOf(".csv"))+";");
		}
		if (validTransformerNames.toString().indexOf(transformerName)<0){
			throw new SensorContainerNotFound("There is no element named "+transformerName+" holding sensors. Valid names are "+validTransformerNames.toString());
		}
		
		try{
			br= new BufferedReader (new FileReader(meter)); // Open meter file
	
			// Read current sensors 
			while (br.ready() && !dataFound) {
				
			     line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) {// If is not a comment or an empty line
			    	 
					data=line.split(",");  // Split fields by commas
					index=0;
					String timestamp=data[index++]; // Get the timestamp
					if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
					Date lineDate=sdf.parse(timestamp); // Convert timestamp into a date
					
					String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
					simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
													  // (if this step is not done, the comparison is not right)
					
					int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 

					if(comp>=0 || (comp<0 && !br.ready())) { // If read date is equal or later than current date
															 // or it is a previous date but the are no more data recorded
						parseSensors(sensors,data);
						dataFound=true;
					} //if date found	
			     } // if it is not a comment
			} // while there are data to read
			
			// If no data found, get the last data read
			if(!dataFound && data!=null) {
				parseSensors(sensors,data);
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("ERROR: File "+meter+" not found");
		}catch(IOException e){	
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(br!=null) br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return sensors;
	}
	
	/**
	 * Gets the sensors of the specified device reading the the GridLAB-D outputs in the current simulation time
	 * @param deviceName name of the device
	 * @param simtime current simulation time
	 * @return
	 */
	public synchronized Hashtable<SensorIDS,Float> getDeviceSensors(String deviceName, Date simtime){
		String []data=null;
		Hashtable<SensorIDS,Float> sensors = new Hashtable<SensorIDS,Float>();
		String line=null;
		BufferedReader br=null;
		boolean dataFound=false;
		int index=0;
		String meter = gridlabOutputsPath+"meter"+deviceName+".csv";
		
		try{
			br= new BufferedReader (new FileReader(meter)); /// Open meter file
	
			// Read current sensors 
			while (br.ready() && !dataFound) {
				
			     line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) { // If is not a comment or an empty line
			    	 
					data=line.split(",");  // Split fields by commas
					index=0;
					String timestamp=data[index++]; // Get the timestamp
					if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
					Date lineDate=sdf.parse(timestamp); // Convert timestamp into a date
					
					String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
					simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
					 								  // (if this step is not done, the comparison is not right)
					
					int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 

					if(comp>=0 || (comp<0 && !br.ready())) { // If read date is equal or later than current date
															// or it is a previous date but the are no more data recorded
						parseSensors(sensors,data);
						dataFound=true;
					} //if date found
			     } // if it is not a comment
			} // while there are data to read
			
			// If no data found, get the last data read
			if(!dataFound && data!=null) {
				parseSensors(sensors,data);
			}
			
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: File "+meter+" not found");
		}catch(IOException e){	
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			try {
				if(br!=null) br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sensors;
	}
	
	/**
	 * Updates the clock of the glm file
	 * @param starttime start time of the simulation in GridLAB-D format
	 * @param stoptime stop time of the simulation in GridLAB-D format
	 */
	public synchronized void updateGlmClock(String starttime, String stoptime) {
		BufferedReader br=null;
		BufferedWriter bw=null;
		String line=null;
		String data="";		
		
		try {
			br = new BufferedReader(new FileReader(grilabdGlmFile)); //Open the glm file to read   
			if(br.ready()){//Read lines until find the clock declaration
				line = br.readLine();
				while(line!=null && !line.contains("starttime")){
	            	data+=line+"\n";
	            	line = br.readLine();
	            }
			}
            
            data+="\t"+"starttime '"+starttime+"';\n"; // Write the new starttime
            line = br.readLine(); // Read a new line
            data+="\t"+"stoptime '"+stoptime+"';\n"; // Write the new stoptime
            line = br.readLine(); // Read a new line
            
            while(line!=null){ // Write lines until the end of the file
            	data+=line+"\n";
            	line = br.readLine();
            }
            
            br.close(); //Close the file
            bw = new BufferedWriter(new FileWriter(grilabdGlmFile)); //Open the glm file to write
            bw.write(data); // Write the updated data
            bw.close(); //Close the written file

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads the Energy property of a battery of its recorder file at the specified simulation time
	 * @param device name of the battery
	 * @param simtime simulation time
	 * @return the energy value of the battery at the specified time, or the last energy value registered in case of no data found for this specific time
	 */
	public synchronized String readBatteryEnergy(String device, Date simtime){
		BufferedReader br=null;
		String line=null;
		String energy=null;
		boolean dataFound=false;
		String []datos=null;
		
		try {
			br = new BufferedReader (new FileReader (gridlabOutputsPath+device+".recorder")); // Read battery recorder file
			
			// Look for energy data for current simulation time
			while (br.ready() && !dataFound) {
				line = br.readLine();
			     if(!(line.trim()).contains("#") && !(line.trim()).equals("") ) {// If it is not a comment or an empty line
			    	 
			    	datos=line.split(",");  // Split the data by commas
			    	 
			    	String timestamp=datos[0]; //Get the timestamp
					if(timestamp.contains("GMT")) timestamp=timestamp.replace("GMT", "").trim(); // Remove time zone
					Date lineDate=sdf.parse(timestamp); // Convert into a date
					
					String simtimeString = sdf.format(simtime); // Convert date into a GridLAB-D format string
					simtime=sdf.parse(simtimeString); // Convert again into date to compare dates correctly
					  								  // (if this step is not done, the comparison is not right)
					int comp=lineDate.compareTo(simtime); // Compare read date with simulation time 
					if(comp>=0 && datos.length>=2) { // If read date is equal or later than current date
						energy=datos[1];
						dataFound=true;
					}	
			     }
			}
			
			// If no data found, get the last data read
			if(!dataFound && datos!=null && datos.length>=2) {
				energy=datos[1];
			}
						
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: File "+gridlabOutputsPath+device+".recorder not found");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PatternSyntaxException e){
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if (br!=null)
				  br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return energy;
	}
	
	/**
	 * Updates the property Energy of a object battery in the .glm file
	 * @param device device name
	 * @param value new value of Energy
	 */
	public synchronized void updateBatteryEnergy(String device, String value)
	{
		BufferedReader br=null;
		BufferedWriter bw=null;
		String line=null;
		String data="";
		
		try {
				br = new BufferedReader(new FileReader(grilabdGlmFile)); //Open the file to read
	            
	            //Read lines until find the device declaration
				if(br.ready()){
					line = br.readLine();
					while(line!=null && !line.contains("name "+device)){
		            	data+=line+"\n";
		            	line = br.readLine();
		            }
				}
	            
				// Once it has found the device declaration, find the property Energy of this device
				while(line!=null && !line.contains("Energy")){
	            	data+=line+"\n";
	            	line = br.readLine();
	            }
	            
	            data+="\tEnergy "+value+";\n"; // Write the new value
	            line = br.readLine(); // Read a new line
	            
	            // Write lines until the end of the file
	            while(line!=null){
	            	data+=line+"\n";
	            	line = br.readLine();
	            }
	            
	            br.close(); //Close the file
	            
	            bw = new BufferedWriter(new FileWriter(grilabdGlmFile)); //Open the file to write
	            bw.write(data); // Write the updated data
	            bw.close(); //Close the written file
	            
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Transforms an InputStream to String
	 * @param in InputStream
	 * @return The InputStrem transformed into String
	 * @throws IOException
	 */
	private String readInputStreamAsString(InputStream in) throws IOException {
	    BufferedInputStream bis = new BufferedInputStream(in);
	    ByteArrayOutputStream buf = new ByteArrayOutputStream();
	    int result = bis.read();
	    while(result != -1) {
	      byte b = (byte)result;
	      buf.write(b);
	      result = bis.read();
	    }        
	    return buf.toString();
	}
	
	/**
	 * Prints GridLAB-D output execution into a file
	 * @param gridlabdExit File to print the output
	 * @throws IOException
	 */
	private void printOutputToFile(String gridlabdExit) throws IOException {
		FileWriter outputFile = new FileWriter(gridlabdOutput,true);
		PrintWriter pw = new PrintWriter(outputFile);
		pw.println(gridlabdExit);
		outputFile.close();
	}

}
