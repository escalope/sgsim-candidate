/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

/**
 * This class contains all the information obtained from a GridLAB-D run. It is 
 * coded according to some keywords we will call SensorID. These keywords corresponds
 * to the terminology used by GridLAB-D to name each network part. By default, 
 * all sensor data is coded into floats
 * 
 *  @author Jorge J. Gomez-Sanz
 * 	@author Nuria Cuartero-Soler
 *
 */
public class NetworkStatus {
	
	/** The substation sensors. */
	private Hashtable<SensorIDS, Float> substationSensors;
	
	/** The overvoltage. */
	private Overvoltage overvoltage;
	
	/**
	 * Instantiates a new network status.
	 */
	public NetworkStatus(){
		substationSensors=new Hashtable<SensorIDS, Float>();
		overvoltage=new Overvoltage();			
	}

	/**
	 * Get all the sensors stored for the substation
	 * @return A Hastable with the sensors and their values
	 */
	public synchronized Hashtable<SensorIDS, Float> getSubstationSensors() {
		return substationSensors;
	}

	/**
	 * Set new sensors for the substation. Remove the old stored sensors.
	 * @param substationSensors the new substation sensors and their values
	 */
	public synchronized void setSubstationSensors(Hashtable<SensorIDS, Float> substationSensors) {
		this.substationSensors = substationSensors;
	}

	/**
	 * Gets the substation sensor with the given ID.
	 * @param sensorID the sensor id to get
	 * @return the value of the sensor
	 * @throws UnknownSensor when the sensor is unknown or if there is no value stored for this sensor
	 */
	public synchronized double getSubstationSensor(SensorIDS sensorID) throws UnknownSensor{
		if (!substationSensors.containsKey(sensorID))
			throw new UnknownSensor("Sensor "+sensorID+" is not known. Valid sensors are "+substationSensors.keySet());
		return substationSensors.get(sensorID);
	}

	/**
	 * Puts a new substation sensor value 
	 * @param sensorID the sensor id
	 * @param value the new value
	 */
	public synchronized void putSubstationSensor(SensorIDS sensorID, Float value){
		substationSensors.put(sensorID,value);
	}
	
	/**
	 * Add new substation sensors. If a sensor ID already exists, its value is overwritten.
	 * @param sensors The new sensors to put and their values
	 */
	public synchronized void putSubstationSensors(Hashtable<SensorIDS, Float> sensors){
		substationSensors.putAll(sensors);
	}
	
	
	/**
	 * Sets an overvoltage state in the system.
	 *
	 * @param timestamp the current timestamp
	 */
	public synchronized void overvoltageDetected(Date timestamp){
		overvoltage.overvoltageDetected=true;
		overvoltage.timestamp=timestamp;
		System.out.println("Overvoltage at "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timestamp));		
	}
	
	/**
	 * Sets a no overvoltage state in the system.
	 */
	public synchronized void noOvervoltage(){
		overvoltage.overvoltageDetected=false;
		overvoltage.timestamp=null;
	}
	
	/**
	 * Return if there is overvoltage in the system
	 *
	 * @return true if overvoltage, false if not
	 */
	public synchronized boolean thereIsOvervoltage()
	{
		return overvoltage.overvoltageDetected;
	}
	
	/**
	 * Gets the overvoltage timestamp
	 *
	 * @return the overvoltage timestamp, null if there is no overvoltage
	 */
	public synchronized Date getOvervoltageTimestamp()
	{
		return overvoltage.timestamp;
	}
	
	/**
	 * The Class Overvoltage. It defines if there is an overvoltage state in the system.
	 */
	public class Overvoltage {
		
		/** If overvoltage detected. */
		boolean overvoltageDetected;
		
		/** The timestamp. */
		Date timestamp;
		
		public Overvoltage() {
			overvoltageDetected=false;
			timestamp=null;
		}
	}
	
	
	/**
	 * The Class UnknownSensor. Thrown when a sensor ID does not exist.
	 */
	public class UnknownSensor extends Exception {

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;

		/**
		 * Instantiates a new unknown sensor.
		 */
		public UnknownSensor() {
			super();
		}

		/**
		 * Instantiates a new unknown sensor.
		 *
		 * @param message the message
		 * @param cause the cause
		 */
		public UnknownSensor(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * Instantiates a new unknown sensor.
		 *
		 * @param message the message
		 */
		public UnknownSensor(String message) {
			super(message);
		}

		/**
		 * Instantiates a new unknown sensor.
		 *
		 * @param cause the cause
		 */
		public UnknownSensor(Throwable cause) {
			super(cause);
		}

	}

}
