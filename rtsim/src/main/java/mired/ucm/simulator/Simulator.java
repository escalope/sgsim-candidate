/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package mired.ucm.simulator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.w3c.dom.svg.GetSVGDocument;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.monitor.Event;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.NetworkStatus.UnknownSensor;
import mired.ucm.simulator.clients.Client;
import mired.ucm.simulator.orders.Order;

/**
 * Class which simulates a system that receives orders and executes them in FIFO order.
 * It uses GridLAB-D simulator to generate the results of the different orders applied and changing conditions.
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class Simulator implements Runnable{

	private boolean stop=false;
	private long TIMESTEP=100;
	private Queue<Order> pendingOrders=new ConcurrentLinkedQueue<Order>(); // order queue will be concurrently modified
	private NetworkConfiguration nc; 
	private Vector<NetworkListener> listeners;
	private Vector<Client> clients;
	private boolean intelligence; 

	/**
	 * Constructor of the class.
	 * @param paths The PATHS instance
	 * @param startDate Date to start simulation
	 * @param nes NetworkElementsStatus instance 
	 * @param rs ReadScenario instance
	 * @param gridlabActions GridlabActions instance
	 * @param intelligence Indicates if intelligence is ON (true) or OFF (false)
	 * @throws GridlabException
	 */
	public Simulator(PATHS paths, Date startDate, NetworkElementsStatus nes,ReadScenario rs,GridlabActions gridlabActions,boolean intelligence) throws GridlabException{
		this.listeners=new Vector<NetworkListener>();
		this.clients=new Vector<Client>();
		this.intelligence=intelligence;
		this.nc=new NetworkConfiguration(paths,startDate,nes,rs,gridlabActions);
	}

	/**
	 * Constructor of the class. It starts the simulation in the current clock time.
	 * @param paths The PATHS instance
	 * @param nes NetworkElementsStatus instance 
	 * @param rs ReadScenario instance
	 * @param gridlabActions GridlabActions instance
	 * @param intelligence Indicates if intelligence is ON (true) or OFF (false)
	 * @throws GridlabException
	 */
	public Simulator(PATHS paths, NetworkElementsStatus nes,ReadScenario rs,GridlabActions gridlabActions,boolean intelligence) throws GridlabException{
		this(paths,Calendar.getInstance().getTime(),nes,rs,gridlabActions,intelligence);
	}

	/**
	 * Stop the simulator.
	 */
	public synchronized void stop() {
		stop=true;
	}

	/**
	 * Queue a new order
	 * @param order The order to execute
	 */
	public synchronized void queueOrder(Order order) {
		pendingOrders.add(order);
	}

	/**
	 * Register a new listener
	 * @param nl The NetworkListener to register
	 */
	public synchronized void registerListener(NetworkListener nl){	
		listeners.add(nl);
	}

	/**
	 * Unregister a listener
	 * @param nl The NetworkListener to unregister
	 */
	public synchronized void unregisterListener(NetworkListener nl){
		listeners.remove(nl);
	}

	/**
	 * Get all the listeners registered.
	 * @return
	 */
	public synchronized Vector<NetworkListener> getListeners() {
		return listeners;
	}

	/**
	 * Adds a new client
	 * @param client The client to add
	 * @return true if the client has been added successfully, false in other case
	 */
	public synchronized boolean addClient(Client client) {
		return clients.add(client);
	}

	/**
	 * Removes a client
	 * @param client The client to remove
	 * @return true if the client has been removed successfully, false in other case
	 */
	public synchronized boolean removeClient(Client client) {
		return clients.remove(client);
	}

	/**
	 * Gets all the clients added to the simulator
	 * @return The clients
	 */
	public synchronized Vector<Client> getClients() {
		return clients;
	}

	/**
	 * Inform registered listeners of the value of a new simulation with gridlabd
	 * @param ns NetworkStatus with the value of the network sensors
	 * @param timestamp Current timestamp
	 * @throws UnknownSensor 
	 */
	public synchronized void notifyStatus(NetworkStatus ns,Date timestamp){
		if(PATHS.debugMode){ System.out.println("[Simulator] Notifying status to listeners");}
		for (NetworkListener currentListener: getListeners()){	
			currentListener.processStatus(ns.getSubstationSensors(),timestamp);				
			//If Overvoltage
			if(ns.thereIsOvervoltage())	{
				currentListener.overvoltageEvent(ns.getOvervoltageTimestamp());
			}
		}
	}

	private java.util.concurrent.ConcurrentLinkedQueue<Order> appliedOrders=new java.util.concurrent.ConcurrentLinkedQueue<Order>();

	public Vector<Order> getAppliedOrders() {
		return new Vector<Order>(appliedOrders);
	}

	public synchronized Vector<Order> getAppliedOrdersSinceAndClean(Date lasttimestamp) {

		Vector<Order> snapshot = new Vector<Order>(appliedOrders);
		Vector<Order> result=new Vector<Order>();
		
		for (Order ord:snapshot){
			if (lasttimestamp.getTime()<=ord.getExecutionTimeStamp().getTime() 
					)
				result.add(ord);
		}
		appliedOrders.removeAll(result);
		return result;
	}
	
	public synchronized Vector<Order> getAppliedOrdersSince(Date lasttimestamp) {

		Vector<Order> snapshot = new Vector<Order>(appliedOrders);
		Vector<Order> result=new Vector<Order>();
		
		for (Order ord:snapshot){
			if (lasttimestamp.getTime()<=ord.getExecutionTimeStamp().getTime() 
					)
				result.add(ord);
		}
		
		return result;
	}


	public Vector<Order> getAppliedOrdersAt(Date timestamp) {

		Vector<Order> snapshot = new Vector<Order>(appliedOrders);
		Vector<Order> result=new Vector<Order>();
		SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
		for (Order ord:snapshot){
			if (sdf.format(ord.getExecutionTimeStamp()).equals(sdf.format(timestamp)))
				result.add(ord);
		}
		return result;
	}

	private Date currentSimTime;
	private boolean overvoltage; 



	/**
	 * Execute the orders that are queued. If there are no pending orders, wait for new orders to execute.
	 */
	public void run() {
		try {
			while (!stop){
				if (!pendingOrders.isEmpty()){
					// retrieves pending orders and modifies the current configuration
					// according to the stored orders so far
					Order topOrder=pendingOrders.peek(); // Retrieve the order
					nc.apply(topOrder);
					this.setOvervoltageDetected(nc.getOvervoltageDetected());
					topOrder.setExecutionTimeStamp(getSimTime());
					appliedOrders.add(topOrder);
					pendingOrders.poll(); // Remove the top order
				}
				else{ // there are no orders to apply
					Thread.sleep(TIMESTEP);
				}
			}
			if(PATHS.debugMode){System.out.println("Stopping Simulator");}
		}catch(GridlabException e){
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void setOvervoltageDetected(boolean overvoltageDetected) {
		this.overvoltage=overvoltageDetected;

	}

	public boolean getOvervoltageDetected(){
		return overvoltage;
	}


	/**
	 * Get the pending orders to execute sent to the simulation
	 * @return The pending orders
	 */
	public Vector<Order> getPendingOrders() {
		return new Vector<Order>(pendingOrders);
	}



	/**
	 * Updates the status of all the batteries (energy value) in NetworkElementsStatus
	 * @param simtime current simulation time
	 */
	public void updateBatteriesInNetworkElementsStatus (Date simtime) {
		nc.updateBatteriesInNetworkElementsStatus(simtime);
	}

	/**
	 * Loads the current scenario depending on weather conditions and the consumption curve for each element of the network
	 * @param simtime Current simulation time
	 * @throws GridlabException
	 */
	public void loadScenario(Date simtime) throws GridlabException {
		nc.loadScenario(simtime);
	}

	/**
	 * Throw and apply a new event.
	 * @param event The event thrown
	 * @return true if the event has been applied successfully, false in other case.
	 * @throws GridlabException
	 */
	public boolean throwNewEvent(Event event) throws GridlabException {
		boolean eventApplied=false;
		eventApplied= nc.apply(event);
		if(eventApplied) {
			System.err.println("Throwing new event: "+event);
			notifyEventToListeners(event);
			if(intelligence){
				notifyEventToClients(event);
			}
		}else {
			System.err.println("Error: Event "+event+" not applied");
		}
		return eventApplied;
	}

	/**
	 * Notify a new event to the listeners
	 * @param event
	 */
	private void notifyEventToListeners(Event event) {
		for (NetworkListener currentListener: getListeners()){	
			currentListener.processEvent(event);
		}
	}

	/**
	 * Notify a new event to the clients
	 * @param event
	 */
	private void notifyEventToClients(Event event) {
		Vector<Order> ordersToQueue= new Vector<Order>();
		for (Client currentClient: getClients()){	
			ordersToQueue.addAll(currentClient.processEvent(event)); // Notify event and get orders from clients
		}

		// Queue orders
		for(Order order:ordersToQueue) {
			this.queueOrder(order);
		}
	}

	/**
	 * Checks if we are in a new GridLAB-D cycle
	 * @param simtime Current simulation time
	 * @throws GridlabException
	 */
	public synchronized void checkForNewGridlabCycle(Date simtime) throws GridlabException{
		nc.checkForNewGridlabCycle(simtime);
		currentSimTime=simtime;
	}

	public synchronized Date getSimTime(){
		return currentSimTime;
	}
}
