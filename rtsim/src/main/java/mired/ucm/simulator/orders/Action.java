/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.orders;

import java.io.Serializable;
import java.util.Date;

/**
 * Class which represents an action to apply in a device of the grid
 * 
 * @author Nuria Cuartero-Soler
 */
public abstract class Action  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/** The name of the device to apply the action*/
	protected String deviceName;
	
	/** The timestamp to apply the action */
	protected Date timestamp;
	
	protected Date executionTimeStamp;


	/**
	 * Instantiates a new action.
	 *
	 * @param deviceName the device name
	 * @param timestamp the timestamp
	 */
	public Action (String deviceName, Date timestamp) {
		this.deviceName=deviceName;
		this.timestamp=timestamp;
		executionTimeStamp=timestamp;
	}
	
	
	public Date getExecutionTimeStamp() {
		return executionTimeStamp;
	}

	public void setExecutionTimeStamp(Date executionTimeStamp) {
		this.executionTimeStamp = executionTimeStamp;
	}

	/**
	 * Gets the device name.
	 *
	 * @return the device name
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * Sets the device name.
	 *
	 * @param deviceName the new device name
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
