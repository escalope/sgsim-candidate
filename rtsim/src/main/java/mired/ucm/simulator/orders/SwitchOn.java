/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.orders;

import java.text.SimpleDateFormat;
import java.util.Date;

import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.TypesElement;
import mired.ucm.properties.PATHS;

/**
 * Class that represents an order to switch on an element
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 */
public class SwitchOn extends Order {
	
	private static final long serialVersionUID = 1L;
	protected double value; 

	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param value value to apply
	 * @param paths PATHS
	 */
	public SwitchOn(String deviceName, Date time, double value, PATHS paths) {
		super(deviceName, time, paths);
		this.value=value;
		
	}
	
	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param paths PATHS
	 */
	public SwitchOn(String deviceName, Date time, PATHS paths) {
		super(deviceName, time, paths);
	}

	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param value value to apply
	 * @param delay delay in the application of the order
	 * @param paths PATHS
	 */
	public SwitchOn(String deviceName, Date time, double value, int delay, PATHS paths) {
		super(deviceName, time, delay,paths);
		this.value=value;
	}
	
	@Override
	public void writeOrder(ElementEMS e, boolean log)
	{
		if(e.getType()==TypesElement.BATTERY) {
			writeToPlayer(deviceName+".player",value);
		}else{ //It is LOAD type 
		
			String phases=e.getPhases();
			if(phases.contains("N")) phases=phases.replace("N","").trim();
			
			for(int i=0; i<phases.length();i++){
				writeToPlayer(deviceName+"_"+phases.charAt(i)+".player",value/phases.length());
			}
		}
		
		if(PATHS.debugMode && log){
			System.out.println("APPLYING ORDER: Switching ON "+this.getDeviceName()+" in TIMESTAMP "+applyFormatToDate(timestamp)+" ("+value+")\n");
		}
		
	}

	@Override
	public double getValueToApply() {
		return value;
	}
	
	@Override
	public void setValueToApply(double newValue) {
		value=newValue;
	}
	
	@Override
	public String toString() {
		SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
		String date = sdf.format(this.getTimestamp());
		return date+"-"+sdf.format(this.executionTimeStamp)+" Switch ON "+this.getDeviceName();
	}

	@Override
	public Order getCopy() {
		return new SwitchOn(getDeviceName(),this.getTimestamp(),this.getValueToApply(),this.getPaths());
	}
}
