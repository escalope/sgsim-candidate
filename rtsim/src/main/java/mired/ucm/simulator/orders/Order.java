/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.orders;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.TypesElement;
import mired.ucm.properties.PATHS;

/**
 * Class that represents an order to apply in a device of the grid
 * 
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 */
public abstract class Order extends Action{

	private static final long serialVersionUID = -1284513596041077598L;
	protected TypesElement deviceType;
	protected int delay;
	protected String playerPath;
	protected SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //Format of GridLAB-D time simulation 
	private PATHS paths=null;

	/**
	 * Class constructor
	 * @param deviceName Name of the device where the order is applied
	 * @param timestamp Time to apply the order
	 */
	public Order(String deviceName, Date timestamp){
		super(deviceName,timestamp);
	}
	
	
	/**
	 * Class constructor
	 * @param deviceName name of the device which the order is applied
	 * @param timestamp time to apply the order
	 * @param paths
	 */
	public Order(String deviceName, Date timestamp, PATHS paths){
		super(deviceName,timestamp);
		this.deviceType=null;
		delay=0;
		if(paths!=null){
			this.paths=paths;
			playerPath=paths.getPlayers();
		}
	}
	
	/**
	 * Class constructor
	 * @param deviceName name of the device which the order is applied
	 * @param timestamp time to apply the order
	 * @param paths
	 * @param deviceType 
	 */
	public Order(String deviceName, Date timestamp, PATHS paths,TypesElement deviceType){
		super(deviceName,timestamp);
		this.deviceType=deviceType;
		delay=0;
		if(paths!=null){
			this.paths=paths;
			playerPath=paths.getPlayers();
		}
	}

	/**
	 * Class constructor
	 * @param deviceName Name of the device where the order is applied
	 * @param timestamp Time to apply the order
	 * @param paths Reference to the object that records the classpaths
	 * @param delay Time to wait before executing the order
	 */
	public Order(String deviceName, Date timestamp, int delay, PATHS paths) {
		super(deviceName,timestamp);
		this.deviceType=null;
		this.delay=delay;
		this.paths=paths;
	}

	/**
	 * 	Write the player in the specified path 
	 * @param e the element
	 * @param log log mode on (true) or off (false)
	 * @param pathPlayer new path for the player
	 */
	public void writeOrder(ElementEMS e, boolean log, String pathPlayer){
		playerPath = pathPlayer;
		writeOrder(e, log);
	}

	/**
	 * Write an order in the correspondent player file
	 * @param e the element lement
	 * @param log log mode on (true) or off (false)
	 */
	public abstract void writeOrder(ElementEMS e, boolean log);
	
	/**
	 * Get the value of the order
	 * @return Value to set 
	 */
	public abstract double getValueToApply();
	
	/**
	 * Set the value for the order
	 * @param newValue Value that gives the order
	 */
	public abstract void setValueToApply(double newValue);
	
	/**
	 * Returns a copy of the order
	 * @return A copy of the object Order
	 */
	public abstract Order getCopy();

	/**
	 * Get the type of the device where the order is applied
	 * @return type of device
	 */
	public TypesElement getDeviceType() {
		return deviceType;
	}
	
	/**
	 * Set the type of the device where the order is applied
	 * @param deviceType Type of device
	 */
	public void setDeviceType(TypesElement deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * Get path instance
	 * @return path instance
	 */
	public PATHS getPaths() {
		return paths;
	}

	/**
	 * Write the new value to apply (order) in the player file of the element
	 * @param playerName Name of the player
	 * @param value
	 */
	public void writeToPlayer(String playerName, double value){
		FileWriter player=null;
		PrintWriter pw=null;

		try{			
			player = new FileWriter(paths.getPlayers()+playerName,true);
			pw = new PrintWriter(player);
			pw.print("\n"+applyFormatToDate(timestamp)+","+value);
			player.close();
		}catch(IOException e){
			e.printStackTrace();
		} 
	}

	/**
	 * Format date
	 * @param date to format
	 * @return date in string
	 */
	public String applyFormatToDate(Date date){
		return sdf.format(date);
	}



}
