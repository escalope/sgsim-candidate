/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package mired.ucm.simulator.orders;

import java.text.SimpleDateFormat;
import java.util.Date;

import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.TypesElement;
import mired.ucm.properties.PATHS;

/**
 * Class that represents an order to set a specific value of active power to an element (positive if consumption, negative if generation)
 *
 * @author Nuria Cuartero-Soler
 * @author Sandra Garcia-Rodriguez
 */
public class SetControl extends Order {

	private static final long serialVersionUID = 1L;
	protected double value;

	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param val value to apply
	 * @param paths PATHS
	 */
	public SetControl(String deviceName, Date time, double val, PATHS paths) {
		super(deviceName, time, paths);
		value=val;
		delay=0;
	}

	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param val value to apply
	 * @param paths PATHS
	 * @param type type of the element to apply the order
	 */
	public SetControl(String deviceName, Date time, double val, PATHS paths,TypesElement type) {
		super(deviceName, time, paths,type);
		value=val;
		delay=0;
	}

	/**
	 * Constructor of the class
	 * @param deviceName device name
	 * @param time date to apply the order
	 * @param val value to apply
	 * @param paths PATHS
	 * @param delay delay in the application of the order 
	 */
	public SetControl(String deviceName, Date time, double val, PATHS paths, int delay) {
		super(deviceName, time, delay, paths);
		value= val;		
	}

	@Override
	public void writeOrder(ElementEMS e, boolean log)
	{
		if(e.getType()==TypesElement.BATTERY){
			writeToPlayer(deviceName+".player",value);
		}else{ //It is LOAD type 
			String phases=e.getPhases();
			if(phases.contains("N")) phases=phases.replace("N","").trim();

			for(int i=0; i<phases.length();i++){
				writeToPlayer(deviceName+"_"+phases.charAt(i)+".player",value/phases.length());
			}
		}

		if(PATHS.debugMode && log) {
			System.out.println("APPLYING ORDER: Setting value "+value+" to device "+this.getDeviceName()+" in TIMESTAMP "+applyFormatToDate(timestamp)+" ("+value+")\n");
		}
	}

	@Override
	public double getValueToApply() {
		return value;
	}

	@Override
	public void setValueToApply(double newValue) {
		value=newValue;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
		String date = sdf.format(this.getTimestamp());
		String message = null;

		if(getDeviceType()==null){
			message =  date+"-"+sdf.format(this.executionTimeStamp)+": Control "+Math.abs((float)getValueToApply())+" W to "+this.getDeviceName(); 
		}else{
			switch(this.getDeviceType()) {
			case BATTERY: if(getValueToApply()<0) {
				message =  date+"-"+sdf.format(this.executionTimeStamp)+": Charge "+this.getDeviceName()+" at "+Math.abs((float)getValueToApply())+" W";
			}else if(getValueToApply()>0){
				message =  date+"-"+sdf.format(this.executionTimeStamp)+": discharge"+this.getDeviceName()+" at "+Math.abs((float)getValueToApply())+" W";
			}else {
				message =  date+"-"+sdf.format(this.executionTimeStamp)+": Control "+Math.abs((float)getValueToApply())+" W to "+this.getDeviceName(); 
			}
			break;
			case GENERATOR: if(getValueToApply()!=0) {
				message =  date+"-"+sdf.format(this.executionTimeStamp)+": Activate "+this.getDeviceName()+" at "+Math.abs((float)getValueToApply())+" W";
			}else {
				message =  date+"-"+sdf.format(this.executionTimeStamp)+": Control "+Math.abs((float)getValueToApply())+" W to "+this.getDeviceName(); 
			}
			break;
			default: message = date+"-"+sdf.format(this.executionTimeStamp)+": Control "+Math.abs((float)getValueToApply())+" W to "+this.getDeviceName(); 
			break;
			}
		}
		return message;
	}

	@Override
	public Order getCopy() {
		return new SetControl(getDeviceName(),this.getTimestamp(),this.getValueToApply(),this.getPaths());
	}

}
