/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

/**
 * Exception that is thrown when the simulator is not initialized 
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class SimulatorNotInitializedException extends Exception{

	private static final long serialVersionUID = 2794496946343754326L;
	private static String defaultMessage = "Simulator not initialized";

	public SimulatorNotInitializedException() {
		super(defaultMessage);
	}

	public SimulatorNotInitializedException(String message, Throwable cause) {
		super(message, cause);
	}

	public SimulatorNotInitializedException(String message) {
		super(message);
	}

	public SimulatorNotInitializedException(Throwable cause) {
		super(cause);
	}

}
