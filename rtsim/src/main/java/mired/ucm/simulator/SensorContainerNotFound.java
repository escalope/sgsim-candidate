package mired.ucm.simulator;

public class SensorContainerNotFound extends Exception {

	public SensorContainerNotFound() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SensorContainerNotFound(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SensorContainerNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SensorContainerNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SensorContainerNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
