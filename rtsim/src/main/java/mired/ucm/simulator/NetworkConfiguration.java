/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package mired.ucm.simulator;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import mired.ucm.dynamicGLM.XMLParser;
import mired.ucm.dynamicGLM.XMLUtilities;
import mired.ucm.grid.Battery;
import mired.ucm.grid.Consumer;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.grid.TypesElement;
import mired.ucm.monitor.Event;
import mired.ucm.monitor.EventBTFalls;
import mired.ucm.monitor.EventElementFalls;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.orders.Action;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.orders.SetControl;
import mired.ucm.simulator.orders.SwitchOff;
import mired.ucm.simulator.orders.SwitchOn;

/**
 * Class that manages the execution of orders and stores the state of the network.
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 */
public class NetworkConfiguration {
	
	private NetworkElementsStatus elementsStatus; // Status of the elements of the network
	private GridlabActions gridlabActions; // To edit GridLAB-D files in execution
	private ReadScenario rs; //To read the current scenario
	
	//Properties
	private String gridlabdCommand; // Command to execute GridLAB-D
	private String originalXMLFile; // XML file of the original network
	private String networkXMLFile; // XML file of the current network
	private PATHS paths;

	// Cycle of GridLAB-D simulation
	private Calendar calendar;
	private SimpleDateFormat sdf; //Format of GridLAB-D time simulation 
	private Date starttime; //Start time of the simulation
	private Date stoptime; //Stop time of the simulation
	private int cycle; //Hours of a cycle in GridLAB-D
	private boolean overvoltage=false;

	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 * @param startDate Date to start the simulation
	 * @param nes NetworkElementsStatus instance
	 * @param rs ReadScenario instance
	 * @param gridlabActions GridlabActions instance
	 * @throws GridlabException
	 */
	public NetworkConfiguration(PATHS paths, Date startDate,NetworkElementsStatus nes,ReadScenario rs,GridlabActions gridlabActions) throws GridlabException{
		this.paths=paths;
		this.elementsStatus = nes;
		this.rs=rs;
		this.gridlabActions=gridlabActions;
		this.sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //GridLAB-D date format
		this.cycle = 8; //Hours of a cycle in GridLAB-D
		
		this.gridlabdCommand=paths.getGridLab()+" "+paths.getGlmModel();
		this.originalXMLFile=paths.getNetXmlSrcFile();
		this.networkXMLFile=paths.getXmlTemp();
		
		calendar=Calendar.getInstance();
		calendar.setTime(startDate); // Sets the starDate
		createNetwork(startDate); // Creates the network
		startSimulation(); // Sets the clock and updates the glm file
		loadScenario(startDate); // Loads the current data for energy consumption and generation in player files
	}
	
	/**
	 * Starts the first cycle of simulation. Sets the clock and updates the glm file.
	 */
	private synchronized void startSimulation(){
		starttime=calendar.getTime(); // Sets the starttime
		calendar.add(Calendar.HOUR_OF_DAY, cycle);
		stoptime= calendar.getTime(); // // Sets the stoptime "cycle" hours later
		gridlabActions.updateGlmClock(applyFormatToDate(starttime),applyFormatToDate(stoptime)); // Updates the clock in the glm file
		
		if(PATHS.debugMode){
			System.out.println("Starting GridLAB-D cycle");
			System.out.println("Starttime of the cycle: "+applyFormatToDate(starttime));
			System.out.println("Stoptime of the cycle: "+applyFormatToDate(stoptime));
			System.out.println("");
		}
	}
	
	/**
	 * Starts a new cycle of simulation (not the first). Sets the clock and updates the glm file.
	 */
	private synchronized void startNewCycle(){

		updateNetwork(stoptime); // Updates network 
		starttime=stoptime; // Sets as starttime the previous stoptime
		calendar.add(Calendar.HOUR_OF_DAY, cycle);
		stoptime= calendar.getTime(); // Sets the stoptime "cycle" hours later
		gridlabActions.updateGlmClock(applyFormatToDate(starttime),applyFormatToDate(stoptime)); // Updates the clock in the glm file
		
		if(PATHS.debugMode){
			System.out.println("Starting new GridLAB-D cycle");
			System.out.println("Starttime of the cycle: "+applyFormatToDate(starttime));
			System.out.println("Stoptime of the cycle: "+applyFormatToDate(stoptime));
			System.out.println("");
		}
	}
	
	/**
	 * Checks if we are in a new GridLAB-D cycle
	 * @param simtime Current simulation time
	 * @throws GridlabException 
	 */
	public void checkForNewGridlabCycle(Date simtime) throws GridlabException{
		// If current time is in a new GridLAB-D cycle, start new cycle
		int compare=simtime.compareTo(stoptime);
		if(compare>=0){
			startNewCycle(); // Start a new cycle
			gridlabActions.execGridlab(gridlabdCommand); // Executes GridLAB-D to update results
		}
	}
	
	/**
	 * Creates the network to simulate with GridLAB-D and loads all the elements in NetworkElementsStatus
	 * @param simtime current simulation time
	 */
	private void createNetwork(Date simtime) {

		XMLParser parser=new XMLParser(originalXMLFile,elementsStatus,paths,applyFormatToDate(simtime)); 
		parser.loadModel();  // Creates the gml file and the elements status
		File tempFilePath = new File(paths.getXmlTempPath());		
		tempFilePath.mkdirs(); // Creates the directory of the xml temporary file if it does not exists
		XMLUtilities.copyFile(originalXMLFile, networkXMLFile); // Have a copy of the original network
	}
	
	/**
	 * Loads the current scenario depending on weather conditions and the consumption curve for each element of the network
	 * @param simtime current simulation time
	 * @throws GridlabException 
	 */
	public synchronized void loadScenario(Date simtime) throws GridlabException {
		
		for(ElementEMS e:getElementsStatus().getElements()) { // For each element of the grid
			// If LOAD
			if(e instanceof Consumer) { 
				double consumption = rs.getGenerationOrLoad(e, simtime); // Getting current consumption
				writeElementPlayer(e,simtime,consumption); // Writes value in player file
				e.setCurrentPower(consumption); // Element status is updated
			
			// If GENERATOR and is ACTIVE (ON)
			} else if(e instanceof Generator && ((Generator)e).isActivated()) { 
				double maxGeneration = rs.getGenerationOrLoad(e, simtime); // Getting maximum current generation
				Generator generator = ((Generator)e);
				
				// If generator has ON/OFF control or is not controlled, generation is modified depending on weather conditions
				if(generator.getControlType()==0 || generator.getControlType()==1) { 
					writeElementPlayer(generator,simtime,maxGeneration); // Writing new generation in the player file
					generator.setCurrentPower(maxGeneration); // Element status is updated
				
				// If generator has PQ control
				} else if(generator.getControlType()==2) {
					Order lastOrder = generator.getLastOrder(); // Getting last order sent to the generator
					if(lastOrder!=null) { // If the order is not null
						// Calculating absolute values
						double currentGeneration = Math.abs(generator.getCurrentPower());
						double maximumGeneration = Math.abs(maxGeneration);
						double lastOrderValue = Math.abs(lastOrder.getValueToApply());
						
						// If current generation is lower than the value of the last given order
						if(currentGeneration<lastOrderValue) { 
							double valueToApply = (-1)*Math.min(lastOrderValue,maximumGeneration); // The smaller value of last order value and maximum generation is applied
							writeElementPlayer(generator,simtime,valueToApply); // Writes value in the player file
							generator.setCurrentPower(valueToApply); // Element status is updated
							
						// If current generation is equal to the last given order, check if it is higher than the maximum possible generation at this moment 	
						}else if(currentGeneration>maximumGeneration){
							writeElementPlayer(generator,simtime,maxGeneration); // Writes new value in the player file
							generator.setCurrentPower(maxGeneration);  // Element status is updated
						}
					}else{
						generator.setCurrentPower(maxGeneration);  // Element status is updated
					}
				}
			}
		}
		
		gridlabActions.execGridlab(gridlabdCommand); // Executes GridLAB-D to update grid status
	}
	
	/**
	 * Writes a value in the .player file of an element
	 * @param e The element to apply the new value
	 * @param simtime Date to activation
	 * @param valueToApply Value to apply
	 */
	private void writeElementPlayer(ElementEMS e,Date simtime,double valueToApply){
		Order order = new SwitchOn(e.getName(),simtime,valueToApply,paths); // It generates an order
		order.writeOrder(e, false); // Writes the new value in the player file (without log)
	}
	
	/**
	 * Applies an action to the network. 
	 * If the action is an event, applies the event changing network structure
 	 * If the action is an order, it is applied and a GridLAB-D execution is launched and its data stored. It may result in changes in the configuration of the 
	 * network
	 * @param action Action to apply
	 * @throws GridlabException 
	 */
	public synchronized boolean apply(Action action) throws GridlabException{
		boolean success=true;
		ElementEMS element=null;
		String deviceName=null;
		
		if(action instanceof Event) { // If it is an event
			success=applyEvent((Event)action); // Applying event
			
		}else{ // If it is an order
		
			try {
				Order topOrder= (Order)action;
				deviceName=topOrder.getDeviceName(); // Getting device name
				element = isAValidElement(deviceName); // Checks if it is a valid device
				
				// If element does not exist or is not controllable, an exception is thrown
				if(element==null) {
					throw new UnknownElement("WARNING: Element "+deviceName+" is not a valid one. Order aborted."); 
				}
	
				element.setLastOrder(topOrder.getCopy()); // Keeps a copy of the order
				checkOrder(topOrder,element); // Checks if order values are valid
				topOrder.setDeviceType(element.getType()); // Updates element type to show the correspondent log
				topOrder.writeOrder(element, true); // Writes the order in the player file
				String gridlabdExit=gridlabActions.execGridlab(gridlabdCommand); // Executes GridLAB-D
				boolean overvoltage=gridlabActions.checkOvervoltage(gridlabdExit); // Checks overvoltage
				this.setOvervoltageDetected(overvoltage);
			
				
				if(success) {
					updateElementStatus(topOrder,element); // Updates element status
				}
			
			} catch (UnknownElement ex) {
				System.err.println(ex.getMessage());
				success=false;
	    	} 
		}
		
		return success;	
	}
	
	public void setOvervoltageDetected(boolean overvoltage) {
		this.overvoltage=overvoltage;
		
	}
	
	public boolean getOvervoltageDetected(){
		return overvoltage;
	}

	/**
	 * Check if it is possible to apply the value of the order with current weather conditions
	 * @param order Order to apply
	 * @param element Element to apply the order
	 */
	private void checkOrder(Order order, ElementEMS element){
		double orderValue=order.getValueToApply();
	
		if(order instanceof SwitchOn && element instanceof Generator){ // ON/OFF control	
			orderValue=rs.getGenerationOrLoad(element, order.getTimestamp());
			order.setValueToApply(orderValue);
			
		}else if(order instanceof SetControl && element instanceof Generator){ // Control 2
			double currentGeneration = rs.getGenerationOrLoad(element, order.getTimestamp());
			if(Math.abs(orderValue)>Math.abs(currentGeneration)) { // If order value is higher than maximum current generation
				order.setValueToApply(currentGeneration); // Apply the maximum value
			}	
		}
	}
	
	/**
	 * Applies a new event in the network
	 * @param event the event
	 * @return true if event applied, false in other case
	 * @throws GridlabException 
	 */
	public boolean applyEvent(Event event) throws GridlabException {
		boolean eventApplied=false;
		
		if(event instanceof EventElementFalls){
			// Updates element status and generates a new XML file for the network
			if(event.isEnabled()) {
				eventApplied = elementsStatus.enableElementEvent(event.getDeviceName(),paths);
			}else{
				eventApplied = elementsStatus.disableElementEvent(event.getDeviceName(),paths); 
			}
		}else if (event instanceof EventBTFalls) {
			// Updates transformer status and generates a new XML file for the network
				if(event.isEnabled()) {
					eventApplied = elementsStatus.enableTransformerEvent(event.getDeviceName(),paths);
				}else{
					eventApplied = elementsStatus.disableTransformerEvent(event.getDeviceName(),paths);
				}
		}
		
		if(eventApplied) {
			deleteDirectory(paths.getNetFolder()); // The previous network is deleted
			XMLParser parser=new XMLParser(networkXMLFile,paths); // Loads the xml file which represents the network
			parser.loadModel(); // Creates a new glm file according to the new model
			loadScenario(event.getTimestamp()); // Loads the current scenario
			updateNetwork(event.getTimestamp()); // Updates the glm file with the data of the last GridLAB-D execution
			gridlabActions.updateGlmClock(applyFormatToDate(event.getTimestamp()),applyFormatToDate(stoptime)); // Updates the clock of the glm file
		}
		return eventApplied;
	}
	
	
	/**
	 * Updates NetworkElementsStatus with the new status of an element
	 * @param order executed order
	 * @param element element to be updated
	 */
	public synchronized void updateElementStatus(Order order, ElementEMS element) {
		
		double value = order.getValueToApply(); // Getting the value of the order (value in player file)
		
		if(element instanceof Battery) { // If battery
			Battery bateria = (Battery)element;
			String energy=gridlabActions.readBatteryEnergy(element.getName(),order.getTimestamp()); // Reading current battery energy
			if(energy!=null) {
				bateria.setEnergy(Double.parseDouble(energy)); // Setting battery Energy
			}
			bateria.setCurrentPower((-1)*value); // Setting battery power
		
		} else if(element instanceof Generator) { // If generator
			Generator generador = (Generator)element;
			generador.setCurrentPower(value); //Setting current generation
			
			if(order instanceof SwitchOff /*|| (order instanceof SetControl && order.getValueToApply()==0.0)*/) { // Setting if the generator is active or not
				generador.setActivated(false);
			} else {
				generador.setActivated(true);
			}
		}
	}
	
	/**
	 * Updates the status of all the batteries (energy value) in NetworkElementsStatus
	 * @param simtime current simulation time
	 */
	public void updateBatteriesInNetworkElementsStatus (Date simtime) {
		for(ElementEMS e:this.getElementsStatus().getControllableElements()) {
			if(e.getType().equals(TypesElement.BATTERY) && (!e.isDisabled())) { // If battery
				String batteryEnergy=gridlabActions.readBatteryEnergy(e.getName(),simtime);
				if(batteryEnergy!=null) {
					((Battery)e).setEnergy(Double.parseDouble(batteryEnergy)); // Setting battery Energy
				}
			}
		}
	}
	
	/**
	 * Updates the glm file with current state.
	 * Updates the energy of the batteries for the next execution of GridLAB-D.
	 * @param simtime
	 */
	public synchronized void updateNetwork(Date simtime){
		// Updates the value of energy (current energy) of the batteries in the glm file 
		Set<ElementEMS> elements=this.getElementsStatus().getControllableElements();
		for(ElementEMS element:elements){ 
	    	if((element instanceof Battery) && (!element.isDisabled())){ // If it is a battery and is enabled
	    		String energy=String.valueOf(((Battery)element).getEnergy()); // Getting current value of energy
	    		if(energy!=null) {
	    			gridlabActions.updateBatteryEnergy(element.getName(),energy); // Updates value in the glm file
	    		}
	    	}
		}
	}

	/**
	 * Gets the NetworkElementsStatus
	 * @return
	 */
	public synchronized NetworkElementsStatus getElementsStatus() {
		return elementsStatus;
	}

	/**
	 * Sets the NetworkElementsStatus
	 * @return
	 */
	public synchronized void setElementsStatus(NetworkElementsStatus elementsStatus) {
		this.elementsStatus = elementsStatus;
	}
	
	
	/**
	 * Check if the element is controllable and enabled
	 * @param elementName
	 * @return The Element if it is valid, null if not
	 */
	private ElementEMS isAValidElement(String elementName) {
		ElementEMS element=null;
		element=this.getElementsStatus().isAControllableElement(elementName);
		
		if(element!=null && !element.isDisabled()) {
			return element;
		}
		
		return null;
	}
	

	/**
	 * Transforms a Date to String with the GridLAB-D date format
	 * @param date
	 * @return The date in format "yyyy-MM-dd HH:mm:ss"
	 */
	public String applyFormatToDate(Date date)
	{
		return sdf.format(date);
	}
	
	/**
	 * Deletes the files of a directory
	 * @param pathname
	 */
	public void deleteDirectory(String pathname){
		File temp=null;
		
		temp = new File(pathname);
		for(File f : temp.listFiles()) {
			 f.delete();
		}
	}
	
	
	/**
	 * UnknownElement Exception
	 *
	 */
	public class UnknownElement extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public UnknownElement() {
			super();
		}

		public UnknownElement(String message, Throwable cause) {
			super(message, cause);
		}

		public UnknownElement(String message) {
			super(message);
		}

		public UnknownElement(Throwable cause) {
			super(cause);
		}

	}

}
