/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.grid.ElementEMS;
import mired.ucm.monitor.Event;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.clients.Client;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.orders.SetControl;
import mired.ucm.simulator.orders.SwitchOn;

/**
 * Class which simulates the behavior of a smart grid using cycles of simulation.
 * It simulates the advance of the time specifying the number of units of a cycle.
 * Every cycle asks to its clients for orders to send to the simulator.
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class SimulatorCycle extends Thread{
	protected boolean stop=false;
	protected Calendar simtime;
	protected Simulator simulator;
	protected Vector<Client> clients;
	protected Vector<NetworkListener> listeners;
	protected Vector<Event> events;
	protected int timeUnit;
	protected int timeAmount ;
	protected boolean pause;
	protected NetworkElementsStatus nes;
	protected ReadScenario rs;
	protected PATHS paths;
	protected boolean intelligence;
	protected boolean firstCycle;
	protected Hashtable<Date,NetworkStatus> networkStatus; // Status of the network for each simulation cycle
	protected GridlabActions gridlabActions;
	
	/**
	 * Constructor of the class
	 * @param simtime start time of simulation
	 * @param simulator simulator
	 * @param timeUnit time unit to advance in the cycles
	 * @param timeAmount time amount of every cycle
	 * @param nes network elements status
	 * @param intelligence if intelligence is ON (true) or OFF (false)
	 * @param paths PATHS instance
	 * @param rs read scenario
	 * @param gridlabActions TODO
	 */
	public SimulatorCycle(Calendar simtime, Simulator simulator, int timeUnit,	int timeAmount,NetworkElementsStatus nes,boolean intelligence,PATHS paths,ReadScenario rs, GridlabActions gridlabActions){
		this.simtime=simtime;
		this.simulator=simulator;
		this.clients=new Vector<Client>();
		this.listeners=new Vector<NetworkListener>();
		this.events=new Vector<Event>();
		this.timeUnit=timeUnit;
		this.timeAmount=timeAmount;
		this.nes=nes;
		this.intelligence=intelligence;
		this.paths=paths;
		this.firstCycle=true;
		this.rs=rs;
		this.networkStatus=new Hashtable<Date,NetworkStatus>();
		this.gridlabActions=gridlabActions;
	}
	
	/**
	 * Adds a new client
	 * @param client The client to add
	 * @return true if the client has been added successfully, false in other case
	 */
	public synchronized boolean addClient(Client client) {
		return clients.add(client);
	}
	
	/**
	 * Removes a client
	 * @param client The client to remove
	 * @return true if the client has been removed successfully, false in other case
	 */
	public synchronized boolean removeClient(Client client) {
		return clients.remove(client);
	}
	
	/**
	 * Gets all the clients added to the simulator
	 * @return The clients
	 */
	public synchronized Vector<Client> getClients() {
		return clients;
	}
	
	/**
	 * Registers a new listener
	 * @param listener The listener to add
	 * @return true if the listener has been registered successfully, false in other case
	 */
	public synchronized boolean registerListener(NetworkListener listener) {
		return listeners.add(listener);
	}

	/**
	 * Unregisters a listener
	 * @param listener
	 * @return true if the listener has been unregistered successfully, false in other case
	 */
	public synchronized boolean unregisterListener(NetworkListener listener) {
		return listeners.remove(listener);
	}

	/**
	 * Gets all the listeners registered to the simulator
	 * @return The listeners
	 */
	public synchronized Vector<NetworkListener> getListeners() {
		return listeners;
	}
	
	/**
	 * Add a new event
	 * @param event
	 * @return true if the event has been added successfully, false in other case
	 */
	public boolean addNewEvent(Event event) {
		return events.add(event);
	}
	
	/**
	 * Returns all the events 
	 * @return
	 */
	public Vector<Event> getEvents() {
		return events;
	}
	
	/**
	 * Ask for new events and send them to the simulator
	 * @param simtime Current simulation time
	 * @throws GridlabException 
	 */
	public void askForEvents(Date simtime) throws GridlabException {
		Vector<Event> appliedEvents = new Vector<Event>();
		
		for(Event event:getEvents()) {
			if(simtime.compareTo(event.getTimestamp())>=0) {
				simulator.throwNewEvent(event);
				appliedEvents.add(event);
			}
		}
		
		events.removeAll(appliedEvents);
	}


	/**
	 * Notify the status of the network and its elements to the registered listeners
	 * @param ns NetworkStatus
	 * @param timestamp Simulation time
	 */
	public void notifyStatus(NetworkStatus ns,Date timestamp){
		// Notifying status to listeners
		for (NetworkListener currentListener: getListeners()){	
			currentListener.processCycleStatus(ns,nes,timestamp);		
			if (ns.thereIsOvervoltage())
				currentListener.overvoltageEvent(ns.getOvervoltageTimestamp());
		}
	}
	
	/**
	 * Reads the sensors of the network meters and stores their values in NetworkStatus.
	 * Updates the energy values of the batteries.
	 * @param simtime current simulation time
	 */
	public synchronized void updateNetworkStatus(Date simtime) {
		// Setting substation sensors
		Hashtable<SensorIDS,Float> substationSensors = gridlabActions.getSubstationSensors(simtime);
		getNetworkStatus(simtime).putSubstationSensors(substationSensors);
		
		// Setting losses sensor
		getNetworkStatus(simtime).putSubstationSensor(SensorIDS.LOSSES, gridlabActions.getLossesSensor(simtime));
		if (this.simulator.getOvervoltageDetected())			
			getNetworkStatus(simtime).overvoltageDetected(simtime);
		
		// Setting batteries Energy
		simulator.updateBatteriesInNetworkElementsStatus(simtime);
	}
	
	/**
	 * Gets the network status (sensors) in a specific simulation time
	 * @param simtime
	 * @return NetworkStatus if there are data for this simulation time, null if not
	 */
	public synchronized NetworkStatus getNetworkStatus(Date simtime) {
		return networkStatus.get(simtime);
	}
	
	/**
	 * Puts a new NetworkStatus in the specified simulation time
	 * @param date the simulation time
	 * @param ns the new network status
	 * @return the previous network status, null if it did not have one
	 */
	public synchronized NetworkStatus setNetworkStatus(Date date, NetworkStatus ns) 
	{
		return networkStatus.put(date,ns);
	}

	/**
	 * Run the cycles of simulation. In every cycle, the system updates the current scenario, ask clients for orders and send them
	 * to the simulator. After the orders have been executed, notify listeners the new network status.
	 */
	public void run(){
		try {
			while (!stop) {
				if (!pause) {
					simulator.checkForNewGridlabCycle(simtime.getTime()); //ask simulator for a new cycle
					askForEvents(simtime.getTime()); // ask for events
					Vector<Order> orders=new Vector<Order>(); // Orders to apply at this simtime
					setNetworkStatus(simtime.getTime(), new NetworkStatus()); // Sets a new NetworkStatus at this simtime
					simulator.loadScenario(simtime.getTime());
					
					// If intelligence ON, ask clients what they want to do
					if(intelligence) {
						// Getting orders for all the clients in the current cycle
						for (Client glc:clients){ 
							orders.addAll(glc.runCycle(simtime));				
						}
						
						// Sending orders to the simulator
						for (Order order:orders){
							simulator.queueOrder(order);
						}
						
					} else if(firstCycle){ // If intelligence OFF, default orders in the first cycle
						Set<ElementEMS> generators=nes.getGenerators(); // Getting the generators of the network
				
						// Applying default orders to generators
						for(ElementEMS element:generators) {
							if(element.getControlType()==1) {
								orders.add(new SwitchOn(element.getName(),simtime.getTime(),rs.getGenerationOrLoad(element, simtime.getTime()),paths));
							}else if(element.getControlType()==2) {
								orders.add(new SetControl(element.getName(),simtime.getTime(),element.getMaxPower(),paths));
							}
						}
						
						//Sending orders to simulator
						for (Order order:orders){
							simulator.queueOrder(order);
						}
						
						firstCycle = false;
					}
					
					// wait for the orders to be processed
					while (!simulator.getPendingOrders().isEmpty()){
						Thread.sleep(100);
					}
					
					updateNetworkStatus(simtime.getTime()); // Once orders have been executed, update NetworkStatus with the value of the sensors
					notifyStatus(getNetworkStatus(simtime.getTime()),simtime.getTime()); // Notify new network status to the listeners
					Thread.sleep(500); // Time to wait while charts refresh
					simtime.add(timeUnit, timeAmount); // Advance to the next cycle
					
				} else { // if pause
					Thread.sleep(100);
				}
			}
			
			if(stop) {
				simulator.stop(); // Stops simulator
			}
			
			if(PATHS.debugMode){System.out.println("Stopping SimulatorCycle");}
		
		} catch (GridlabException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Stops the simulator
	 */
	public synchronized void stopDaemon(){
		stop=true;
	}

	/**
	 * Gets the current simulation time
	 * @return
	 */
	public synchronized Calendar getSimTime() {
		return simtime;
	}

	/**
	 * Return if simulator cycle is paused
	 * @return true if paused, false if not
	 */
	public boolean isPause() {
		return pause;
	}

	/**
	 * Set a pause value to the simulator cycle
	 * @param pause true to pause simulator, false to resume
	 */
	public void setPause(boolean pause) {
		this.pause = pause;
	}
}
