/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.block.ColumnArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RefineryUtilities;
import org.jfree.util.ShapeUtilities;

/**
 * Class which shows a comparative of the results of several concurrent simulations
 * @author Nuria Cuartero-Soler
 *
 */
public class ConcurrentSimulationsResults extends Thread{
	private static final int SECONDS_TO_WAIT_FOR_DATA=10; // Seconds to wait for new data
	private int MOMENTS; // 
	private int numberOfSimulations;
	private boolean stop; // Stop condition
	private StateWindow simulationChart; // Simulation comparative chart
	private ArrayList<SimulatorDisplayer> simulations; // Simulations running
	private boolean pause; // Pause condition
	private ApplicationFrame summaryFrame;
	
	/* Default colors for series */
	private Color[] seriesColors=  {new Color(153,0,0), // Red
									new Color(34,139,34), // Green
									new Color(0,0,255), // Blue
									new Color(218,165,32), // Orange
									new Color(255,0,255), // Magenta
									};

	/* Default shapes for series. The shapes have different sizes to make them visible if several series overlap */
	private Shape[] seriesShapes=  {ShapeUtilities.createDiamond(3), //Height=6
									ShapeUtilities.createUpTriangle((float) 3.5), //Height=7
									new Ellipse2D.Double(-4, -4, 8, 8),
									new Rectangle2D.Double(-4.5, -4.5, 9, 9) 
									};
	
	
	/**
	 * Constructor of the class.
	 * @param simulations The collection of simulations to show the comparative
	 * @param timeAmount Number of minutes of a cycle of simulation
	 * @param date Date to start the simulation
	 * @param moments Number of moments to show in the chart
	 * @param minRange Minimum range of the y axis of the chart. If null, the axis range is automatically adjusted to fit the data.
	 * @param maxRange Maximum range of the y axis of the chart. If null, the axis range is automatically adjusted to fit the data.
	 */
	public ConcurrentSimulationsResults(ArrayList<SimulatorDisplayer> simulations,int timeAmount, Date date, int moments, Double minRange, Double maxRange){
		this.MOMENTS=moments;
		this.simulations=simulations;
		this.numberOfSimulations=simulations.size();
		stop=false;
		
		// Starts simulations and locate frames in cascade
		int x=0;
		int y=0;
		for(int i=0;i<simulations.size();i++){
			JFrame frame=simulations.get(i).getMainFrame();
			frame.setLocation(x, y); // Locates frame
			x+=40;
			y+=40;
			//simulations.get(i).start(); // Starts simulation
		}
		
		// Initial data
		ArrayList<float[]> newData = new ArrayList<float[]>();
		
		for(int i=0; i<numberOfSimulations; i++) {
			newData.add(new float[MOMENTS]);
		}
		
        for(int i=0; i<numberOfSimulations; i++) {
        	for(int j=0; j<MOMENTS; j++) {
	        	try {
					newData.get(i)[j]=getData(i).take(); // Wait for available simulation data 
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
        	}
        }
        
        // Creating dataset with its series
        RegularTimePeriod period = new MultipleOfMinute(timeAmount,date);
        final MinuteDTSC simulationChartDataset = new MinuteDTSC(numberOfSimulations, MOMENTS, period);
        simulationChartDataset.setTimeBase(period);
        for(int i=0; i<numberOfSimulations; i++){
        	simulationChartDataset.addSeries(newData.get(i), i, simulations.get(i).getTitle());
        }

        // Creating comparative chart of simulations' power demand
        simulationChart=new StateWindow("Comparative Chart",simulationChartDataset);
        simulationChart.createChart("Substation Demand", "Time", "kW", minRange, maxRange);
        
        // Changing series color and symbols
        XYPlot plot = (XYPlot) simulationChart.getChart().getPlot();
        for(int i=0; i<numberOfSimulations; i++) {
      	  Color color = getSeriesColor(i);
      	  Shape shape = getSeriesShape(i);
      	  plot.getRenderer().setSeriesPaint(i, color);
      	  plot.getRenderer().setSeriesShape(i, shape);
        }
        plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF)); //Background color
        
        // Legend format
        simulationChart.getChart().removeLegend();
        LegendTitle legend = new LegendTitle(simulationChart.getChart().getPlot(), new ColumnArrangement(),new ColumnArrangement());
        legend.setPosition(RectangleEdge.BOTTOM);
        legend.setFrame(new BlockBorder(Color.black));
        simulationChart.getChart().addLegend(legend);
        
        // Pause button
 		final JButton jbPause=new JButton("Pause");
 		jbPause.setPreferredSize(new Dimension(200, jbPause.getPreferredSize().height)); // Changing size
 		jbPause.addActionListener(new ActionListener() {

 			@Override
 			public void actionPerformed(ActionEvent arg0) {
 				if(pause){
 					pause=false;
 					jbPause.setText("Pause");
 				}else{
 					pause=true;
 					jbPause.setText("Resume");
 				}				
 			}
 		});
 		
 		jbPause.setVisible(true);
 		
 		
 		//Save as SVG button
 		JButton saveAsSvgButton = simulationChart.createSaveAsSvgJButton();
 		saveAsSvgButton.setVisible(true);
 		
 		// Buttons Panel
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
        buttonsPanel.add(jbPause);
 		buttonsPanel.add(saveAsSvgButton);
 		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setVisible(true);
 		simulationChart.add(BorderLayout.SOUTH,buttonsPanel);
        
	}
	
	/**
	 * Runs the chart
	 */
	public void run(){
		//Show chart
		simulationChart.start();
		ApplicationFrame inicio = new ApplicationFrame("Smart Grid Simulator UCM");
        inicio.getContentPane().add(simulationChart);
        inicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //inicio.setAlwaysOnTop(true);
        inicio.pack();
        RefineryUtilities.centerFrameOnScreen(inicio);
		inicio.setVisible(true);
        
		// Updating comparative chart with simulation data
        while(!stop){
        	if(!pause){
	        	float updateData[]= new float[this.numberOfSimulations];
	        	for(int i=0; i<numberOfSimulations && !stop;i++){
	        		boolean queueEmpty=getData(i).isEmpty();
	        		boolean simulationEnded=simulations.get(i).getGridLabDiscreteEventSimulator().isSimulationEnded();
	        		
	        		try {
		        		if(queueEmpty && simulationEnded) {
		        			stop=true;
		        		}else if(simulationEnded && !queueEmpty){
		        			updateData[i] = getData(i).take(); //Take data
		        		}else{
		        			Float value=null;
		        			while(!simulations.get(i).getGridLabDiscreteEventSimulator().isSimulationEnded() && value==null){
		        				value = getData(i).poll(SECONDS_TO_WAIT_FOR_DATA, TimeUnit.SECONDS); //Wait for data
			        			//value = getData(i).take(); //Wait for data
			        			if(value!=null) {
			        				updateData[i]=value;
			        			}
		        			}
								
		        		}
	        		} catch (InterruptedException e) {
						e.printStackTrace();
					}
	        	}
	        	
	        	if(!stop){
		        	simulationChart.getDataset().advanceTime(); // Advance to the next simtime
		            simulationChart.getDataset().appendData(updateData); //Append new data to the chart dataset
	        	}
        	}
       }
        
       createSummaryFrame(); // Show summary frame
         
     // Wait for frame to close
     while (summaryFrame.isVisible()){
         try {
             Thread.sleep(1000);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
     }
  
     System.out.println("SIMULATION FINISHED");
	}

	/**
	 * Get the demand curve data of the specified simulation
	 * @param simulation The index of the simulation
	 * @return A BlockingQueue with the data
	 */
	public BlockingQueue<Float> getData(int simulation) {
		return simulations.get(simulation).getDemandCurve();
	}
	
	/**
	 * Creates the summary frame to compare simulations results
	 */
	public void createSummaryFrame(){
		 // Defining datasets categories 
		 final CategoryDataset datasetSummary = new DefaultCategoryDataset();
		 final String categoryImportedEnergy = "Imported energy (kWh)";
	     final String categoryExportedEnergy = "Exported energy (kWh)";
	     final String categoryLosses = "Losses (kWh)";
	     final String categoryFactura = "Bill (€)";
	     
	     
	     // Define Energy dataset
	     for(int i=0; i<numberOfSimulations; i++) {
	    	 ((DefaultCategoryDataset) datasetSummary).addValue(simulations.get(i).getImportedEnergy(),simulations.get(i).getTitle(),categoryImportedEnergy);
	    	 ((DefaultCategoryDataset) datasetSummary).addValue(simulations.get(i).getExportedEnergy(), simulations.get(i).getTitle(),categoryExportedEnergy);
	    	 ((DefaultCategoryDataset) datasetSummary).addValue(simulations.get(i).getEnergyLosses(), simulations.get(i).getTitle(),categoryLosses);
	    	 ((DefaultCategoryDataset) datasetSummary).addValue(null,simulations.get(i).getTitle(),categoryFactura);
	     }
	     
	     // Define Bill dataset
	     final CategoryDataset datasetFactura = new DefaultCategoryDataset();
	     for(int i=0; i<numberOfSimulations; i++) {
	    	 ((DefaultCategoryDataset) datasetFactura).addValue(null,simulations.get(i).getTitle(),categoryImportedEnergy);
	    	 ((DefaultCategoryDataset) datasetFactura).addValue(null, simulations.get(i).getTitle(),categoryExportedEnergy);
	    	 ((DefaultCategoryDataset) datasetFactura).addValue(null,simulations.get(i).getTitle(),categoryLosses);
	    	 ((DefaultCategoryDataset) datasetFactura).addValue(simulations.get(i).getBill(),simulations.get(i).getTitle(),categoryFactura);
	     }
	     
         // Plot customization
	     final CategoryAxis domainAxis = new CategoryAxis("");
	     final NumberAxis rangeAxis = new NumberAxis("kWh");
	     final BarRenderer renderer1 = new BarRenderer();
	     final CategoryPlot plot = new CategoryPlot(datasetSummary, domainAxis, rangeAxis, renderer1) {
	            
			private static final long serialVersionUID = 1L;

				/**
	             * Override the getLegendItems() method to handle special case.
	             * As there are two renderer (and two axis), the series are duplicated. 
	             * The legend must show only one item of each series.
	             *
	             * @return the legend items of only one renderer
	             */
	            public LegendItemCollection getLegendItems() {

	                final LegendItemCollection result = new LegendItemCollection();

	                // Returns series of dataset 0
	                final CategoryDataset data = getDataset(0);
	                if (data != null) {
	                    final CategoryItemRenderer r = getRenderer(0);
	                    if (r != null) {
	                    	for(int i=0; i<numberOfSimulations; i++) {
	                    		result.add(r.getLegendItem(0, i));
	                    	}
	                    }
	                }

	                return result;
	            } 
	        };
	      
          plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF)); // Changing background color
          plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
          
          
          // Including bill dataset in the char
          plot.setDataset(1, datasetFactura);
          plot.mapDatasetToRangeAxis(1, 1);
          
          // Including second axis
          final ValueAxis axis2 = new NumberAxis("€"); 
          plot.setRangeAxis(1, axis2);
          plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);
          
          // Including price bar
          final BarRenderer renderer2 = new BarRenderer(); 
          plot.setRenderer(1, renderer2);
          
          // To show labels of values for each bar
          plot.getRenderer(0).setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator()); 
          plot.getRenderer(0).setBaseItemLabelsVisible(true);
          
          plot.getRenderer(1).setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator()); 
          plot.getRenderer(1).setBaseItemLabelsVisible(true);
          
          // Changing series color
          for(int i=0; i<numberOfSimulations; i++) {
        	  Color color = getSeriesColor(i);
        	  plot.getRenderer(0).setSeriesPaint(i, color);
        	  plot.getRenderer(1).setSeriesPaint(i, color);
          }
          
          // Creates JFreechart
	      final JFreeChart chart = new JFreeChart("Results Summary",null,plot,false);
	      chart.setBackgroundPaint(Color.white);
     
         // Specifying legend format
         LegendTitle legend = new LegendTitle(plot, new ColumnArrangement(),new ColumnArrangement());
         legend.setPosition(RectangleEdge.BOTTOM);
         legend.setFrame(new BlockBorder(Color.black));
	     chart.addLegend(legend);
	  
         // Including the chart into a frame
	     summaryFrame = new JFreeChartFrameSvgSaving("Summary",chart, true);
	     summaryFrame.setSize(1200, 500);
	     summaryFrame.setLocationRelativeTo(null);
	}
	
	/**
	 * Get the default color of a specified series
	 * @param series The number of the series
	 * @return The Color
	 */
	private Color getSeriesColor(int series){
		if(series<seriesColors.length) {
			return seriesColors[series];
		}else{
			return seriesColors[series%seriesColors.length];
		}
	}
	
	/**
	 * Get the default shape of a specified series
	 * @param series The number of the series
	 * @return The Shape
	 */
	private Shape getSeriesShape(int series){
		if(series<seriesColors.length) {
			return seriesShapes[series];
		}else{
			return seriesShapes[series%seriesShapes.length];
		}
	}

}
