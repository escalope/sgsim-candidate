/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.grid.Battery;
import mired.ucm.grid.Consumer;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.grid.Transformer;
import mired.ucm.monitor.Event;
import mired.ucm.simulator.NetworkElementsStatus;
import mired.ucm.simulator.NetworkStatus;
import mired.ucm.simulator.SensorIDS;

/**
 * Class which displays a chart with a table of CTs and the state of its elements
 * @author Nuria Cuartero-Soler
 *
 */
public class CtsDisplayer extends JInternalFrame implements NetworkListener{
	
	private static final long serialVersionUID = -818790793222711137L;
	private Vector<Component> elementStateComponents;
	private Vector<JLabel> labelComponents;
	private Vector<JTextArea> orders;
	private Hashtable<String,Vector<Component>> ctsComponents;
	private Vector<JPanel> ctsPanel; // Panel that includes all the CTs 
	private JPanel mainPanel;

	/**
	 * Constructor of the class
	 * @param title Title of the frame
	 */
	public CtsDisplayer(String title){
		super(title);
		mainPanel=new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		getContentPane().add(new JScrollPane(mainPanel));
		elementStateComponents=new Vector<Component>();
		labelComponents=new Vector<JLabel>();
		orders=new Vector<JTextArea>();
		ctsComponents=new Hashtable<String,Vector<Component>>();
		ctsPanel=new Vector<JPanel>();
	}
	
	/**
	 * Get the state components of all the elements
	 * @return
	 */
	public Vector<Component> getElementStateComponents() {
		return elementStateComponents;
	}
	
	/**
	 * Get the JLabel of all the elements
	 * @return
	 */
	public Vector<JLabel> getLabelComponents() {
		return labelComponents;
	}

	/**
	 * Get the state component with the specified name
	 * @param name the name of the Component
	 * @return The Component, or null if the name does not exists
	 */
	public Component getStateComponent(String name) {
		for(Component comp:elementStateComponents) {
			if(comp.getName().equals(name)) {
				return comp;
			}
		}
		
		return null;
	}
	
	/**
	 * Get the JLabel of the frame with the specified name
	 * @param name the name of the JLabel
	 * @return The JLabel, or null if the name does not exists
	 */
	public JLabel getLabel(String name) {
		for(JLabel comp:labelComponents) {
			if(comp.getName().equals(name)) {
				return comp;
			}
		}
		
		return null;
	}
	
	/**
	 * Get the JTextArea of a transformer
	 * @param name The name of the transformer
	 * @return The JTextArea, or null if the name is not found
	 */
	public JTextArea getJTextAreaOrders(String name) {
		for(JTextArea comp:orders) {
			if(comp.getName().equals(name)) {
				return comp;
			}
		}
		
		return null;
	}
	
	/**
	 * Get all the components of the specified transformer
	 * @param name The name of the transformer
	 * @return The components
	 */
	public Vector<Component> getCtComponents(String name) {
		return this.ctsComponents.get(name);
	}

	/**
	 * Add a new CT with its elements to the panel
	 * @param ctName the name of the CT
	 * @param elements a Hashtable with the pairs <Element, power_value>
	 */
	public void addCt(String ctName, Hashtable<ElementEMS,String> elements){
		// Vector of components of the CT
		Vector<Component> ctComponents = new Vector<Component>();
		
		// Panel for the entire CT
		JPanel ctPanel = new JPanel(); 
		ctPanel.setName(ctName);
		ctPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		ctPanel.setLayout(new BorderLayout());
		
		// Panel for the title of the CT
		JPanel ctTitlePanel = new JPanel(); 
		
		JLabel labelCt = new JLabel();
		labelCt.setText("Transformer "+ctName);
		labelCt.setForeground(new Color(0,0,150));

		ctTitlePanel.setAlignmentX(CENTER_ALIGNMENT);
		ctTitlePanel.setAlignmentY(CENTER_ALIGNMENT);
		//ctTitlePanel.setBackground(new Color(0,0,200));
		//ctTitlePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		ctTitlePanel.add(labelCt);
		ctPanel.add(ctTitlePanel, BorderLayout.NORTH);
		
		ctComponents.add(labelCt); // To have external access
		
		// Panel for the elements of the CT
		JPanel ctElementsPanel = new JPanel(); 
		//SpringLayout springLayout = new SpringLayout();
		GridBagLayout gridBagLayout = new GridBagLayout();
		ctElementsPanel.setLayout(gridBagLayout);
		
		int row=0;
		for(ElementEMS element:elements.keySet()) { // For each element
			
			GridBagConstraints constraints = new GridBagConstraints();
			
			// Type of control of the element
			JLabel labelControl = new JLabel();
			switch(element.getControlType()){
				case 0: labelControl.setText("-"); break;
				case 1: labelControl.setText("ON/OFF"); break;
				case 2: labelControl.setText("PQ"); break;
			}
			labelControl.setForeground(new Color(230,89,54));
			labelControl.setPreferredSize(new Dimension(45,25));
			labelControl.setMaximumSize(labelControl.getPreferredSize());
			labelControl.setMinimumSize(labelControl.getPreferredSize());
			labelControl.setHorizontalAlignment(JLabel.CENTER);
			constraints.gridx=0; //Constraints of the grid
			constraints.gridy=row;
			constraints.ipadx=10;
			constraints.ipady=2;
			constraints.insets= new Insets(2, 2, 2, 2);
			constraints.anchor=GridBagConstraints.CENTER;
			ctElementsPanel.add(labelControl,constraints);
			
			// Title of the element
			JLabel elementName = new JLabel(element.getName(),JLabel.TRAILING); 
			elementName.setName(element.getName());
			elementName.setPreferredSize(new Dimension(145,25));
			elementName.setMaximumSize(elementName.getPreferredSize());
			elementName.setMinimumSize(elementName.getPreferredSize());
			labelComponents.add(elementName);
			ctComponents.add(elementName);
			constraints.gridx=1; //Constraints of the grid
			constraints.gridy=row;
			constraints.anchor=GridBagConstraints.CENTER;
			constraints.ipadx=10;
			constraints.ipady=2;
			ctElementsPanel.add(elementName,constraints);
			elementName.setHorizontalAlignment(JLabel.CENTER);
			
			// State of the element
			//UIManager.put("ProgressBar.foreground",Color.GREEN);
			final JProgressBar progressBar=new JProgressBar(0,100);
			progressBar.setValue(0);
			progressBar.setStringPainted(true);
			progressBar.setName(element.getName()+"S");
			progressBar.setPreferredSize(new Dimension(150,25));
			progressBar.setMinimumSize(progressBar.getPreferredSize());
			progressBar.setMaximumSize(progressBar.getPreferredSize());
			constraints.gridx=2; //Constraints of the grid
			constraints.gridy=row;
			constraints.anchor=GridBagConstraints.EAST;
			constraints.ipadx=10;
			constraints.ipady=2;
			ctElementsPanel.add(progressBar,constraints);
			elementStateComponents.add(progressBar);
			ctComponents.add(progressBar);
			row++;
		}
		
		/*SpringUtilities.makeCompactGrid(ctElementsPanel,
				elements.keySet().size(), 3, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad*/
		
		ctPanel.add(ctElementsPanel, BorderLayout.SOUTH);
		
		// Area to show the orders of the CT
		JTextArea textAreaOrders= new JTextArea(5, 30);
		textAreaOrders.setName(ctName);
		textAreaOrders.setBackground(Color.black);
		textAreaOrders.setForeground(Color.white);
		JScrollPane scrollPane = new JScrollPane(textAreaOrders);
		ctPanel.add(scrollPane, BorderLayout.CENTER);
		orders.add(textAreaOrders);
				
		this.mainPanel.add(ctPanel); // Adding CT panel to main panel
		ctsPanel.add(ctPanel);
		this.ctsComponents.put(ctName, ctComponents);
		
	}
	
	/**
	 * Shows the frame
	 */
	public void start() {		
    	this.setResizable(true);
		this.setClosable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
		this.pack();
        this.setVisible(true);
	}
	
	/**
	 * Removes all the CTs of the frame
	 */
	public void removeAllCts(){
		for(JPanel panel:ctsPanel){
			mainPanel.remove(panel);
		}
		ctsPanel.clear();
	}

	@Override
	public void processStatus(Hashtable<SensorIDS, Float> sensorValues,
			Date timestamp) {
		
	}

	@Override
	public void overvoltageEvent(Date timestamp) {
		
	}

	@Override
	public void processCycleStatus(NetworkStatus ns, NetworkElementsStatus nes,	Date timestamp) {
        
		// Update the frame with the results of a new cycle
        Set<Transformer> transformers = nes.getTransformers();
        
        for(Transformer trafo:transformers) { // For every trafo
        	
        	if(trafo.isEnabled()) { // Enabled trafo
        		// Enable the trafo
        		Vector<Component> ctComponents = getCtComponents(trafo.getName());
        		if(ctComponents!=null) {
	        		for(Component c:ctComponents) {
	        			c.setEnabled(true);
	        		}
        		}
        		
        		Set<ElementEMS> trafoElements = nes.getElementsByTransformer(trafo.getName()); // Getting elements of the trafo
        		
        		for(ElementEMS e:trafoElements){ // For every element of the transformer
        			if(e instanceof Generator || e instanceof Battery) {
	        			JLabel jLabelName = getLabel(e.getName());
	        			JProgressBar progressBar = (JProgressBar)getStateComponent(e.getName()+"S"); // JProgressBar which represents the element
	                	
	                	if(jLabelName!=null && progressBar!=null) { //If the elements exists in the table
	                	
		        			if(e.isDisabled()) { // If the element is disabled, disable the entire panel and its component
		            			jLabelName.setEnabled(false);
		            			progressBar.setValue(100);
		            			progressBar.setBackground(Color.gray);
		            			progressBar.setForeground(Color.gray);
		            			progressBar.setString("DISABLED");
		            			progressBar.setEnabled(false);
		            			
		            		}else { // If the element is enable
		            			
		            			// Enable the components of the panel
		            			jLabelName.setEnabled(true);
		            			progressBar.setEnabled(true);
		            			
		            			// Updating data
		    		        	if(e instanceof Battery) { //If Battery
		    		        		Battery bateria = (Battery)e; //Battery element
		    		        		int energy=(int) ((bateria.getEnergy()*100)/bateria.getMaxEnergy()); // Current energy of the battery
		    		        		progressBar.setValue(energy); // Updating current energy in the progress bar
		    		        		String message = energy+"% "; // Message for the bar
		    		        		
		    		        		if(bateria.getEnergy()>0.0) { // If energy > 0
			    		        		if(bateria.getCurrentPower()>0) {
			    		        			message+="Charging...";
			    		        		}
			    		        		else if(bateria.getCurrentPower()<0) {
			    		        			message+="Discharging...";
			    		        		}
		    		        		}
		    		        		
		    		        		progressBar.setString(message); // Writting message in bar
		    		        		progressBar.setBackground(new Color(238,238,238));
		    		        		progressBar.setForeground(new Color(163,184,204));
		    		        		
		    		        		
		    		        	}else if (e instanceof Generator) { // If Generator
	
		    		        		if(((Generator)e).isActivated()) { // Generator ON
		    	            			
		    		        			Generator generador = (Generator)e;
		    		        			int power = (int) ((int)(generador.getCurrentPower()*100)/generador.getMaxPower());
		    		        			progressBar.setValue(power);
		    		        			progressBar.setString(String.valueOf(Math.abs((float)generador.getCurrentPower()))+" W");
		    		        			progressBar.setBackground(null);
		    		        			progressBar.setForeground(new Color(34,139,34)); // Green
		    		    				
		    		        		} else { // Generator OFF
		    		        			progressBar.setString("OFF");
		    		        			progressBar.setValue(100);
		    		        			progressBar.setForeground(new Color(178,34,34)); // Red color
		    		        		}
		    		
		    		        		
		    		        		
		    		        	}else if (e instanceof Consumer) { //If load
		    		        		// For future cases with controllable loads
		    		        	}
		            		}
		        			
	                	}else { // The element is not in the table
	                		System.err.println("It does not exist any table for "+e.getName());
	                	}
	        			
	        		}
        		}
        		
        	}else { // Disabled transformer
        		Vector<Component> ctComponents = getCtComponents(trafo.getName());
        		if(ctComponents!=null) {
	        		for(Component c:ctComponents) {
	        			c.setEnabled(false);
	        			if(c instanceof JProgressBar){
	        				JProgressBar pb = (JProgressBar)c;
	        				pb.setValue(100);
	        				pb.setValue(100);
	        				pb.setBackground(Color.gray);
	        				pb.setForeground(Color.gray);
	        				pb.setString("DISABLED");
	        			}
	        		}
        		}
        	}
        }
		
	}

	@Override
	public void processEvent(Event event) {
		
	}

}
