/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.displayer;

import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;

/**
 * DynamicTimeSeriesCollection which allows to use a MultipleOfMinute time period in a JFreeChart
 * @author Nuria Cuartero-Soler
 *
 */
public class MinuteDTSC extends DynamicTimeSeriesCollection{
	
	private static final long serialVersionUID = 1L;

		public MinuteDTSC(int nSeries, int nMoments, RegularTimePeriod timePeriod){
			  super(nSeries, nMoments, timePeriod);
			  if(timePeriod instanceof MultipleOfMinute){
			    this.pointsInTime = new MultipleOfMinute[nMoments];
			  }else if (timePeriod instanceof Minute) {
			    this.pointsInTime = new Minute[nMoments];
			  } 
		  }

}
