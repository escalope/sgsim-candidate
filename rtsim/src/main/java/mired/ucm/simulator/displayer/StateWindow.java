/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

/**
 * Class which generates a JInternalFrame with a Dynamic Time Series Chart.
 * It includes the functionality of creating a button to save the chart in SVG format.
 * @author Nuria Cuartero-Soler
 *
 */
public class StateWindow extends JInternalFrame {

	private static final long serialVersionUID = -8403419657071556752L;
	private final DynamicTimeSeriesCollection dataset;
	private JFreeChart chart;
	private ChartPanel chartPanel;

	/**
	 * Constructor of the class
	 * @param title Title of the frame
	 * @param dataset Dataset of the chart
	 */
	public StateWindow(String title,final DynamicTimeSeriesCollection dataset){
		super(title);
		this.dataset=dataset;
	}

	/**
	 * Creates a new JFreeChart.
	 * @param title Title of the chart
	 * @param xTitle Title of the x axis
	 * @param yTitle Title of the y axis
	 * @param minRange Minimum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @param maxRange Maximum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @return the created chart
	 */
	public JFreeChart createChart(String title, String xTitle, String yTitle, Double minRange, Double maxRange) {
		chart = ChartFactory.createTimeSeriesChart(
				title, xTitle, yTitle, dataset, true, true, false);
		final XYPlot plot = chart.getXYPlot();
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		//        renderer.setSeriesLinesVisible(1, false);
		//       renderer.setSeriesShapesVisible(1, false);
		renderer.setAutoPopulateSeriesShape(true);

		plot.setRenderer(renderer);
		ValueAxis domain = plot.getDomainAxis();
		domain.setAutoRange(true);

		if(minRange!=null && maxRange!=null){ // Set maximum and minimum range
			ValueAxis range = plot.getRangeAxis();
			range.setRange(minRange, maxRange);
		}

		chartPanel=new ChartPanel(chart);
		this.getContentPane().add(chartPanel, BorderLayout.CENTER);
		return chart;
	}

	/**
	 * Get the dataset of the JFreeChart
	 * @return The DynamicTimeSeriesCollection
	 */
	public DynamicTimeSeriesCollection getDataset() {
		return dataset;
	}

	/**
	 * Get the chart
	 * @return The chart if created, null in other case
	 */
	public JFreeChart getChart() {
		return chart;
	}

	/**
	 * Shows the frame
	 */
	public void start() {
		this.pack();
		this.setVisible(true);
		this.setResizable(true);
		this.setClosable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
	}

	/**
	 * Exports a JFreeChart to a SVG file.
	 * 
	 * @param chart JFreeChart to export
	 * @param bounds the dimensions of the viewport
	 * @param svgFile the output file.
	 * @throws IOException if writing the svgFile fails.
	 */
	void exportChartAsSVG(File svgFile) throws IOException {
		//Getting the area to draw
		Rectangle bounds=chartPanel.getChartRenderingInfo().getPlotInfo().getPlotArea().getBounds(); // Get plot area
		// Calculating new area with the coordinates of the content pane (getPlotArea() does not return the right coordinates) 
		Rectangle calculatedBounds=new Rectangle(this.getContentPane().getX(),this.getContentPane().getY(),bounds.width,bounds.height);

		// Get a DOMImplementation and create an XML document
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
		Document document = domImpl.createDocument(null, "svg", null);

		// Create an instance of the SVG Generator
		SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

		// Draw the chart in the SVG generator
		chart.draw(svgGenerator, calculatedBounds);

		// Write svg file
		OutputStream outputStream = new FileOutputStream(svgFile);
		Writer out = new OutputStreamWriter(outputStream, "UTF-8");
		svgGenerator.stream(out, true /* use css */);						
		outputStream.flush();
		outputStream.close();
	}

	/**
	 * Generates a JButton with the functionality of saving the chart area as SVG file
	 * @return The JButton to save SGV files
	 */
	public JButton createSaveAsSvgJButton(){
		JButton saveAsSvgButton = new JButton("Save chart as SVG...",new ImageIcon(getClass().getResource("/images/save.gif")));
		saveAsSvgButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					final JFileChooser fc = new JFileChooser(); // Dialog to choose the file
					fc.setFileFilter(new FileNameExtensionFilter("SVG Image (*.svg)", "svg")); // Only svg files
					int returnVal=fc.showSaveDialog(null); //Show dialog

					if (returnVal == JFileChooser.APPROVE_OPTION) { 
						File file = fc.getSelectedFile();
						if (!file.getName().endsWith(".svg")) { //Check if file has the right extension
							file = new File(file.getCanonicalPath()+".svg");
						}
						if (file.exists()) { // Check if the file already exists
							if (JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(
									null,
									"Do you want to overwrite the file \""+file.getName()+"\"?","File Already Exists",
									JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
									null,
									new Object[]{"Yes","No"},  "No")) {
								exportChartAsSVG(file); // Export as svg
							}
						}else{
							exportChartAsSVG(file); // Export as svg
						}
					}	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		return saveAsSvgButton;
	}

	int alternatingCounter=0;
	public void drawAlternatingMarker(double position, String label, Color color){
		ValueMarker marker = new ValueMarker(position);  // position is the value on the axis
		marker.setPaint(color);
		XYPlot plot = (XYPlot) chart.getPlot();
		marker.setLabel(label);
		int numberOfMaxLines=9;
		 Rectangle2D available = new Rectangle2D.Double(chartPanel.getInsets().left, chartPanel.getInsets().top,
				 chartPanel.getSize().getWidth() - chartPanel.getInsets().left - chartPanel.getInsets().right,
				 chartPanel.getSize().getHeight() - chartPanel.getInsets().top - chartPanel.getInsets().bottom);
		double height = available.getHeight()*3/4;//chartPanel.getSize().getHeight()-chartPanel.getInsets().bottom-chartPanel.getInsets().top;
		//marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
		marker.setLabelOffset(new RectangleInsets((height/numberOfMaxLines)*(alternatingCounter)+30,10,30,10));
		marker.setLabelTextAnchor(TextAnchor.CENTER_RIGHT);
		alternatingCounter=(alternatingCounter+1)%numberOfMaxLines;
		chart.getXYPlot().addDomainMarker(marker);

	}

	public static void main(String args[]){
		JFrame window=new JFrame();
		window.setSize(new Dimension(800,500));
		JDesktopPane jd=new JDesktopPane();
		window.getContentPane().setLayout(new BorderLayout());
		window.getContentPane().add(jd,BorderLayout.CENTER);
		DynamicTimeSeriesCollection series=new DynamicTimeSeriesCollection(1,100, new Second());
		series.setTimeBase(new Second());

		series.addValue(0, 1, 10);
		series.addValue(0, 2, 10);
		series.addValue(0, 10, 100);
		series.addValue(0, 20, 100);
		series.advanceTime();
		StateWindow sw=new StateWindow("test",series);
		sw.setVisible(true);
		sw.createChart("hello", "x", "y", 0d, 200d);
		jd.add(sw);



		sw.invalidate();
		sw.start();
		
		//window.pack();
		window.setVisible(true);
		int numberMaxOfLabels=20;// it should show numberMaxOfLabels-1 labels, one per line
		for (int k=1;k<numberMaxOfLabels;k++)
			sw.drawAlternatingMarker(sw.getDataset().getEndXValue(0, sw.getDataset().getItemCount(0)-k), "uno"+k, Color.black);




	}



}
