/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

/**
 * Class to generate a frame with a JFreeChart of any type. 
 * It includes the functionality of creating a button to save the chart in SVG format.
 * @author Nuria Cuartero-Soler
 *
 */
public class JFreeChartFrameSvgSaving extends ApplicationFrame{
	
	private static final long serialVersionUID = -6119553970833031030L;
	/** The JFreeChart */
	private JFreeChart chart;
	/** The Panel that includes the JFreeChart */
	private ChartPanel chartPanel;
	private JPanel southPanel;
	
	/**
	 * Constructor of the class
	 * @param title Title of the frame
	 * @param jFreeChart JFreeChart to show in the chart
	 * @param createSaveAsSvgButton Indicates if button to save chart as SVG is shown (true) or not (false)
	 */
	public JFreeChartFrameSvgSaving(String title, JFreeChart jFreeChart, boolean createSaveAsSvgButton) {
		super(title);
		chart=jFreeChart;
	    chartPanel = new ChartPanel(chart);
	    chartPanel.setPreferredSize(new java.awt.Dimension(650, 300));
	    getContentPane().add(chartPanel);
	    southPanel = new JPanel();
	    southPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
	    southPanel.setBackground(Color.white);
	    this.add(BorderLayout.SOUTH,southPanel);
	    if(createSaveAsSvgButton){
		    southPanel.add(createSaveAsSvgJButton());
	    }
	    setDefaultCloseOperation(ApplicationFrame.HIDE_ON_CLOSE);
	    pack();
	    RefineryUtilities.centerFrameOnScreen(this);
	    setVisible(true);
	}
	
	/**
	 * Exports a JFreeChart to a SVG file.
	 * 
	 * @param chart JFreeChart to export
	 * @param bounds the dimensions of the viewport
	 * @param svgFile the output file.
	 * @throws IOException if writing the svgFile fails.
	 */
	void exportChartAsSVG(File svgFile) throws IOException {
        //Getting the area to draw
		Rectangle bounds=chartPanel.getChartRenderingInfo().getPlotInfo().getPlotArea().getBounds(); // Get plot area
		// Calculating new area with the coordinates of the content pane (getPlotArea() does not return the right coordinates) 
		Rectangle calculatedBounds=new Rectangle(this.getContentPane().getX(),this.getContentPane().getY(),bounds.width,bounds.height);
		
		// Get a DOMImplementation and create an XML document
        DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
        Document document = domImpl.createDocument(null, "svg", null);

        // Create an instance of the SVG Generator
        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

        // Draw the chart in the SVG generator
        chart.draw(svgGenerator, calculatedBounds);

        // Write svg file
        OutputStream outputStream = new FileOutputStream(svgFile);
        Writer out = new OutputStreamWriter(outputStream, "UTF-8");
        svgGenerator.stream(out, true /* use css */);						
        outputStream.flush();
        outputStream.close();
	}
	
	/**
	 * Generates a JButton with the functionality of saving the chart area as SVG file
	 * @return The JButton to save SGV files
	 */
	public JButton createSaveAsSvgJButton(){
		JButton saveAsSvgButton = new JButton("Save chart as SVG...",new ImageIcon(getClass().getResource("/images/save.gif")));
		saveAsSvgButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					final JFileChooser fc = new JFileChooser(); // Dialog to choose the file
					fc.setFileFilter(new FileNameExtensionFilter("SVG Image (*.svg)", "svg")); // Only svg files
					int returnVal=fc.showSaveDialog(null); //Show dialog
					
					if (returnVal == JFileChooser.APPROVE_OPTION) { 
			            File file = fc.getSelectedFile();
			            if (!file.getName().endsWith(".svg")) { //Check if file has the right extension
			                file = new File(file.getCanonicalPath()+".svg");
			            }
			            if (file.exists()) { // Check if the file already exists
			                if (JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(
			                        null,
			                        "Do you want to overwrite the file \""+file.getName()+"\"?","File Already Exists",
			                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
			                        null,
			                        new Object[]{"Yes","No"},  "No")) {
			                	exportChartAsSVG(file); // Export as svg
			                }
			            }else{
			            	exportChartAsSVG(file); // Export as svg
			            }
					}	
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		return saveAsSvgButton;
	}

}
