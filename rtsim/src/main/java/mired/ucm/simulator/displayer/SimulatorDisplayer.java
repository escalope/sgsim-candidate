/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.plaf.multi.MultiFileChooserUI;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.ui.RectangleAnchor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.dynamicGLM.XMLUtilities;
import mired.ucm.grid.Battery;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Generator;
import mired.ucm.monitor.Event;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.price.Tariff;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.GridLabDiscreteEventSimulator;
import mired.ucm.simulator.GridlabException;
import mired.ucm.simulator.NetworkElementsStatus;
import mired.ucm.simulator.NetworkStatus;
import mired.ucm.simulator.NetworkStatus.UnknownSensor;
import mired.ucm.simulator.GridlabActions;
import mired.ucm.simulator.SensorIDS;
import mired.ucm.simulator.clients.Client;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.SimulatorNotInitializedException;
import mired.ucm.viewer.PowerGridViewer;

/**
 * Class which displays the results of the simulation.
 * It includes all the information in a frame.
 * 
 * @author Nuria Cuartero-Soler
 *
 */
public class SimulatorDisplayer extends Thread implements NetworkListener{

	protected ReadScenario rs;
	protected GridlabActions gridlabActions;
	protected StateWindow simulationChart;
	protected StateWindow weatherChart;
	protected CtsDisplayer ctsFrame;
	protected JInternalFrame eventsFrame;
	protected int moments;
	protected int NUMBER_OF_SERIES=4;
	protected int CONSUMPTION_SERIES_INDEX = 0;
	protected int GENERATION_SERIES_INDEX = 1;
	protected int DEMAND_SERIES_INDEX = 2;
	protected int LOSSES_SERIES_INDEX = 3;
	protected final int timeUnit = Calendar.MILLISECOND;
	protected int cycleTimeInMillis;
	protected JInternalFrame pgvInternalFrame;
	protected PowerGridViewer pgv;
	protected JLabel labelTime;
	protected JLabel labelLosses;
	protected JLabel labelPeriod;
	protected JLabel labelTariff;
	protected JButton jbPause;
	protected JButton saveAsSvgButton;
	protected SimpleDateFormat sdf;
	protected DecimalFormat decimalFormat;
	protected NetworkElementsStatus nes;
	protected boolean intelligence;
	protected GridLabDiscreteEventSimulator gldes;
	protected BlockingQueue<Float> demandCurve;
	protected int hoursOfSimulation;
	protected String title;
	protected Tariff tariff;
	protected String currentTariffPeriod;
	protected double currentTariffPrice;
	protected float importedEnergy;
	protected float exportedEnergy;
	protected float energyLosses;
	protected double bill;
	protected JFrame mainFrame;
	protected boolean activeClients;
	private long realTimeCycleLengthMillis;

	/**
	 * Constructor of the class
	 * @param title Title of the frame
	 * @param startDate Date to start the simulation
	 * @param cycleTimeInMinutes Number of minutes of a cycle of simulation
	 * @param paths The PATHS variable
	 * @param intelligence If intelligence mode is ON (true) or OFF (false)
	 * @param hoursOfSimulation Number of hours to simulate
	 * @param tarif Electric tariff
	 * @param moments the number of items per series in the charts
	 * @param minRange Minimum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @param maxRange Maximum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @param activeClients If true, it is assumed that clients will work independently. If false, the simulator will 
	 * ask clients for orders to execute every cycle of simulation 
	 * @see mired.ucm.simulator.clients.Client#runCycle(Calendar simtime)
	 */
	public SimulatorDisplayer(String title,Date startDate, int cycleTimeInMillis, PATHS paths, boolean intelligence,
			int hoursOfSimulation,Tariff tarif, int moments, Double minRange, Double maxRange, boolean activeClients,
			long realTimeCycleLengthMillis) {
		this.realTimeCycleLengthMillis=realTimeCycleLengthMillis;
		init(title, startDate, cycleTimeInMillis, paths, intelligence, hoursOfSimulation, tarif, moments, minRange, maxRange, activeClients);
	}

	/**
	 * Constructor of the class.
	 * The start and stop times are read from the clock.
	 * @param title Title of the frame
	 * @param cycleTimeInMinutes Number of minutes of a cycle of simulation
	 * @param paths The PATHS variable
	 * @param intelligence If intelligence mode is ON (true) or OFF (false)
	 * @param tarif Electric tariff
	 * @param moments the number of items per series in the charts
	 * @param minRange Minimum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @param maxRange Maximum range of the y axis. If null, the axis range is automatically adjusted to fit the data.
	 * @param activeClients If true, it is assumed that clients will work independently. If false, the simulator will 
	 * ask clients for orders to execute every cycle of simulation 
	 * @see mired.ucm.simulator.clients.Client#runCycle(Calendar simtime)
	 */
	public SimulatorDisplayer(String title, int cycleTimeInMillis, PATHS paths, boolean intelligence,
			Tariff tarif, int moments, Double minRange, Double maxRange, boolean activeClients) {
		Date[] dates=readSimulationTime(paths.getNetXmlSrcFile()); // Reads simulation times from XML file
		long diffInMillies = dates[1].getTime() - dates[0].getTime();
		int hoursOfSimulation=(int) TimeUnit.HOURS.convert(diffInMillies,TimeUnit.MILLISECONDS);
		init(title, dates[0], cycleTimeInMillis, paths, intelligence, hoursOfSimulation, tarif, moments, minRange, maxRange, activeClients);
	}






	/**
	 * Inits object
	 * @param title
	 * @param startDate
	 * @param cycleTimeInMinutes
	 * @param paths
	 * @param intelligence
	 * @param hoursOfSimulation
	 * @param tarif
	 * @param moments
	 * @param minRange
	 * @param maxRange
	 * @param activeClients
	 */
	private void init(String title,Date startDate, int cycleTimeInMillis, PATHS paths, boolean intelligence,
			int hoursOfSimulation,Tariff tarif, int moments, Double minRange, Double maxRange, boolean activeClients){
		this.sdf=new SimpleDateFormat("HH:mm:ss");
		this.decimalFormat = new DecimalFormat("#.##");
		this.moments=moments;
		this.nes = new NetworkElementsStatus(); // Status of the network elements
		this.intelligence=intelligence;
		this.gridlabActions = new GridlabActions(paths);
		this.hoursOfSimulation=hoursOfSimulation;
		this.demandCurve=new ArrayBlockingQueue<Float>(100);
		this.title=title;
		this.cycleTimeInMillis=cycleTimeInMillis;
		this.importedEnergy=0;
		this.exportedEnergy=0;
		this.energyLosses=0;
		this.bill=0;
		this.tariff=tarif;
		this.mainFrame = new JFrame(title);
		this.activeClients=activeClients;
		setName("SimulatorDisplayer");

		this.rs=new ReadScenario(startDate,paths);
		if(tariff!=null){ // Getting current energy period and price
			this.currentTariffPeriod = tariff.getPeriod(startDate);
			this.currentTariffPrice = tariff.getEnergyPrice(currentTariffPeriod);
		}

		try {
			// Calculation of date to start charts since charts must show initial default data before getting simulation data
			int minutesToStartCharts = cycleTimeInMillis*moments;
			Calendar chartCalendar = Calendar.getInstance();
			chartCalendar.setTime(startDate);
			chartCalendar.add(timeUnit, -minutesToStartCharts);

			initSimulator(startDate,timeUnit,cycleTimeInMillis,paths,intelligence);
			initSimulationChart(minRange,maxRange,chartCalendar.getTime());
			initWeatherChart(chartCalendar.getTime());
			initFramePowerGridVisualizer(paths); // Before CTs frame
			initCtsDisplayer();
			initEventsDisplayer();		
		} catch (GridlabException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads simulation times from the clock of the XML file
	 * @param XMLfile
	 * @return
	 */
	private Date[] readSimulationTime(String XMLfile){
		Document doc;
		String starttime,stoptime;
		SimpleDateFormat sdf;
		Date []dates = new Date[2];
		try {
			doc = XMLUtilities.loadXMLDoc(XMLfile);
			Node clock = doc.getElementsByTagName("clock").item(0);
			if (clock.getNodeType() == Node.ELEMENT_NODE) {
				// Reading clock tags
				Element eclock=(Element)clock;
				starttime=eclock.getElementsByTagName("starttime").item(0).getTextContent();
				stoptime=eclock.getElementsByTagName("stoptime").item(0).getTextContent();

				sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //GridLAB-D date format
				dates[0]=sdf.parse(starttime);
				dates[1]=sdf.parse(stoptime);
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return dates;
	}

	/**
	 * Initialize the power grid viewer
	 * @param paths
	 */
	protected void initFramePowerGridVisualizer(PATHS paths) {
		try {
			pgv=new PowerGridViewer(paths.getNetXmlSrcFile());
			pgvInternalFrame = new JInternalFrame("PowerGrid viewer",true,true,true,true);
			pgvInternalFrame.getContentPane().setLayout(new BorderLayout());
			pgvInternalFrame.getContentPane().add(BorderLayout.CENTER,pgv);			 

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Initializes the simulation chart. 
	 * @param minRange
	 * @param maxRange
	 * @param initDate
	 */
	protected void initSimulationChart(Double minRange, Double maxRange, Date initDate) {
		// SIMULATION CHART
		float []consumo= new float[moments]; // Initial consumption data (zero by default)
		for(int i=0; i<moments; i++) {
			consumo[i]=0;
		}

		float []generacion=new float[moments]; // Initial generation data (zero by default)
		for(int i=0; i<moments; i++) {
			generacion[i]=0;
		}

		// Creating dataset and series
		MultipleOfMillisecond period = new MultipleOfMillisecond(cycleTimeInMillis,initDate); //Period to advance in charts
		MillisecondDTSC simulationChartDataset = new MillisecondDTSC(NUMBER_OF_SERIES, moments, period);
		simulationChartDataset.setTimeBase(period);
		simulationChartDataset.addSeries(consumo, CONSUMPTION_SERIES_INDEX, "Consumption Curve");
		simulationChartDataset.addSeries(consumo, GENERATION_SERIES_INDEX, "Grid Generation");
		simulationChartDataset.addSeries(generacion, DEMAND_SERIES_INDEX, "Substation Demand");
		simulationChartDataset.addSeries(generacion, LOSSES_SERIES_INDEX, "Losses");

		// Creating chart
		simulationChart=new StateWindow("System Simulation",simulationChartDataset);
		simulationChart.createChart("Simulation Results", "Time", "kW", minRange, maxRange);

		// Changing series color
		XYPlot plot = (XYPlot) simulationChart.getChart().getPlot();
		plot.getRenderer().setSeriesPaint(CONSUMPTION_SERIES_INDEX, Color.RED);
		plot.getRenderer().setSeriesPaint(GENERATION_SERIES_INDEX, new Color(34,139,34)); // Green
		plot.getRenderer().setSeriesPaint(DEMAND_SERIES_INDEX, Color.BLUE);
		plot.getRenderer().setSeriesPaint(LOSSES_SERIES_INDEX, Color.ORANGE);
		plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF)); // Changing background color


		// BOTTOM PANEL

		// Pause button
		jbPause=new JButton("Pause");
		//jbPause.setPreferredSize(new Dimension(150, jbPause.getPreferredSize().height));
		jbPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(gldes.getSimCycle().isPause()){
					gldes.getSimCycle().setPause(false);
					jbPause.setText("Pause");
				}else{
					gldes.getSimCycle().setPause(true);
					jbPause.setText("Resume");
				}				
			}
		});

		// Button to save chart as SVG
		saveAsSvgButton = simulationChart.createSaveAsSvgJButton();

		// Timestamp Label
		labelTime = new JLabel("Time: Starting simulation...");
		Border timePaddingBorder = BorderFactory.createEmptyBorder(10,10,10,10);
		Border timeLineBorder = BorderFactory.createLineBorder(Color.black);
		labelTime.setBorder(BorderFactory.createCompoundBorder(timeLineBorder,timePaddingBorder));

		// Panel Time and Buttons Pause/Resume and Save chart
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new SpringLayout());
		buttonsPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		buttonsPanel.add(labelTime);
		buttonsPanel.add(jbPause);
		buttonsPanel.add(saveAsSvgButton);
		buttonsPanel.setBackground(Color.white);
		SpringUtilities.makeCompactGrid(buttonsPanel,1,3,8, 8, 8, 0);

		// Losses Label
		labelLosses = new JLabel("Losses");
		Border lossesPaddingBorder = BorderFactory.createEmptyBorder(10,10,10,10);
		Border lossesLineBorder = BorderFactory.createLineBorder(Color.black);
		labelLosses.setBorder(BorderFactory.createCompoundBorder(lossesLineBorder,lossesPaddingBorder));

		// Period and price Label
		labelTariff=new JLabel("Period:");
		labelPeriod= new JLabel("Period");
		labelPeriod.setAlignmentY(Label.CENTER_ALIGNMENT);

		// Losses and Period Panel
		JPanel lossesPanel = new JPanel();
		lossesPanel.setLayout(new SpringLayout());
		lossesPanel.add(labelLosses);
		lossesPanel.add(labelTariff);
		lossesPanel.add(labelPeriod);
		lossesPanel.setBackground(Color.white);
		SpringUtilities.makeCompactGrid(lossesPanel,1,3,8, 8, 8, 0);

		//Panel with all the components
		JPanel auxiliaryPanel = new JPanel();
		auxiliaryPanel.setLayout(new SpringLayout());
		auxiliaryPanel.add(buttonsPanel);
		auxiliaryPanel.add(lossesPanel);
		auxiliaryPanel.setBackground(Color.white);
		SpringUtilities.makeCompactGrid(auxiliaryPanel,2,1,0,0,8,2);

		//Init components
		jbPause.setVisible(false);
		saveAsSvgButton.setVisible(false);
		labelLosses.setVisible(false);
		labelPeriod.setVisible(false);
		labelTariff.setVisible(false);
		auxiliaryPanel.setVisible(true);
		simulationChart.add(BorderLayout.SOUTH,auxiliaryPanel);
	}

	/**
	 * Initializes chart of weather conditions 
	 * @param initDate Initial date
	 */
	protected void initWeatherChart(Date initDate) {
		// Initial data from sun and wind
		float []sun=new float[moments];
		float []wind=new float[moments];

		for(int i=0; i<moments; i++) {
			sun[i]=0;
			wind[i]=90;
		}

		// Creating dataset and series
		MultipleOfMillisecond period = new MultipleOfMillisecond(cycleTimeInMillis,initDate); //Period to advance in charts
		MillisecondDTSC simulationChartDataset = new MillisecondDTSC(2, moments, period);

		final MillisecondDTSC weatherChartDataset = new MillisecondDTSC(2, moments, period);
		weatherChartDataset.setTimeBase(period);
		weatherChartDataset.addSeries(sun, 0, "Sun");
		weatherChartDataset.addSeries(wind, 1, "Wind");

		// Creating chart
		weatherChart=new StateWindow("Forecast",weatherChartDataset);
		weatherChart.createChart("Weather", "Time", "%", new Double(-10), new Double(100));

		// Changing background color
		XYPlot plot = (XYPlot) weatherChart.getChart().getPlot();
		plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
	}

	/**
	 * Initializes a JFrame with the CTs and the state of its elements
	 */
	protected void initCtsDisplayer() {
		try {
			ctsFrame = new CtsDisplayer("Grid Management");      

			for(String transformer:nes.getTransformersNames()){ // Adding transformers and their elements
				addCTToFrame(transformer);
			}

			gldes.registerListener(ctsFrame); // Register listener
			ctsFrame.invalidate();
			ctsFrame.repaint();

			Set<String> transformers=nes.getTransformersNames(); // Select transformers
			pgv.setSelected(transformers);
		} catch (SimulatorNotInitializedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add a transformation center to the events frame
	 * @param transf
	 */
	protected void addCTToFrame(String transf){
		Set<ElementEMS> ctElements = nes.getElementsByTransformer(transf); // Getting elements from transformer
		if(!ctElements.isEmpty()){
			Hashtable<ElementEMS,String> elements = new Hashtable<ElementEMS,String>();
			for(ElementEMS e:ctElements) {
				if(e instanceof Generator || e instanceof Battery){ // If generator or battery
					elements.put(e, "0"); // It is added with a initial value
				}
			}

			if(!elements.isEmpty()){
				ctsFrame.addCt(transf,elements); // If the transformer has elements, is added to the frame
			}
		}
	}

	/**
	 * Initializes a JFrame with the events that can be thrown during the simulation
	 */
	protected void initEventsDisplayer() {
		EventDisplayer eventos= new EventDisplayer(nes,gldes);
		eventsFrame = eventos.createFrame("Events");
		eventos.addElements();        
		eventsFrame.pack();
	}

	/**
	 * Initializes the simulator
	 * @param startDate Date to start simulation
	 * @param transformers Name of the CTs
	 * @param timeUnit Cycle time unit
	 * @param timeUnitAmount Cycle time amount
	 * @throws GridlabException 
	 */
	protected void initSimulator(Date startDate,int timeUnit, int timeUnitAmount, PATHS paths, boolean intelligence) throws GridlabException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		try {
			gldes=new GridLabDiscreteEventSimulator(paths,startDate,timeUnit, timeUnitAmount,nes,intelligence,rs,gridlabActions, activeClients); // Inits simulator
			gldes.registerListener(this); // Registers a listener

		}catch(SimulatorNotInitializedException ex){
			ex.printStackTrace();
		}
	}


	/**
	 * Starts the simulator and shows the frames
	 */
	public void run(){		
		// Printing if intelligence is ON or OFF
		for(String transformer:nes.getTransformersNames()){
			JTextArea jtextArea = ctsFrame.getJTextAreaOrders(transformer);
			if(jtextArea!=null){
				if(intelligence){
					jtextArea.append("MANAGEMENT ON\n");
					jtextArea.setCaretPosition(jtextArea.getDocument().getLength()-1); // Setting position at the end of the JTextArea
				}else{
					jtextArea.append("MANAGEMENT OFF\n");
					jtextArea.setCaretPosition(jtextArea.getDocument().getLength()-1); // Setting position at the end of the JTextArea
				}
			}
		}

		simulationChart.start();
		weatherChart.start();
		ctsFrame.start();
		eventsFrame.setVisible(true);
		pgvInternalFrame.setVisible(true);

		// Main frame
		JDesktopPane dp = new JDesktopPane();
		dp.add(ctsFrame);
		dp.add(eventsFrame);
		dp.add(simulationChart);      
		dp.add(weatherChart);  
		dp.add(pgvInternalFrame);

		mainFrame.addComponentListener(new ComponentListener(){

			@Override
			public void componentResized(ComponentEvent e) {
				if (e.getSource() == mainFrame) {
					pgvInternalFrame.setBounds(0, 0, mainFrame.getContentPane().getWidth() / 4, mainFrame.getContentPane().getHeight());
					eventsFrame.setBounds(mainFrame.getContentPane().getWidth()/ 4, mainFrame.getContentPane().getHeight()*2/3, mainFrame.getContentPane().getWidth()/ 4, mainFrame.getContentPane().getHeight()/3);
					ctsFrame.setBounds(mainFrame.getContentPane().getWidth()/ 4, 0, mainFrame.getContentPane().getWidth() / 4, mainFrame.getContentPane().getHeight()*2/3);
					simulationChart.setBounds(mainFrame.getContentPane().getWidth()* 2/4, 0, mainFrame.getContentPane().getWidth()*2/4, mainFrame.getContentPane().getHeight()*2/3);
					weatherChart.setBounds(mainFrame.getContentPane().getWidth()* 2/4, mainFrame.getContentPane().getHeight()*2/3, mainFrame.getContentPane().getWidth()*2/4, mainFrame.getContentPane().getHeight()/3);

					// and so on
					// also can use yourDesktopPane instead of yourFrame.getContentPane()

				}

			}

			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub

			}

		});


		mainFrame.getContentPane().add(dp);

		// Size and location of frames
		int simulationChartWidth=450;
		int pgvInternalFrameWidth=400;
		ctsFrame.setSize(ctsFrame.getSize().width+15,ctsFrame.getSize().height+15); // To leave a space to print the log
		simulationChart.setSize(simulationChartWidth,450);
		weatherChart.setSize(400,230);
		//ctsFrame.setSize(400,832);
		//eventsFrame.setSize(400,200);  
		//eventsFrame.setSize(400,ctsFrame.getSize().height-simulationChart.getSize().height); 
		//pgvInternalFrame.setSize(600,400);
		//pgvInternalFrame.setSize(400,ctsFrame.getSize().height-weatherChart.getSize().height);

		simulationChart.setLocation(0, 0);
		eventsFrame.setLocation(0,simulationChart.getSize().height+1);		

		if(!ctsFrame.getLabelComponents().isEmpty()){ // If there are controllable elements
			if(ctsFrame.getHeight()>simulationChart.getHeight()){ // Events frame is located below simulation frame
				eventsFrame.setSize(simulationChartWidth,ctsFrame.getSize().height-simulationChart.getSize().height);
				pgvInternalFrame.setSize(pgvInternalFrameWidth,ctsFrame.getSize().height-weatherChart.getSize().height);
				mainFrame.setSize(simulationChart.getSize().width+ctsFrame.getSize().width+weatherChart.getSize().width+5,ctsFrame.getSize().height+30);

				ctsFrame.setLocation(simulationChart.getSize().width+2, 0);
				weatherChart.setLocation(simulationChart.getSize().width+ctsFrame.getSize().width+2, 0);
				pgvInternalFrame.setLocation(simulationChart.getSize().width+ctsFrame.getSize().width+2, weatherChart.getSize().height+1);
			}
			else{ // Events frame is located below components frame
				eventsFrame.setSize(ctsFrame.getSize().width,simulationChart.getSize().height-ctsFrame.getSize().height);
				pgvInternalFrame.setSize(pgvInternalFrameWidth,simulationChart.getSize().height-weatherChart.getSize().height);
				mainFrame.setSize(simulationChart.getSize().width+ctsFrame.getSize().width+weatherChart.getSize().width+5,simulationChart.getSize().height+30);

				ctsFrame.setLocation(simulationChart.getSize().width+2, 0);
				weatherChart.setLocation(simulationChart.getSize().width+ctsFrame.getSize().width+2, 0);
				pgvInternalFrame.setLocation(simulationChart.getSize().width+ctsFrame.getSize().width+2, weatherChart.getSize().height+1);
				eventsFrame.setLocation(simulationChart.getSize().width+2,ctsFrame.getSize().height+1);
			}

		}else{ //If not, the ctsFrame is not shown
			eventsFrame.setSize(simulationChartWidth,200);
			pgvInternalFrame.setSize(pgvInternalFrameWidth,420);
			mainFrame.setSize(simulationChart.getSize().width+weatherChart.getSize().width+5,simulationChart.getSize().height+eventsFrame.getSize().height+30);
			weatherChart.setLocation(simulationChart.getSize().width+2, 0);
			pgvInternalFrame.setLocation(simulationChart.getSize().width+2, weatherChart.getSize().height+1);
			ctsFrame.setVisible(false);
		}

		//Layout
		mainFrame.setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.PAGE_AXIS));
		((JComponent)mainFrame.getContentPane()).setBorder(BorderFactory.createLineBorder(Color.black));	
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);

		ctsFrame.setMaximizable(true);
		ctsFrame.setResizable(true);
		simulationChart.invalidate();
		try {
			gldes.start();
			gldes.waitForSimTime(Calendar.HOUR,hoursOfSimulation);
		} catch (SimulatorNotInitializedException e) {
			e.printStackTrace();
		}

		// Simulation finished
		pgv.stopRepaintTimer();
		mainFrame.dispose();
	}

	/**
	 * Sets a ReadScenario
	 * @param rS
	 */
	public void setReadScenario(ReadScenario rS) {
		rs=rS;
	}

	/**
	 * Gets the ReadScenario
	 * @return
	 */
	public ReadScenario getReadScenario() {
		return rs;
	}

	public GridlabActions getGridlabActions() {
		return gridlabActions;
	}

	public GridLabDiscreteEventSimulator getGridLabDiscreteEventSimulator() {
		return gldes;
	}

	public String getTitle() {
		return title;
	}

	public BlockingQueue<Float> getDemandCurve() {
		return demandCurve;
	}

	public void setDemandCurve(BlockingQueue<Float> demandCurve) {
		this.demandCurve = demandCurve;
	}

	public float getImportedEnergy() {
		return importedEnergy;
	}

	public float getExportedEnergy() {
		return exportedEnergy;
	}

	public float getEnergyLosses() {
		return energyLosses;
	}

	public double getBill() {
		return bill;
	}

	public NetworkElementsStatus getNetwork() {
		return nes;
	}

	public boolean addClient(Client client) throws SimulatorNotInitializedException{
		return gldes.addClient(client);
	}

	@Override
	public void processStatus(Hashtable<SensorIDS, Float> sensorValues,
			Date timestamp) {

	}

	@Override
	public void overvoltageEvent(Date timestamp) {
		System.err.println("overvoltage "+timestamp);
		simulationChart.drawAlternatingMarker(simulationChart.getDataset().getEndXValue(0, simulationChart.getDataset().getItemCount(0)-1), "overvoltage", Color.red);

	}

	Date lastTimeStamp=null;
	Vector<Order> alreadyVisualizedOrders=new Vector<Order>();

	@Override
	public void processCycleStatus(NetworkStatus ns, NetworkElementsStatus nes, Date timestamp) {
		Date init=new Date(); // to measure cycle length
		labelTime.setText("Time: "+sdf.format(timestamp)); //Updating current time
		jbPause.setVisible(true);
		saveAsSvgButton.setVisible(true);
		if (ns.thereIsOvervoltage())
			System.err.println(ns.getOvervoltageTimestamp());
		// UPDATING SIMULATION CHART
		float[] newSimulationData = new float[NUMBER_OF_SERIES];
		newSimulationData[CONSUMPTION_SERIES_INDEX] = (float)rs.getConsumption(nes.getAvailableElements(),timestamp); // Getting consumption of energy of the next simtime
		newSimulationData[GENERATION_SERIES_INDEX] = (float) nes.calculateGenerationSum()/1000; // Getting the sum of the generation of the grid of the next simtime, in kW

		try {
			newSimulationData[DEMAND_SERIES_INDEX] = (float) ns.getSubstationSensor(SensorIDS.POWER_DEMAND)/1000; // Getting substation demand of the next simtime, in kW
			newSimulationData[LOSSES_SERIES_INDEX] = (float) ns.getSubstationSensor(SensorIDS.LOSSES)/1000; // Getting losses of the next simtime, in kW
		} catch (UnknownSensor e) {
			e.printStackTrace();
		}

		//Updating data for summary chart (substation demand)
		if(demandCurve!=null){
			demandCurve.offer(newSimulationData[DEMAND_SERIES_INDEX]);
		}


		simulationChart.getDataset().advanceTime(); // Advance to the next simtime
		

		if (lastTimeStamp!=null){
			//System.out.println(lastTimeStamp.toString()+":"+this.gldes.getSimulator().getAppliedOrders());
			Vector<Order> orders = this.gldes.getSimulator().getAppliedOrdersSince(lastTimeStamp);
			orders.removeAll(alreadyVisualizedOrders);
			alreadyVisualizedOrders.addAll(orders);
			if (!orders.isEmpty()){
				simulationChart.drawAlternatingMarker(
						simulationChart.getDataset().getEndXValue(0, simulationChart.getDataset().getItemCount(0)-1),
						orders.toString(),Color.black);
				//System.err.println(orders);
			}
		}
		simulationChart.getDataset().appendData(newSimulationData); //Append new data to the chart dataset

		// Updating system losses
		labelLosses.setVisible(true);
		labelLosses.setText("Losses: "+decimalFormat.format(newSimulationData[LOSSES_SERIES_INDEX])+" kW");

		// Calculating imported energy, exported energy and bill
		double energyOfLastCycle = (newSimulationData[DEMAND_SERIES_INDEX]*(cycleTimeInMillis/60000.0));
		if(newSimulationData[DEMAND_SERIES_INDEX]>=0){
			importedEnergy+=energyOfLastCycle;
			if(tariff!=null){
				bill+=(energyOfLastCycle*currentTariffPrice);
			}
		}else{
			exportedEnergy+=Math.abs(energyOfLastCycle);
		}

		// Updating tariff period and price
		if(tariff!=null){
			labelTariff.setVisible(true);
			labelPeriod.setVisible(true);
			currentTariffPeriod = tariff.getPeriod(timestamp);
			currentTariffPrice = tariff.getEnergyPrice(currentTariffPeriod);
			labelPeriod.setText(currentTariffPeriod+" ("+currentTariffPrice+" €/kWh)");
			labelPeriod.setForeground(tariff.getPeriodColor(currentTariffPeriod));
		}

		// Calculating energy losses
		energyLosses+=(newSimulationData[LOSSES_SERIES_INDEX]*(cycleTimeInMillis/60000.0));

		// UPDATING WEATHER CHART
		float[] newWeatherData = new float[2];
		newWeatherData[0] =  (float)rs.getSunPercentage(timestamp); // Getting the % of sun of the next simulation time
		newWeatherData[1] = (float)rs.getWindPercentage(timestamp); // Getting the % of wind of the next simulation time
		weatherChart.getDataset().advanceTime();  // Advance to the next simtime
		weatherChart.getDataset().appendData(newWeatherData); //Append new data to the chart dataset
		try {
			long millisSpent=(new Date().getTime()-init.getTime());
			if (realTimeCycleLengthMillis>millisSpent)
			Thread.currentThread().sleep(realTimeCycleLengthMillis-millisSpent);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lastTimeStamp=timestamp;
	} // End of processCycleStatus

	@Override
	public void processEvent(Event event) {

		// Add event in simulation chart
		ValueMarker marker = new ValueMarker(simulationChart.getDataset().getEndXValue(0, simulationChart.getDataset().getItemCount(0)-1));  // position is the value on the axis
		marker.setPaint(Color.red);
		marker.setLabel("Event "+event.getDeviceName());
		marker.setLabelAnchor(RectangleAnchor.BOTTOM);

		XYPlot plot = (XYPlot) simulationChart.getChart().getPlot();
		plot.addDomainMarker(marker);

		// Update PowerGridViewer
		if(event.isEnabled()) {
			pgv.removeCrossedItem(event.getDeviceName());
		}else{
			pgv.addCrossedItem(event.getDeviceName());
		}

	}


	/**
	 * Return the main frame of the simulation
	 * @return
	 */
	public JFrame getMainFrame() {
		return mainFrame;
	}

	/**
	 * Writes in client blackboard
	 * @param clientName
	 * @param message
	 */
	public void writeInClientBlackboard(String clientName, String message){
		JTextArea jTextArea = ctsFrame.getJTextAreaOrders(clientName);
		if(jTextArea!=null){
			jTextArea.append(message+"\n");
			jTextArea.setCaretPosition(jTextArea.getDocument().getLength()-1); // Setting position at the end of the JTextArea
		}
	}

	public void stopServer() throws TimeoutException{
		this.gldes.stop();
	}

}
