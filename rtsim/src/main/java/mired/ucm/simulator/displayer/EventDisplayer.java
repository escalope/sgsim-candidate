/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.simulator.displayer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.JButton;

import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.Transformer;
import mired.ucm.monitor.EventBTFalls;
import mired.ucm.monitor.EventElementFalls;
import mired.ucm.simulator.GridLabDiscreteEventSimulator;
import mired.ucm.simulator.NetworkElementsStatus;
import mired.ucm.simulator.SimulatorNotInitializedException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

/**
 * Frame of events
 * 
 * @author Sandra Garcia-Rodriguez
 * @author Nuria Cuartero-Soler
 *
 */
public class EventDisplayer implements ItemListener {
	private JInternalFrame events;
    private JPanel cards; //A panel that uses CardLayout
    private JPanel mainPanel; // For scroll
    private Hashtable <String, Vector<JButton>> buttons;
    private NetworkElementsStatus nes; // Network representation
    private GridLabDiscreteEventSimulator gldes;
    
    public EventDisplayer(NetworkElementsStatus nes,GridLabDiscreteEventSimulator gldes){
    	buttons = new Hashtable <String, Vector<JButton>>();
    	this.nes=nes;
    	this.gldes=gldes;
    }
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    protected JInternalFrame createFrame(String nombre) {
        //Create and set up the window
    	events = new JInternalFrame(nombre);
    	events.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	events.setResizable(true);
    	events.setClosable(true);
    	events.setMaximizable(true);
    	events.setIconifiable(true);
    	
        return events;
    }
    
    /**
     * Add grid elements to the event panel
     */
    public void addElements(){
        JPanel comboBoxPane = new JPanel(); //Use FlowLayout
        cards = new JPanel(new CardLayout());
		Set<String> transformers = nes.getTransformersNames();
		
		int cont=0;
		for(String trafo:transformers) { // For every transformer
			Vector<String> elementos = nes.getElementsNamesByTransformer(trafo); // Get transformer elements
			if(!elementos.isEmpty()){ // Count transformers with one or more elements connected to it
				cont++;
			}
		}
					
		String comboBoxItems[] = new String[cont];
		int i=0;
		for(String trafo:transformers) { // For every transformer				
			Vector<String> elementos = nes.getElementsNamesByTransformer(trafo); 
			if(!elementos.isEmpty()){
				comboBoxItems[i] = trafo;
				i++;
				addTransf(trafo,elementos); // Adds transformer and its elements the panel
			}
			
		}
		
        JComboBox cb = new JComboBox(comboBoxItems);
        cb.setEditable(false);
        cb.addItemListener(this);
        comboBoxPane.add(cb);
        
		mainPanel=new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));	        
		mainPanel.add(comboBoxPane, BorderLayout.PAGE_START);
		mainPanel.add(cards, BorderLayout.CENTER);
		events.getContentPane().add(new JScrollPane(mainPanel));
    }
     
    
    
    /**
     * Add a transformer to the layout
     * @param trafoName name of transformer added
     * @param elements its elements
     */
	public void addTransf(String trafoName, Vector<String> elements){
	   JPanel card = new JPanel();	 
	   Vector<JButton> jbuttons = new Vector<JButton>();
	   
	   SpringLayout layout = new SpringLayout();
	   card.setLayout(layout);
	   	   
	   JButton boton;
	   
	   // Button for disconnecting the entire transformer
	   boton = new JButton("Disconnect");
	   boton.setName(trafoName);
	   boton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {				
				JButton boton = (JButton)ae.getSource();
				Transformer trafo;
				try {
					trafo = nes.getTransformerByName(boton.getName());		
					if(!trafo.isEnabled()){
						boton.setForeground(Color.BLACK);//activate
						boton.setText("Disconnect");
						enableButtonsOfTransformer(boton.getName());
						gldes.throwNewEvent( new EventBTFalls(true,boton.getName(),null) );
					}else{
						boton.setForeground(Color.RED);//deactivate						
						boton.setText("Connect");
						disableButtonsOfTransformer(boton.getName());
						gldes.throwNewEvent( new EventBTFalls(false,boton.getName(),null) );
						
					}					
				} catch (SimulatorNotInitializedException e) {
					e.printStackTrace();
				}	
			}
		});
	   
	   card.add( new JLabel("Full transformer :"));
	   card.add(boton);
	   
	   // Buttons for the elements
	   for(String element:elements) {
		   boton = new JButton("Disconnect");
		   boton.setName(element);
		   boton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent ae) {
					JButton boton = (JButton)ae.getSource();
					ElementEMS elemento;
					
					try {
						elemento = nes.getElementByName(boton.getName());		
						if(elemento.isDisabled()){ //If element is disabled, connect
							boton.setForeground(Color.BLACK);
							boton.setText("Disconnect");
							//Throw the event
							gldes.throwNewEvent( new EventElementFalls(true,boton.getName(),null) );
							// Enable buttons of the elements that are connected to this one
							Set<String> connectedElements=nes.getElementsNamesConnectedTo(elemento.getName()); //Get elements that are connected
							Vector<JButton> trafoButtons=buttons.get(elemento.getTransformer()); // Get buttons of the transformer
							for(JButton jb:trafoButtons){
								if(connectedElements.contains(jb.getName())){ // If the button belongs to an element which is connected to the disabled element, enable the button
									jb.setEnabled(true);
								}
							}
						}else{ // If element is enabled, disconnect
							boton.setForeground(Color.RED);
		//					boton.setBackground(Color.LIGHT_GRAY);
							boton.setText("Connect");
							gldes.throwNewEvent( new EventElementFalls(false,boton.getName(),null) );
							// Disable buttons of the elements that are connected to this one
							Set<String> connectedElements=nes.getElementsNamesConnectedTo(elemento.getName()); //Get elements that are connected
							Vector<JButton> trafoButtons=buttons.get(elemento.getTransformer()); // Get buttons of the transformer
							for(JButton jb:trafoButtons){
								if(connectedElements.contains(jb.getName())){ // If the button belongs to an element which is connected to the disabled element, disable the button
									jb.setEnabled(false);
								}
							}
						}					
					} catch (SimulatorNotInitializedException e) {
						e.printStackTrace();
					}	
				}
			});				
		   		   
		   card.add( new JLabel(element+" :"));
		   card.add(boton);
		   jbuttons.add(boton);
	   }
	   
		SpringUtilities.makeCompactGrid(card,
				elements.size()+1, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
	   
        cards.add(card, trafoName);
        this.buttons.put(trafoName, jbuttons);
    }
     
	/**
	 * Notifies a change
	 * @param evt Item event
	 */
    public void itemStateChanged(ItemEvent evt) {
        CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, (String)evt.getItem());
    }
    
    /**
     * Disable all the buttons of a transformer panel
     * @param trafoName name of transformer
     */
    private void disableButtonsOfTransformer(String trafoName){
    	Vector<JButton> jbuttons = buttons.get(trafoName);
    	if(jbuttons!=null){
    		for(JButton button:jbuttons){
    			button.setEnabled(false);
    		}
    	}
    }
    
    /**
     *  Enable all the buttons of a transformer panel
     * @param trafoName name of transformer
     */
    private void enableButtonsOfTransformer(String trafoName){
    	Vector<JButton> jbuttons = buttons.get(trafoName);
    	if(jbuttons!=null){
    		for(JButton button:jbuttons){
    			button.setEnabled(true);
    		}
    	}
    }
}