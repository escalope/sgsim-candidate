/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package mired.ucm.simulator.displayer;

import java.util.Date;

import org.jfree.data.time.Hour;
import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;

/**
 * Class which represents a regular time period in minutes
 * @author Nuria Cuartero-Soler
 *
 */
public class MultipleOfMinute extends Minute{
	
	private static final long serialVersionUID = 1L;
	  private int period;

	  public MultipleOfMinute(int period){
	    super();
	    this.period = period;
	  }

	  public MultipleOfMinute(int period, int minute, int hour, int day, int month, int year){
	    super(minute, hour, day, month, year);
	    this.period = period;
	  }
	  
	  public MultipleOfMinute(int period, Date date) {
		  super(date);
		  this.period=period;
	  }
	  
	  public MultipleOfMinute(int period, int minute, Hour hour){
          super(minute, hour);
          this.period = period;
       }

	  @Override
	  public RegularTimePeriod next() {

	    RegularTimePeriod result = null;
	    if(getMinute() + period <= LAST_MINUTE_IN_HOUR){
	        result = new MultipleOfMinute( period, (int)(getMinute() + period), getHour());
	    }else{
	        Hour next = (Hour)getHour().next();
	        if(next != null){
	            result = new MultipleOfMinute(period, (int)(getMinute() + period - LAST_MINUTE_IN_HOUR - 1), next);
	        }
	    }
	    return result;

	  }

	  @Override
	  public RegularTimePeriod previous() {

	    RegularTimePeriod result = null;
	    if(getMinute() - period >= FIRST_MINUTE_IN_HOUR){
	        result = new MultipleOfMinute(period, (int)getMinute() - period, getHour());
	    }else{
	    	Hour previous = (Hour)getHour().previous();
	        if(previous != null){
	            result = new MultipleOfMinute(period, (int)(getMinute() - period + LAST_MINUTE_IN_HOUR + 1), previous);
	        }
	    }
	    return result;

	  } 

}
