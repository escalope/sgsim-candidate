/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 

package mired.ucm.simulator;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import mired.ucm.simulator.NetworkListener;
import mired.ucm.monitor.Event;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.clients.Client;

/**
 * Class that manages the simulation and its cycles
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class GridLabDiscreteEventSimulator {

	protected static final int TIMEOUT = 200000;
	protected Thread simulatorDemon;
	protected Simulator simulator; 
	protected SimulatorCycle scycle;
	protected Calendar initSimtime;
	protected PATHS paths;
	protected boolean simulationEnded;
	
	/**
	 * Constructor of the class
	 * @param paths PATHS instance
	 * @param startDate Date to start the simulation
	 * @param timeUnit Time unit of the cycle
	 * @param timeUnitAmount Time amount of the cycle
	 * @param nes  NetworkElementsStatus instance
	 * @param intelligence Indicates if intelligence is ON (true) or OFF (false)
	 * @param rs ReadScenario instance
	 * @param gridlabActions GridlabActions instance
	 * @param activeClients activeClients If true, it is assumed that clients will work independently. If false, the simulator will 
	 * ask clients for orders to execute every cycle of simulation
	 * @throws GridlabException
	 */
	public GridLabDiscreteEventSimulator(PATHS paths,Date startDate, int timeUnit, int timeUnitAmount,NetworkElementsStatus nes,boolean intelligence,ReadScenario rs,
			GridlabActions gridlabActions, boolean activeClients) throws GridlabException{
		if (simulatorDemon==null){
			this.paths=paths;
			this.simulationEnded=false;
			
			simulator=new Simulator(paths,startDate,nes,rs,gridlabActions,intelligence);
			simulatorDemon=new Thread(simulator);
			simulatorDemon.setName("Simulator");
			
			initSimtime = Calendar.getInstance();			
			initSimtime.setTime(startDate);
			
			if(activeClients){
				scycle=new SimulatorCycleActiveClients(initSimtime,simulator,timeUnit,timeUnitAmount,nes,intelligence,paths,rs, gridlabActions);
				scycle.setName("SimulatorCycle");
			}else{
				scycle=new SimulatorCycle(initSimtime,simulator,timeUnit,timeUnitAmount,nes,intelligence,paths,rs, gridlabActions);
				scycle.setName("SimulatorCycle");
			}
			
			initSimtime = Calendar.getInstance();	// to have a copy of the init date
			initSimtime.setTime(startDate);
		}	
	}
	
	/**
	 * Starts simulation
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized void start() throws SimulatorNotInitializedException{
		if(simulatorDemon!=null && scycle!=null) {
			simulatorDemon.start();
			scycle.start();
		} else {
			throw new SimulatorNotInitializedException();
		}
	}


	/**
	 * Stops simulation
	 * @throws TimeoutException
	 */
	public synchronized void stop() throws TimeoutException {
		if (simulatorDemon!=null)
		{
			//simulator.stop();
			scycle.stopDaemon();
			int timeout=0;
			
			while ((simulatorDemon.isAlive() || scycle.isAlive()) && timeout<TIMEOUT){
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				timeout=timeout+100;
			}
			
			if (timeout>=TIMEOUT) {
				throw new TimeoutException("The simulator demon did not finish within the timeout of "+TIMEOUT+" millis");
			}else{
				paths.removeTempFiles(); // Remove temporary files
				simulationEnded=true;
				System.out.println("Simulation finished");
			}
		}		 
	}

	/**
	 * Registers a new listener
	 * @param listener
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized void registerListener(NetworkListener listener) throws SimulatorNotInitializedException {
		if (scycle!=null && simulator!=null){
			scycle.registerListener(listener);
			simulator.registerListener(listener);
			
			for(Client client:simulator.getClients()){
				client.registerListener(listener);
			}
		}else {
				throw new SimulatorNotInitializedException();
		}
	}

	/**
	 * Unregisters a listener
	 * @param listener
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized void unregisterListener(NetworkListener listener) throws SimulatorNotInitializedException {
		if (scycle!=null && simulator!=null){
			scycle.unregisterListener(listener);
			simulator.unregisterListener(listener);
			
			for(Client client:simulator.getClients()){
				client.unregisterListener(listener);
			}
		}else {
				throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Gets the registered listeners
	 * @return the listeners
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized Vector<NetworkListener> getListeners() throws SimulatorNotInitializedException {
		if (scycle!=null ){
			return scycle.getListeners();
		}else {
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Adds a new client to the simulator
	 * @param client
	 * @throws SimulatorNotInitializedException 
	 */
	public synchronized boolean addClient(Client client) throws SimulatorNotInitializedException {
		if (scycle!=null && simulator!=null){
			simulator.addClient(client);
			return scycle.addClient(client);
		} else {
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Adds some clients to the simulator
	 * @param client
	 * @throws SimulatorNotInitializedException 
	 */
	public synchronized boolean addClients(Collection<Client> clients) throws SimulatorNotInitializedException {
		if (scycle!=null && simulator!=null){
			for(Client client:clients) {
				simulator.addClient(client);
				scycle.addClient(client);
			}
			return true;
		} else {
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Removes a client from the simulator
	 * @param client
	 * @throws SimulatorNotInitializedException 
	 */
	public synchronized boolean removeClient(Client client) throws SimulatorNotInitializedException {
		if (scycle!=null && simulator!=null){
			simulator.removeClient(client);
			return scycle.removeClient(client);
		} else {
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Gets the registered clients
	 * @return
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized Vector<Client> getClients() throws SimulatorNotInitializedException {
		if (scycle!=null){
			return scycle.getClients();
		}else {
			throw new SimulatorNotInitializedException();
		}
	}

	/**
	 * Waits the specified time and stops the simulator
	 * @param timeUnit
	 * @param amount
	 */
	public void waitForSimTime(int timeUnit, int amount) {
		
		try{
			while (scycle.isAlive() && timeSinceBeginning(initSimtime, scycle.getSimTime(), timeUnit)<amount){
				Thread.sleep(100); // Sleep while simulation runs
			}
			
			stop(); // Stops simulation
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Sleeps the current thread
	 * @param i Milliseconds to sleep
	 */
	public synchronized void pause(long i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Get current simulation time
	 * @return Current date
	 */
	public synchronized Date getSimTime(){
		if(scycle!=null)
			return scycle.getSimTime().getTime();
		else 
			return null;
	}
	
	/**
	 * The SimulatorCycle instance
	 * @return the simulator cycle
	 */
	public SimulatorCycle getSimCycle() {
		return scycle;
	}

	/**
	 * The Simulator instance
	 * @return the simulator
	 */
	public Simulator getSimulator() {
		return simulator;
	}

	/**
	 * Queue an event to activate in the time specified in the event
	 * @param event
	 * @return true if the event has been added successfully, false in other case
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized boolean addNewEvent(Event event) throws SimulatorNotInitializedException {
		if(scycle!=null) {
			return scycle.addNewEvent(event);
		}else{
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Throws an event in the current simulation time
	 * @param event
	 * @return true if the event has been thrown successfully, false in other case
	 * @throws SimulatorNotInitializedException
	 */
	public synchronized void throwNewEvent(Event event) throws SimulatorNotInitializedException {
		if(scycle!=null) {
			event.setTimestamp(getSimTime());
			scycle.addNewEvent(event);
		}else{
			throw new SimulatorNotInitializedException();
		}
	}
	
	/**
	 * Return the number of time units since simulation started
	 * @param initTime Start time of simulation
	 * @param simtime Current simulation time
	 * @param timeUnit Time unit
	 * @return
	 */
	public static synchronized int timeSinceBeginning(Calendar initTime, Calendar simtime, int timeUnit){
		switch (timeUnit){
		case Calendar.HOUR:
			return (int)((simtime.getTime().getTime()-initTime.getTime().getTime())/(1000*60*60));
		case Calendar.SECOND:
			return (int)((simtime.getTime().getTime()-initTime.getTime().getTime())/(1000));
		case Calendar.MINUTE:
			return (int)((simtime.getTime().getTime()-initTime.getTime().getTime())/(1000*60));		
		default:
			return (int)((simtime.getTime().getTime()-initTime.getTime().getTime())/(1000));
		}
	}
	
	public boolean isSimulationEnded() {
		return simulationEnded;
	}
}
