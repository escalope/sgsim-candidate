Instrucciones

Para compilar
mvn compile

Para ejecutar el algoritmo se escribe
mvn compile exec:java -Dexec.mainClass="mired.ucm.SmartGrid.Main"


Para ejecutar la demo de window test
mvn test -Dtest="mired.ucm.tests.SimulatorDisplayerTest"

En la demo, la representación del grid controla los elementos a mostrar. De momento, sólo muestra trafos seleccionados.

