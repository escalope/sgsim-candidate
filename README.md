Para compilar el proyecto:

mvn clean install -DskipTests=true


*** TRAINING ***

* Para ejecutar el Training:

 mvn exec:java -Dexec.mainClass="mired.ucm.training.Main"

 
*** EMAF ***

* Para ejecutar el simulador (dentro de emaf):

mvn exec:java -Dexec.mainClass="mired.ucm.emaf.launchServer"


* Para lanzar los clientes con checking (dentro de emaf):

mvn test -Dtest="mired.ucm.emaf.CheckDecisionTest"


* Para lanzar los clientes sin checking (dentro de emaf):

mvn test -Dtest="mired.ucm.emaf.RemoteClientTestForSimulation"


* Para lanzar clientes sin simulador (conexión con emulador CIRCE):

mvn exec:java -Dexec.mainClass="mired.ucm.emaf.launchClients"

mvn exec:java -Dexec.mainClass="mired.ucm.emaf.RemoteClientTest" -Dexec.classpathScope="test"