mired.ucm.viewer.PowerGridViewer es la clase principal de visualización. Extiende un JPanel, por lo que se puede usar como un componente ordinario.

Su redimensionamiento no funcionará bien. Depende de un compomente interno (JScrollPane) que no dan dimensiones por defecto para un Graph, porque este último tampoco las tiene. Por eso, se recomienda usarlo junto con un BorderLayout siendo el PowerGridViewer el elemento que ocupe el centro. 

Se incluye un compomente para hacer un zoom/unzoom de la red. 

En resources/xml/ceder.xml esta la red a representar en el main de pruebas. Ojo con cambios en ceder.xml. Este fichero se accede cuando está en el classpath. Por lo tanto, editarlo no implica que el visualizador pueda reflejar el cambio. Es necesario recompilar para que el fichero se lleve a la carpeta target/classes y quede includo en el classpath.

Se puede cambiar la representacion de cada nodo modificando el método definePowerGridNode y alterando la equivalencia entre el tipo enumerado y el tipo de shape. Las shapes válidas están en un fichero resources/shapes/shapes.xml. Son SVGs y si se quiere una idea de qué tipo de dibujo dan lugar, mirar el campo icon de cada shape. Los icon están metidos en la carpeta resources/images/. Para usarlos, hay que poner el nombre individual de cada shape. Ojo con cambios en shapes.xml. Este fichero se accede cuando está en el classpath. Por lo tanto, editarlo no implica que el visualizador pueda reflejar el cambio. Es necesario recompilar para que el fichero se lleve a la carpeta target/classes y quede includo en el classpath.

La representación gráfica se modifica cada vez que se ejecuta. No hay que esperar una distribución uniforme y sin solapamiento en todo caso.

La selección de elementos en la red dispara un evento que puede capturarse. Para ello, implementar el interfaz SelectedCellsAction y usar el addSelectionListener para agregar el listener al PowerGridViewer. Cada vez que haya una selección, se invocará el método setSelectedElements con todos los elementos seleccionados en ese momento. Los elementos se devolverán como un vector de PowerGridNode, que corresponde a un nodo de la red. Cada PowerGridNode dispone de nombre y tipo.



mvn exec:java -Dexec.mainClass="mired.ucm.viewer.PowerGridViewer" 
