/*  PowerGridGraph, a tool based on mxgraph to visualize powergrids
    Copyright (C) 2014  Jorge J. Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mired.ucm.viewer;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mxgraph.examples.swing.Stencils;
import com.mxgraph.examples.swing.editor.EditorActions.ImportAction;
import com.mxgraph.layout.*;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraph;

public class PowerGridViewer extends JPanel {

	private Hashtable<String, Object> nodes=new Hashtable<String, Object>();
	private Hashtable<String, PowerGridNode> definedPGN=new Hashtable<String,PowerGridNode>();
	private Timer mt;

	private PowerGridNode definePowerGridNode(final mxGraph graph, 
			Object parent, String name,PowerGridTypes type){
		if (!nodes.containsKey(name)){
			PowerGridNode pgn=new PowerGridNode(name,type);
			definedPGN.put(pgn.getName(),pgn);
			String shapeName="";
			switch (type){
			case CONSUMER: 
				shapeName="Cisco - Generic Building blue";//Cisco - Generic Building";
				break;
			case SOLAR_GENERATOR:
				shapeName="Renewable Energy - Solar Module";
				break;
			case BATTERY: 
				shapeName="Circuit - Vertical Powersource (European)";
				break;
			case WIND_GENERATOR:
				shapeName="Renewable Energy - Wind Turbine";
				break;
			case TRAFO:
				shapeName="electric2 - transformer";
				break;
			case CT:
				shapeName="Cisco - Telecommuter house";
				break;

			}
			//shapeName="or";
			String style="shape="+shapeName+";fillColor=#ff0000;gradientColor=#ffffff;verticalLabelPosition=top;labelBackgroundColor=white;fontSize=15;fontStyle=1;fontColor=#000000;textOpacity=100;shadow=1";

			nodes.put(name, graph.insertVertex(parent, null, pgn, 100, 100, 80, 30,style
					));
			return pgn;
		}
		return null;
	}

	private void connectPowerGridNode(final mxGraph graph,Object parent,String name,String connectedTo){
		graph.insertEdge(parent, null, "", nodes.get(name), nodes.get(connectedTo));
	}

	private mxGraph graph=null;
	private Vector<String> crossedItems=new Vector<String>();
	private Vector<String> upArrowItems=new Vector<String>();
	private Vector<String> downArrowItems=new Vector<String>();
	private Vector<String> shutdownItems=new Vector<String>();
	private Vector<String> thinkingItems=new Vector<String>();
	private mxGraphComponent graphComponent;

	private Vector<String> selectedCells=new Vector<String>();


	public void paintExtras(BasicStroke dashed, Graphics graphics) {
		graphics.setColor(Color.red);
		Graphics2D gd2=(Graphics2D)graphics;

		gd2.setStroke(dashed);				
		for (String item:crossedItems){
			Object cell=nodes.get(item);					
			Rectangle location = graph.getBoundingBox(cell).getRectangle();
			graphics.drawLine((int)location.getMinX(), (int)location.getMinY(), (int)location.getMaxX(), (int)location.getMaxY());
			graphics.drawLine((int)location.getMinX(), (int)location.getMaxY(), (int)location.getMaxX(), (int)location.getMinY());										
		}
		graphics.setColor(Color.green);

		for (String item:upArrowItems){
			Object cell=nodes.get(item);
			Rectangle location = graph.getBoundingBox(cell).getRectangle();
			graphics.drawLine((int)location.getCenterX(), (int)location.getMaxY(), (int)location.getCenterX(), (int)location.getMinY());
			graphics.drawLine((int)location.getCenterX(), (int)location.getMinY(), (int)location.getCenterX()-10, (int)location.getMinY()+10);
			graphics.drawLine((int)location.getCenterX(), (int)location.getMinY(), (int)location.getCenterX()+10, (int)location.getMinY()+10);										
		}

		graphics.setColor(Color.red);
		for (String item:downArrowItems){
			Object cell=nodes.get(item);
			Rectangle location = graph.getBoundingBox(cell).getRectangle();
			graphics.drawLine((int)location.getCenterX(), (int)location.getMaxY(), (int)location.getCenterX(), (int)location.getMinY());
			graphics.drawLine((int)location.getCenterX(), (int)location.getMaxY(), (int)location.getCenterX()-10, (int)location.getMaxY()-10);
			graphics.drawLine((int)location.getCenterX(), (int)location.getMaxY(), (int)location.getCenterX()+10, (int)location.getMaxY()-10);										
		}

		Color selectedColor=Color.black;
		Color selectedColorWithTransparency=new Color (selectedColor.getRed(), selectedColor.getGreen(), selectedColor.getBlue(), 128);
		Color borderColor=Color.yellow;
		int extraSpacePerct=10;
		for (String item:shutdownItems){
			Object cell=nodes.get(item);
			
			Rectangle location = graph.getView().getBoundingBox(new Object[]{cell}).getRectangle();
			int extraSpace=(int) ((location.getWidth()*extraSpacePerct)/100);
			graphics.setColor(selectedColorWithTransparency);
			graphics.fillRoundRect((int)location.getMinX()-extraSpace, (int)location.getMinY()-extraSpace, 
					(int)location.getWidth()+extraSpace*2, (int)location.getHeight()+extraSpace*2,20,20);
			graphics.setColor(borderColor);
			graphics.drawRoundRect((int)location.getMinX()-extraSpace, (int)location.getMinY()-extraSpace, 
					(int)location.getWidth()+extraSpace*2, (int)location.getHeight()+extraSpace*2,20,20);
		}


		for (String item:selectedCells){
			Object cell=nodes.get(item);
			Rectangle location = graph.getView().getBoundingBox(new Object[]{cell}).getRectangle();
			int extraSpace=(int) ((location.getWidth()*extraSpacePerct)/100);
			graphics.setColor(borderColor);
			graphics.drawRoundRect((int)location.getMinX()-extraSpace, (int)location.getMinY()-extraSpace, 
					(int)location.getWidth()+extraSpace*2, (int)location.getHeight()+extraSpace*2,20,20);
		}

		graphics.setColor(selectedColorWithTransparency);
		float radius=50f;
		for (String item:thinkingItems){
			Object cell=nodes.get(item);
			Rectangle location = graph.getView().getBoundingBox(new Object[]{cell}).getRectangle();
			double x2= (location.getCenterX()+radius*Math.cos(previousAngle));
			double y2=(location.getCenterY()+radius*Math.sin(previousAngle));
			graphics.drawLine((int)location.getCenterX(), (int)location.getCenterY(), (int)x2, (int)y2);			
		}

		previousAngle=previousAngle+0.2f;
		if (previousAngle>=2*3.14){
			previousAngle=0;
		}
	};

	private float previousAngle=0f;
	private Vector<Runnable> selectionListeners=new Vector<Runnable>();
	private JSlider zoom;

	public PowerGridViewer(String gridResource) throws ParserConfigurationException, SAXException, IOException, TransformerException {

		loadShapes();

		this.setLayout(new BorderLayout());

		JPanel mainPanel=this;

		graph = new mxGraph();

		final float dash1[] = {10.0f};
		final BasicStroke dashed =
				new BasicStroke(7.0f);
		graphComponent = new mxGraphComponent(graph){
			// Sets global image base path
			public mxInteractiveCanvas createCanvas()
			{
				mxInteractiveCanvas canvas = super.createCanvas();
				canvas.setImageBasePath("/images");
				return canvas;
			}

			public void paint(Graphics graphics)
			{
				super.paint(graphics);
				paintExtras(dashed, graphics);

			}

		};
		graph.addListener(mxEvent.PAINT, new mxIEventListener() {			
			@Override
			public void invoke(Object sender, mxEventObject evt) {
				SwingUtilities.invokeLater(new Runnable(){
					public void run(){				
						paintExtras(dashed, graphComponent.getGraphics());
					}
				});	

			}
		});


		graphComponent.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
				Point componenteLocation = graphComponent.getLocationOnScreen();
				mouseLocation.x=mouseLocation.x-componenteLocation.x;
				mouseLocation.y=mouseLocation.y-componenteLocation.y;
				if (e.getKeyChar()=='a'){										
					for (String item:nodes.keySet()){
						Object cell=nodes.get(item);
						Rectangle location = graph.getView().getBoundingBox(new Object[]{cell}).getRectangle();
						if (location.contains(mouseLocation) && graph.getModel().getValue(cell) instanceof PowerGridNode){
							addSelectedCells(((PowerGridNode)graph.getModel().getValue(cell)).getName());							
						}
					}

				}
				if (e.getKeyChar()=='r'){
					for (String item:nodes.keySet()){
						Object cell=nodes.get(item);
						Rectangle location = graph.getView().getBoundingBox(new Object[]{cell}).getRectangle();
						if (location.contains(mouseLocation) && graph.getModel().getValue(cell) instanceof PowerGridNode){
							removeSelectedCells(((PowerGridNode)graph.getModel().getValue(cell)).getName());
						}
					}
				}

			}




		});

		mt=new Timer();
		mt.schedule(new TimerTask() {

			@Override
			public void run() {
				graphComponent.repaint();

			}
		}, 1000, 300);

		mainPanel.add(BorderLayout.CENTER,new JScrollPane(graphComponent));
		zoom = new JSlider(JSlider.VERTICAL,0,100,50);		
		zoom.setMajorTickSpacing(10);
		zoom.setPaintTicks(true);

		JPanel eastPanel=new JPanel(new BorderLayout());
		eastPanel.add(BorderLayout.CENTER,zoom);
		eastPanel.add(BorderLayout.NORTH,new JLabel("ZOOM"));
		mainPanel.add(BorderLayout.EAST,eastPanel);
		zoom.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent e) {				
				graph.getView().setScale(zoom.getValue()/50f);				
				graph.getView().reload();
				graphComponent.repaint();			
			}
		});

		graphComponent.getViewport().setOpaque(true);
		graphComponent.getViewport().setBackground(Color.WHITE);
		
		createPowerGridFromFile(graph, graphComponent,gridResource);
		
		setScale(0.6f);

	}
	/**
	 * Use a value between 0 and 1
	 * @param scale
	 */
	public void setScale(final float scale){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().setScale(scale);				
				graph.getView().reload();
				graphComponent.repaint();		
			}
		});
		
	}

	private void createPowerGridFromFile(
			final mxGraph graph,
			mxGraphComponent graphComponent,
			String gridResource)
					throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		//Document doc = dBuilder.parse(getClass().getResourceAsStream(gridResource));
		Document doc = dBuilder.parse(new File(gridResource));

		Object parent = graph.getDefaultParent();
		graph.getModel().beginUpdate();
		try {
			NodeList ctas = doc.getElementsByTagName("nodea");
			for (int k=0;k<ctas.getLength();k++){
				definePowerGridNode(graph, parent, ctas.item(k).getFirstChild().getNodeValue(), 
						PowerGridTypes.CT);
			}
			NodeList ctbs = doc.getElementsByTagName("nodeb");
			for (int k=0;k<ctbs.getLength();k++){
				definePowerGridNode(graph, parent, ctbs.item(k).getFirstChild().getNodeValue(), 
						PowerGridTypes.CT);
			}
			NodeList trafos = doc.getElementsByTagName("transformer");
			for (int k=0;k<trafos.getLength();k++){
				String name=getSubNodeValue("name",trafos.item(k));
				String parentCT=getSubNodeValue("name",trafos.item(k).getParentNode().getParentNode());
				definePowerGridNode(graph, parent, name, 
						PowerGridTypes.TRAFO);
				connectPowerGridNode(graph,parent, name,parentCT);
			}

			NodeList loads = doc.getElementsByTagName("load");
			for (int k=0;k<loads.getLength();k++){

				String type=getSubNodeValue("type",loads.item(k));
				String name=getSubNodeValue("name",loads.item(k));
				String connectedTo=getSubNodeValue("connected_to",loads.item(k));
				definePowerGridNode(graph, parent, name, 
						PowerGridTypes.valueOf(type));
				connectPowerGridNode(graph,parent, name,connectedTo);
			}

			NodeList baterias = doc.getElementsByTagName("battery");
			for (int k=0;k<baterias.getLength();k++){

				String type=PowerGridTypes.BATTERY.toString();
				String name=getSubNodeValue("name",baterias.item(k));
				String connectedTo=getSubNodeValue("connected_to",baterias.item(k));
				definePowerGridNode(graph, parent, name, 
						PowerGridTypes.valueOf(type));
				connectPowerGridNode(graph,parent, name,connectedTo);
			}

			NodeList connectedElements = doc.getElementsByTagName("connection");
			for (int k=0;k<connectedElements.getLength();k++){
				NodeList childNodes = connectedElements.item(k).getChildNodes();
				String connectedA=getSubNodeValue("nodea",connectedElements.item(k));
				String connectedB=getSubNodeValue("nodeb",connectedElements.item(k));        		
				connectPowerGridNode(graph,parent, connectedA,connectedB);
			}



		} finally {
			graph.getModel().endUpdate();
		}

		// define layout
		mxIGraphLayout layout = new mxFastOrganicLayout(graph);

		// layout using morphing
		graph.getModel().beginUpdate();
		try {
			layout.execute(graph.getDefaultParent());
		} finally {
			mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);

			morph.addListener(mxEvent.DONE, new mxIEventListener() {

				@Override
				public void invoke(Object arg0, mxEventObject arg1) {
					graph.getModel().endUpdate();
					// fitViewport();
				}

			});

			morph.startAnimation();         
		}
	}

	private void loadShapes() throws IOException, TransformerException {
		InputStream fileStream = Stencils.class.getResourceAsStream(
				"/shapes/shapes.xml");
		String diafile=mxUtils.readInputStream(fileStream);
		Document stencilsdoc = mxXmlUtils.parseXml(diafile);

		Element shapes = (Element) stencilsdoc.getDocumentElement();
		NodeList list = shapes.getElementsByTagName("shape");

		for (int i = 0; i < list.getLength(); i++)
		{
			Element shape = (Element) list.item(i);
			String name="";
			if (shape.getAttribute("name")==null || shape.getAttribute("name").isEmpty()){
				name=getSubNodeValue("name", list.item(i));				
			}
			else
				name=shape.getAttribute("name");

			ImportAction.addStencilShape(null, xmlToString(shape), null);



		}
	}
	
	public void runSelectionListeners(){
		for (Runnable run: selectionListeners){
			run.run();
		}
	}

	public void addSelectionListener(final SelectedCellsAction al){
		selectionListeners.add(new Runnable() {			
			@Override
			public void run() {
				Vector<PowerGridNode> pgn=new Vector<PowerGridNode>(); 
				for (String selected:selectedCells){
					pgn.add(definedPGN.get(selected));
				}
				al.setSelectedElements(pgn);
			};

		});

	}

	private String xmlToString(Node shape) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(shape), new StreamResult(writer));
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		return output;
	}

	private String getSubNodeValue(String expectedName, Node item) {
		NodeList children = item.getChildNodes();
		String value="";
		for (int k=0;k<children.getLength();k++){
			if (children.item(k).getNodeName().equalsIgnoreCase(expectedName)){
				value=children.item(k).getFirstChild().getNodeValue();
			}
		}

		return value;
	}

	public void addCrossedItem(String item) {		
		crossedItems.add(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});	
	}


	public void removeCrossedItem(String item) {		
		crossedItems.remove(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});	
	}

	public void addCrossedItem(final String item, final long duringMillis) {
		Thread t=new Thread(){
			public void run(){
				addCrossedItem(item);
				try {
					Thread.currentThread().sleep(duringMillis);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				removeCrossedItem(item);				
			}
		};
		t.start();		
	}
	
	
	public void addUpArrowItem(final String item, final long duringMillis, final long blinkingTime) {
		Thread t=new Thread(){
			public void run(){
				long accumulatedTime=0;
				while (accumulatedTime<duringMillis){
				addUpArrowItem(item);
				try {
					Thread.currentThread().sleep(blinkingTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				removeUpArrowItem(item);
				try {
					Thread.currentThread().sleep(blinkingTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				accumulatedTime=accumulatedTime+blinkingTime;
				}
			}
		};
		t.start();
	}
	

	public void addUpArrowItem(final String item, final long duringMillis) {
		Thread t=new Thread(){
			public void run(){
				addUpArrowItem(item);
				try {
					Thread.currentThread().sleep(duringMillis);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				removeUpArrowItem(item);
			}
		};
		t.start();
	}

	public void removeUpArrowItem(String item) {
		upArrowItems.remove(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}

	public void addUpArrowItem(String item) {
		upArrowItems.add(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}

	private void removeThinkingItem(String item) {
		thinkingItems.remove(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});
	}

	public void addThinkingItem(String item) {
		thinkingItems.add(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});
	}


	public void addThinkingItem(final String item, final long duringMillis) {
		Thread t=new Thread(){
			public void run(){
				addThinkingItem(item);
				try {
					Thread.currentThread().sleep(duringMillis);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				removeThinkingItem(item);
			}
		};
		t.start();

	}




	public void addShutdownItem(final String item, final long duringMillis) {
		Thread t=new Thread(){
			public void run(){
				addShutdownItem(item);
				try {
					Thread.currentThread().sleep(duringMillis);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				removeShutdownItem(item);
			}
		};
		t.start();
	}
	public void removeShutdownItem(String item) {
		shutdownItems.remove(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}
	public void addShutdownItem(String item) {
		shutdownItems.add(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}

	public void addDownArrowItem(String item) {
		downArrowItems.add(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}


	public void removeDownArrowItem(String item) {
		downArrowItems.remove(item);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});

	}

	public void setSelected(Set<String> transformers) {
		for (String trafo:transformers)
			selectedCells.add(trafo);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});		
		runSelectionListeners();
	}

	public  void addSelectedCells(String name) {
		selectedCells.add(name);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});		
		runSelectionListeners();
	}


	public  void removeSelectedCells(String name) {
		selectedCells.remove(name);
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){				
				graph.getView().reload();
				graphComponent.repaint();
			}
		});
		runSelectionListeners();
	}

	public Vector<String> getSelected() {
		return new Vector<String>(selectedCells);
	}
	
	public void stopRepaintTimer(){
		mt.cancel();
	}


	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException, InterruptedException {
		JFrame f=new JFrame();

		// The powergrid definition must be in the resources folder
		PowerGridViewer t = new PowerGridViewer("/xml/ceder.xml");
		f.getContentPane().add(BorderLayout.CENTER, t);
		f.setSize(1024,700);
		f.setVisible(true);
		t.addSelectionListener(new SelectedCellsAction() {

			@Override
			public void setSelectedElements(Vector<PowerGridNode> pgn) {
				System.out.println(pgn);
			}
		});
		HashSet<String> hs=new HashSet<String>();
		hs.add("trafoCTPEPAII");
		t.setSelected(hs);
		t.addCrossedItem("trafoCTPEPAII");
		t.addUpArrowItem("CT-PEPAI");
		t.addUpArrowItem("trafoCTPEPAII", 10000, 500);
		t.addDownArrowItem("CT-PEPAII");
		t.addShutdownItem("CT-PEPAII");
		t.addShutdownItem("CT-PEPAI",2000);
		t.addThinkingItem("trafoCTPEPAII",300000);
		t.setScale(0.6f);

	}

}

