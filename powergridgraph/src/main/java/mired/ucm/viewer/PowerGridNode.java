/*  PowerGridGraph, a tool based on mxgraph to visualize powergrids
    Copyright (C) 2014  Jorge J. Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package mired.ucm.viewer;

import java.io.Serializable;

import mired.ucm.viewer.PowerGridTypes;

public class PowerGridNode implements Serializable{
	private String name;
	private PowerGridTypes type;
	public PowerGridNode(String name, PowerGridTypes type){
		this.name=name;
		this.type=type;
	}
	public String getName() {
		return name;
	}
	public PowerGridTypes getType() {
		return type;
	}
	public String toString(){
		return name;
	}
}