/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.remote.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import mired.ucm.remote.server.RemoteSGServer;

/**
 * Remote SGClient interface
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public interface RemoteSGClient extends Remote{
	/**
	 * Returns the name of the client
	 * @return the name of the client
	 * @throws RemoteException
	 */
	public String getName() throws RemoteException;
	
	/**
	 * Notifies the client to stop the server connections
	 * @throws RemoteException
	 */
	public void serverStopped() throws RemoteException;
	
	/**
	 * informs the client who to talk with
	 * @param server
	 * @throws RemoteException
	 */
	public void serverStarted(RemoteSGServer server) throws RemoteException;
	
}
