package mired.ucm.remote.client;

import java.rmi.RemoteException;

import mired.ucm.remote.server.RemoteSGServer;
import mired.ucm.remote.server.SGServer;

public abstract class SGClientImp implements SGClient {
	RemoteSGServer server;
	String host="";
	
	public SGClientImp(String host) {this.host=host;};
	
	public String getName() throws RemoteException{
		return host;
	}

	public RemoteSGServer getServer(){return server;};

	@Override
	public abstract void serverStopped() throws RemoteException;

	@Override
	public void serverStarted(RemoteSGServer server) throws RemoteException {
		this.server=server;
	}

}
