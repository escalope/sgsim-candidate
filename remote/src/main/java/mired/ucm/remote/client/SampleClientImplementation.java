/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package mired.ucm.remote.client;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import mired.ucm.remote.orders.RemoteSwitchOn;
import mired.ucm.remote.server.RemoteSGServer;
import mired.ucm.simulator.SensorContainerNotFound;
import mired.ucm.simulator.SensorIDS;

/**
 * A basic switch-on/switch-off client
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class SampleClientImplementation implements SGClient, Runnable {
	
	private static final long serialVersionUID = 2716700179415199405L;
	private static final long WaitForNextCycle = 5000;
	boolean keepSendingOrders=true;
	String clientID="";
	private RemoteSGServer server;
	private Vector<String> associatedDevices=new  Vector<String>();
	
	public SampleClientImplementation(String clientID,
			String controlledElement, 
			RemoteSGServer server) throws RemoteException, AlreadyBoundException{
		this.clientID=clientID;
		this.server=server;
		//this.server.addPlug(this);
		associatedDevices.addAll(this.server.getControllableDevices(clientID));
	}
	
	@Override
	public String getName() throws RemoteException {		
		return clientID;
	}
	
	
	public void run(){
		while (!isFinished()){		
			Hashtable<SensorIDS, Float> measurements;
			try {
				measurements = retrieveMeasurements();
				System.out.println("Client:" +this.getName()+" Measurements: "+measurements);
				decideWhatToDo(measurements);
			} catch (RemoteException e) {
				// a disconnection happened
				e.printStackTrace();
				handleDisconnection();
				
			}			
			waitForNextCycle();			
		}
	}

	private void handleDisconnection() {
		// do something to handle the disconnection
		// like restoring the server object
		
	}

	private void waitForNextCycle() {
		try {
			Thread.sleep(WaitForNextCycle);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void decideWhatToDo(Hashtable<SensorIDS, Float> measurements) throws RemoteException {		
		for (String deviceName:this.associatedDevices)
		 server.executeOrder(this.getName(), new RemoteSwitchOn(deviceName));
		
	}

	private Hashtable<SensorIDS, Float> retrieveMeasurements() throws RemoteException {
		Hashtable<SensorIDS, Float> measurements=new Hashtable<SensorIDS, Float>();
		try {
			measurements = this.server.getTransformerSensors(
					this.getName(), server.getTime());			
		} catch (SensorContainerNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return measurements;
		
	}
	
	

	@Override
	public void serverStopped() throws RemoteException {
		keepSendingOrders=false;
		
	}

	@Override
	public void serverStarted(RemoteSGServer server) throws RemoteException {
		
		
	}
	
	private boolean finished=false;
	
	public void setFinished(){
		keepSendingOrders=false;
	}

	@Override
	public boolean isFinished() {
		return !keepSendingOrders;
	}

}
