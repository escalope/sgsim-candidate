/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
package mired.ucm.remote.server;

import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import mired.ucm.remote.client.SGClient;
import mired.ucm.remote.client.RemoteSGClient;
import mired.ucm.remote.orders.RemoteOrder;
import mired.ucm.grid.ElementEMS;
import mired.ucm.grid.TypesElement;
import mired.ucm.monitor.ReadScenario;
import mired.ucm.price.Tariff;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.GridlabException;
import mired.ucm.simulator.GridlabActions;
import mired.ucm.simulator.SensorContainerNotFound;
import mired.ucm.simulator.SensorIDS;
import mired.ucm.simulator.displayer.SimulatorDisplayer;
import mired.ucm.simulator.orders.Order;

/**
 * Server that launches the simulator
 * 
 * @author Nuria Cuartero-Soler
 * @author Jorge J. Gomez-Sanz
 *
 */
public class SGServer implements RemoteSGServer {

	private static final int DEFAULT_PORT = 60010;
	private static final String DEFAULT_REMOTE_SERVER_NAME = "Server";
	private static final String DEFAULT_TITLE = "Smart Grid Simulator UCM";
	private static Registry registry;
	private int port;
	private String name;
	private Hashtable<String, RemoteSGClient> plugs;
	private final PATHS paths;
	private ReadScenario readScenario;
	private GridlabActions gridlabActions;
	private SimulatorDisplayer sd;

	/**
	 * Constructor of the class.
	 * @param title title for the simulation
	 * @param starttime start time of simulation
	 * @param netXmlFile network file
	 * @param configurationFile configuration file
	 * @param scenarioFile scenario file
	 * @param hoursOfSimulation number of hours of simulation
	 * @param cycleTimeInMinutes time cycle considered to advance in the simulation
	 * @param nMoments number of moments to show in the simulation chart
	 * @param tariff electric tariff considered
	 * @param activeClients if true, it is assumed that clients will work independently. If false, the simulator will 
	 * 						ask clients for orders to execute every cycle of simulation 
	 * @param realtimecyclelength 
	 * @throws IOException
	 * @throws AlreadyBoundException
	 * @throws GridlabException
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public SGServer(String title, Date starttime, String netXmlFile, String configurationFile, 
			String scenarioFile,int hoursOfSimulation, int cycleTimeInMinutes, int nMoments, Tariff tariff, boolean activeClients, 
			long realtimecyclelengthMillis) throws IOException, AlreadyBoundException, GridlabException, ParserConfigurationException, SAXException {
		plugs = new Hashtable<String, RemoteSGClient>();

		PATHS.debugMode=false;
		paths = new PATHS(configurationFile,netXmlFile, scenarioFile,"target/resources/");
		boolean intelligence = true;

		String stitle = DEFAULT_TITLE;
		if(title!=null && !title.isEmpty()){
			stitle+=": "+title;
		}
		sd = new SimulatorDisplayer(stitle,starttime,cycleTimeInMinutes,paths,intelligence,hoursOfSimulation,tariff, nMoments, null, null,activeClients,realtimecyclelengthMillis);
		readScenario = sd.getReadScenario();
		gridlabActions = sd.getGridlabActions();
	}

	/**
	 * Constructor of the class.
	 * @param title title for the simulation
	 * @param netXmlFile network file
	 * @param configurationFile configuration file
	 * @param scenarioFile scenario file
	 * @param cycleTimeInMinutes time cycle considered to advance in the simulation
	 * @param nMoments number of moments to show in the simulation chart
	 * @param tariff electric tariff considered
	 * @param activeClients if true, it is assumed that clients will work independently. If false, the simulator will 
	 * 						ask clients for orders to execute every cycle of simulation 
	 * @throws IOException
	 * @throws AlreadyBoundException
	 * @throws GridlabException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public SGServer(String title, String netXmlFile, String configurationFile, 
			String scenarioFile, int cycleTimeInMillis, int nMoments, Tariff tariff, boolean activeClients) throws IOException, AlreadyBoundException, GridlabException, ParserConfigurationException, SAXException {
		plugs = new Hashtable<String, RemoteSGClient>();

		PATHS.debugMode=false;
		paths = new PATHS(configurationFile,netXmlFile, scenarioFile,"target/resources/");
		boolean intelligence = true;

		String stitle = DEFAULT_TITLE;
		if(title!=null && !title.isEmpty()){
			stitle+=": "+title;
		}
		sd = new SimulatorDisplayer(stitle,cycleTimeInMillis,paths,intelligence,tariff, nMoments, null, null,activeClients);
		readScenario = sd.getReadScenario();
		gridlabActions = sd.getGridlabActions();
	}


	/**
	 * Starts the server by the default port and name reference
	 * @throws InterruptedException
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public RemoteSGServer runServer() throws InterruptedException, RemoteException, AlreadyBoundException, NotBoundException{
		return runServer(DEFAULT_REMOTE_SERVER_NAME,DEFAULT_PORT);
	}

	/**
	 * Starts the server
	 * @param name the name to associate with the remote reference
	 * @param port port on which the registry will accept requests
	 * @throws InterruptedException
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public RemoteSGServer runServer(String name, int port) throws RemoteException, InterruptedException, AlreadyBoundException, NotBoundException {
		this.port=port;
		this.name=name;
		System.setProperty("java.rmi.server.useCodebaseOnly","false");
		if(registry==null){
			try {
				registry = LocateRegistry.getRegistry(port);
			} catch (RemoteException e) {
				registry = java.rmi.registry.LocateRegistry.createRegistry(port); // Creates and exports a Registry instance				
			}
		}
		Thread.sleep(2000);
		RemoteSGServer stub = (RemoteSGServer) UnicastRemoteObject.exportObject(this, 0); // Exports remote object
		registry.bind(name, stub); // Binds a remote reference
		registry.lookup(name);
		printCopyright();
		System.out.println("Starting simulation...");
		sd.start(); // Starts simulation
		System.out.println("Server initialized");

		return stub;
	}

	/**
	 * Waits for simulation to finish and stops connections
	 * @throws InterruptedException
	 * @throws AccessException
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public void waitForSimulationToFinish() throws InterruptedException, AccessException, RemoteException, NotBoundException{
		// Wait while simulation runs
		while(isServerActive()){
			Thread.sleep(2000);
		}

		// Simulation finished
		stopConnections();
	}

	/**
	 * Stops all the connections
	 * @throws AccessException
	 * @throws RemoteException
	 * @throws NotBoundException
	 * @throws InterruptedException
	 */
	public void stopConnections() throws AccessException, RemoteException, NotBoundException, InterruptedException{
		// Stop clients connections
		for(RemoteSGClient client:plugs.values()){
			client.serverStopped();
			registry.unbind(client.getName());
			UnicastRemoteObject.unexportObject(client, true);
			Thread.sleep(500);
		}

		// Stop server connection
		registry.unbind(name);
		UnicastRemoteObject.unexportObject(this, true);
	}

	@Override
	public Set<String> getPlugList() {
		return plugs.keySet();
	}

	@Override
	public synchronized RemoteSGClient addPlug(SGClient client) throws RemoteException, AlreadyBoundException {
		this.plugs.put(client.getName(), client);
		RemoteSGClient stub = (RemoteSGClient) UnicastRemoteObject.exportObject(client, 0);
		Registry registry = LocateRegistry.getRegistry(port);
		registry.bind(client.getName(), stub);
		System.out.println("INFO: Added plug "+client.getName());
		return stub;
	}


	@Override
	public RemoteSGClient getPlug(String plugName) throws RemoteException {
		return this.plugs.get(plugName);
	}

	@Override
	public Date getTime() throws RemoteException {
		return sd.getGridLabDiscreteEventSimulator().getSimTime();
	}

	@Override
	public void executeOrder(String clientName, RemoteOrder remoteOrder) throws RemoteException{
		Order simulatorOrder = remoteOrder.update(getTime(), paths);
		sd.writeInClientBlackboard(clientName, simulatorOrder.toString());
		sd.getGridLabDiscreteEventSimulator().getSimulator().queueOrder(remoteOrder.update(getTime(), paths));
	}

	public Vector<Order> getAppliedOrdersSince(Date timestamp) {
		return sd.getGridLabDiscreteEventSimulator().getSimulator().getAppliedOrdersSince(timestamp);
	}




	@Override
	public double getSystemConsumption() throws RemoteException{
		return readScenario.getConsumption(getSimulatorDisplayer().getNetwork().getElements(),getTime());
	}

	@Override
	public Hashtable<SensorIDS, Float> getSubstationSensors()
			throws RemoteException {
		return gridlabActions.getSubstationSensors(getTime());
	}

	@Override
	public float getLossesSensor() throws RemoteException{
		return gridlabActions.getLossesSensor(getTime());
	}

	@Override
	public Hashtable<SensorIDS, Float> getTransformerSensors(String transformerName, Date timestamp) throws RemoteException, SensorContainerNotFound {
		return gridlabActions.getTransformerSensors(transformerName, timestamp);
	}

	@Override
	public Hashtable<SensorIDS, Float> getTransformerSensors(String transformerName) throws RemoteException, SensorContainerNotFound {
		return gridlabActions.getTransformerSensors(transformerName, getTime());
	}


	@Override
	public Hashtable<SensorIDS, Float> getDeviceSensors(String transformerName,String deviceName,Date timestamp) throws RemoteException {
		return gridlabActions.getDeviceSensors(deviceName, timestamp);
	}


	@Override
	public Hashtable<SensorIDS, Float> getDeviceSensors(String transformerName,String deviceName) throws RemoteException {
		return gridlabActions.getDeviceSensors(deviceName, getTime());
	}

	@Override
	public double getCurrentSun(Date currentTime){
		return readScenario.getSunPercentage(currentTime);
	}

	@Override
	public double getCurrentWind(Date currentTime){
		return readScenario.getWindPercentage(currentTime);
	}

	@Override
	public double getCurrentEnergy(String batteryName, Date currentTime) throws RemoteException {
		String energy = gridlabActions.readBatteryEnergy(batteryName, currentTime);
		if(energy==null){
			return 0;
		}else{
			return Double.parseDouble(energy);
		}
	}

	@Override
	public double getCurrentEnergy(String batteryName) throws RemoteException {
		String energy = gridlabActions.readBatteryEnergy(batteryName, getTime());
		if(energy==null){
			return 0;
		}else{
			return Double.parseDouble(energy);
		}
	}

	@Override
	public boolean isServerActive(){
		return !getSimulatorDisplayer().getGridLabDiscreteEventSimulator().isSimulationEnded();
	}

	@Override
	public boolean isSimulatorBusy(){
		return !sd.getGridLabDiscreteEventSimulator().getSimulator().getPendingOrders().isEmpty();
	}	

	/**
	 * Get the simulation displayer frame instance
	 * @return
	 */
	public SimulatorDisplayer getSimulatorDisplayer() {
		return sd;
	}

	/**
	 * Returns the server default port
	 * @return the port
	 */
	public static int getDefaultPort(){
		return DEFAULT_PORT;
	}

	/**
	 * Returns the default remote reference name of the server 
	 * @return the name
	 */
	public static String getDefaultRemoteServerName(){
		return DEFAULT_REMOTE_SERVER_NAME;
	}

	private void printCopyright(){
		System.out.println(
				"SGSim Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz\n"+
						"This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'.\n"+
						"This is free software, and you are welcome to redistribute it\n"+
				"under certain conditions; type `show c' for details.\n");
	}

	@Override
	public Hashtable<String, TypesElement> getControllableDevicesAndTypes(String transformerName) {
		Set<ElementEMS> controlledDevices = getSimulatorDisplayer().getNetwork().getControllableElementsByTransformer(transformerName);
		Hashtable<String, TypesElement> controlledDevNames=new Hashtable<String, TypesElement>();
		for(ElementEMS el:controlledDevices){
			controlledDevNames.put(el.getName(),el.getType());			
		}
		return controlledDevNames;
	}


	@Override
	public Set<String> getControllableDevices(String transformerName) {
		Set<ElementEMS> controlledDevices = getSimulatorDisplayer().getNetwork().getControllableElementsByTransformer(transformerName);
		Set<String> controlledDevNames=new HashSet<String>();
		for(ElementEMS el:controlledDevices){
			controlledDevNames.add(el.getName());
		}
		return controlledDevNames;
	}

	@Override
	public TypesElement getControllableDeviceType(String name) {
		if (getSimulatorDisplayer().getNetwork().getElementByName(name)!=null)
			return getSimulatorDisplayer().getNetwork().getElementByName(name).getType();
		if (getSimulatorDisplayer().getNetwork().getTransformerByName(name)!=null)
			return TypesElement.TRANSFORMER;
		return TypesElement.UNKNOWN;

	}
}
