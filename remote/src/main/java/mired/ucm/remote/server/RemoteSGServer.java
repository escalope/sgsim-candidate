/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.remote.server;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

import mired.ucm.grid.TypesElement;
import mired.ucm.remote.client.SGClient;
import mired.ucm.remote.client.RemoteSGClient;
import mired.ucm.remote.orders.RemoteOrder;
import mired.ucm.simulator.SensorContainerNotFound;
import mired.ucm.simulator.SensorIDS;

/**
 * Remote SGServer interface
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public interface RemoteSGServer extends Remote{
	
	/**
	 * Add a client to the list of plugs
	 * @param client
	 * @return The client registered
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 */
	public RemoteSGClient addPlug(SGClient client) throws RemoteException, AlreadyBoundException;
	
	/**
	 * Get the list of plugs
	 * @return A reference to the list of plugs
	 * @throws RemoteException
	 */
	public Set<String> getPlugList() throws RemoteException;
	
	/**
	 * Returns the plug with the given name
	 * @param plugName The name of the plug
	 * @return A reference to the remote client to work with
	 * @throws RemoteException 
	 */
	public RemoteSGClient getPlug(String plugName) throws RemoteException;
	
	/**
	 * Executes the given order
	 * @param clientName name of the client
	 * @param remoteOrder the order to execute
	 * @throws RemoteException
	 */
	public void executeOrder(String clientName, RemoteOrder remoteOrder) throws RemoteException;
	
	/**
	 * Returns the system consumption in kW
	 * @return the sum of the consumption of all the loads of the grid
	 * @throws RemoteException
	 */
	public double getSystemConsumption() throws RemoteException;

	/**
	 * Returns the sensors read by the substation meter
	 * @return a hashtable with the sensors and their values
	 * @throws RemoteException
	 */
	public Hashtable<SensorIDS,Float> getSubstationSensors() throws RemoteException;
	
	/**
	 * Returns the sensors read by the meter of the transformer with the given name
	 * @param transformerName name of the transformer
	 * @param timestamp timestamp to read the data
	 * @return a hashtable with the sensors and their values
	 * @throws RemoteException
	 * @throws SensorContainerNotFound 
	 */
	public Hashtable<SensorIDS,Float> getTransformerSensors(String transformerName, Date timestamp) throws RemoteException, SensorContainerNotFound;
	
	/**
	 * Returns the sensors read by the meter of the transformer with the given name at the current
	 * simulation time
	 * @param transformerName name of the transformer
	 * @return a hashtable with the sensors and their values
	 * @throws RemoteException
	 * @throws SensorContainerNotFound 
	 */
	public Hashtable<SensorIDS, Float> getTransformerSensors(String transformerName) throws RemoteException, SensorContainerNotFound ;
	/**
	 * Returns the sensors read by the meter of the device with the given name
	 * @param transformerName name of the transformer which the device belongs
	 * @param deviceName name of the device
	 * @param timestamp timestamp to read the data
	 * @return a hashtable with the sensors and their values
	 * @throws RemoteException
	 */
	public Hashtable<SensorIDS,Float> getDeviceSensors(String transformerName,String deviceName,Date timestamp)throws RemoteException;

	/**
	 * Returns the sensors read by the meter of the device with the given name
	 * at the current simulation time
	 * @param transformerName name of the transformer which the device belongs
	 * @param deviceName name of the device
	 * @return a hashtable with the sensors and their values
	 * @throws RemoteException
	 */
	public Hashtable<SensorIDS,Float> getDeviceSensors(String transformerName,String deviceName)throws RemoteException;
	
	
	/**
	 * Returns the list of controllable devices underneath a transformer center. 
	 * It is a controllable device if the device can receive and understand commands
	 * @param transformerName
	 * @return a list of ids of the devices to send them orders
	 */
	public Set<String> getControllableDevices(String transformerName) throws RemoteException;
	
	/**
	 * Returns the losses of the grid in W
	 * @return the sum of the distribution and transformation losses of all the grid
	 * @throws RemoteException
	 */
	public float getLossesSensor() throws RemoteException;
	
	/**
	 * Returns the % of sun at the specified time
	 * @param currentTime current time 
	 * @return the % of sun
	 * @throws RemoteException
	 */
	public double getCurrentSun(Date currentTime) throws RemoteException;
	
	/**
	 * Returns the % of wind at the specified time
	 * @param currentTime current time 
	 * @return the % of wind
	 * @throws RemoteException
	 */
	public double getCurrentWind(Date currentTime) throws RemoteException;
	
	/**
	 * Returns the level of charge of a battery at the current time
	 * @param batteryName name of the battery
	 * @return the energy of the battery in Wh
	 * @throws RemoteException
	 */
	public double getCurrentEnergy(String batteryName, Date currentTime) throws RemoteException;
	
	/**
	 * Returns the level of charge of a battery at the current simulated time
	 * @param batteryName name of the battery
	 * @return the energy of the battery in Wh
	 * @throws RemoteException
	 */
	public double getCurrentEnergy(String batteryName) throws RemoteException;
	
	/**
	 * Returns the current simulation time
	 * @return current simulation time
	 * @throws RemoteException
	 */
	public Date getTime() throws RemoteException;
	
	/**
	 * Checks if the server is active
	 * @return true if the server is active, false if not
	 * @throws RemoteException
	 */
	public boolean isServerActive() throws RemoteException;
	
	/**
	 * Checks if the simulator is busy executing orders 
	 * @return true if the simulator is busy, false if not
	 * @throws RemoteException
	 */
	public boolean isSimulatorBusy() throws RemoteException;

	/**
	 * It returns a hashtable with the list of controllable elements and their corresponding type
	 * @param transformerName
	 * @return hashtable whose keys are the devices' names and their values are the corresponding element's type
	 * @throws RemoteException
	 */
	public Hashtable<String, TypesElement> getControllableDevicesAndTypes(
			String transformerName) throws RemoteException;

	/**
	 * It returns the type associated to the controllable element whose id is provided
	 * @param name id of the device
	 * @return its type
	 */
	public TypesElement getControllableDeviceType(String name) throws RemoteException;
}

