/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.remote.orders;

import java.util.Date;

import mired.ucm.properties.PATHS;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.orders.SwitchOn;

/**
 * Class that represents an order to switch on a device
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class RemoteSwitchOn extends RemoteOrder {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of the class
	 * @param deviceName Name of the device
	 */
	public RemoteSwitchOn(String deviceName) {
		super(deviceName);
	}
	
	@Override
    public Order update(Date time, PATHS paths){
    	return new SwitchOn(getDeviceName(), time, paths);
	}
	
	@Override
	public String toString(){
		return "Switch ON "+getDeviceName();
	}
	
	@Override
	public boolean equals(Object obj){
		boolean equal=false;
		if(obj instanceof RemoteSwitchOn){
			if(((RemoteSwitchOn)obj).getDeviceName().equals(getDeviceName())){
				equal=true;
			}
		}
		
		return equal;
	}
}
