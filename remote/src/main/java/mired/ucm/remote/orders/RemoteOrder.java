/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.remote.orders;

import java.io.Serializable;
import java.util.Date;

import mired.ucm.properties.PATHS;
import mired.ucm.simulator.orders.Order;

/**
 * Class that represents an order to be sent to the server
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public abstract class RemoteOrder implements Serializable{
	
	/** Default serial*/
	private static final long serialVersionUID = 1L;
	/** The name of the device to apply the order*/
	protected String deviceName;
	
	/**
	 * Constructor of the class
	 * @param deviceName name of the device
	 */
	public RemoteOrder(String deviceName){
		this.deviceName=deviceName;
	}
	
	/**
	 * Returns the device name
	 * @return the device name
	 */
	public String getDeviceName() {
		return deviceName;
	}
	
	@Override
	public int hashCode(){
		return deviceName.hashCode();
	}

	/**
	 * Updates the remote order to send it to the simulator
	 * @param time Time to activate the order
	 * @param paths PATHS
	 * @return the generated order
	 */
	public abstract Order update(Date time, PATHS paths);
}
