/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
package mired.ucm.remote.orders;

import java.util.Date;

import mired.ucm.grid.TypesElement;
import mired.ucm.properties.PATHS;
import mired.ucm.simulator.orders.Order;
import mired.ucm.simulator.orders.SetControl;

/**
 * Class which represents the order given to a battery.
 * It needs a special type of order because in GRIDLAB-D the values for the batteries have the opposite sign  
 * (it needs a "-" to charge and a "+" to discharge).
 *
 *@author Nuria Cuartero-Soler
 */
public class RemoteBatteryOrder extends RemoteSetControl{

	private static final long serialVersionUID = -863903955815604236L;
	
	/**
	 * Constructor of the class
	 * @param deviceName Name of the device
	 * @param value Value of P (active power) to apply
	 */
	public RemoteBatteryOrder(String deviceName, double value) {
		super(deviceName, value);
	}
	
	@Override
	public Order update(Date time, PATHS paths){
	 	return new SetControl(deviceName, time, (-1)*getValueToApply(),paths,TypesElement.BATTERY);
	}
	
	@Override
	public String toString(){
		if(getValueToApply()>0){
			return "Charge battery "+getDeviceName()+" at "+getValueToApply();
		}else if(getValueToApply()<0){
			return "Discharge battery "+getDeviceName()+" at "+getValueToApply();
		}
		return "Set Control "+getValueToApply()+" to "+getDeviceName();
	}
	
	@Override
	public boolean equals(Object obj){
		boolean equal=false;
		if(obj instanceof RemoteBatteryOrder){
			if(((RemoteBatteryOrder) obj).getDeviceName().equals(getDeviceName()) && ((RemoteBatteryOrder) obj).getValueToApply()==getValueToApply()){
				equal=true;
			}
		}
		
		return equal;
	}
	
	@Override
	public int hashCode(){
		return (int) getValueToApply();
	}

}
