/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package mired.ucm.examples;

import java.io.IOException;
import java.nio.channels.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import mired.ucm.price.Tariff;
import mired.ucm.remote.server.SGServer;
import mired.ucm.simulator.GridlabException;
import junit.framework.TestCase;

/**
 * Manage server configuration
 * 
 * @author Jorge J. Gomez-Sanz
 * @author Nuria Cuartero-Soler
 *
 */
public class ManageServer {
	static int hoursOfSimulation = 10; // Hours of simulation
	static int cycleTimeInMinutes = 1; // Setting cycle minutes
	static int momentsToShow = 30; // Moments to show in the chart

	public static SGServer runServer() throws IOException,
			java.rmi.AlreadyBoundException, GridlabException,
			ParserConfigurationException, SAXException, InterruptedException,
			RemoteException, NotBoundException {
		SGServer server=null;

		Tariff tarrif = new TariffExample();
		Calendar calendar = Calendar.getInstance();
		calendar.set(2014, 4, 15, 6, 0, 0); // Set the start time of
		// simulation
		String gridFile = "src/test/resources/examples/grid.xml";
		String configFile = "src/test/resources/examples/configuration.glm";
		String scenarioFile = "src/test/resources/examples/scenario.csv";
		try {
				server = new SGServer("My simulation", calendar.getTime(), gridFile,
						configFile, scenarioFile, hoursOfSimulation,
						cycleTimeInMinutes, momentsToShow, tarrif, true,100);
				server.runServer();
		} catch (Throwable t){t.printStackTrace();}
				return server;
	}
	
	public static SGServer runServerFromClock() throws IOException,
	java.rmi.AlreadyBoundException, GridlabException,
	ParserConfigurationException, SAXException, InterruptedException,
	RemoteException, NotBoundException {
		SGServer server=null;
		
		Tariff tarrif = new TariffExample();
		// simulation
		String gridFile = "src/test/resources/examples/grid.xml";
		String configFile = "src/test/resources/examples/configuration.glm";
		String scenarioFile = "src/test/resources/examples/scenario.csv";
		try {
				server = new SGServer("My simulation", gridFile,
						configFile, scenarioFile,cycleTimeInMinutes, momentsToShow, tarrif, true);
				server.runServer();
		} catch (Throwable t){t.printStackTrace();}
				return server;
	}
	
	public static SGServer runServerFails() throws IOException,
	java.rmi.AlreadyBoundException, GridlabException,
	ParserConfigurationException, SAXException, InterruptedException,
	RemoteException, NotBoundException {
		SGServer server=null;
		
		Tariff tarrif = new TariffExample();
		Calendar calendar = Calendar.getInstance();
		calendar.set(2014, 4, 15, 6, 0, 0); // Set the start time of
		// simulation
		String gridFile = "src/test/resources/examples/wrongGrid.xml";
		String configFile = "src/test/resources/examples/configuration.glm";
		String scenarioFile = "src/test/resources/examples/scenario.csv";
		try {
				server = new SGServer("My simulation", calendar.getTime(), gridFile,
						configFile, scenarioFile, hoursOfSimulation,
						cycleTimeInMinutes, momentsToShow, tarrif, true,100);
				server.runServer();
		} catch (Throwable t){t.printStackTrace();}
				return server;
	}
	
	public static SGServer runServerOvervoltage() throws IOException,
	java.rmi.AlreadyBoundException, GridlabException,
	ParserConfigurationException, SAXException, InterruptedException,
	RemoteException, NotBoundException {
		SGServer server=null;
		
		Tariff tarrif = new TariffExample();
		Calendar calendar = Calendar.getInstance();
		calendar.set(2014, 4, 15, 6, 0, 0); // Set the start time of
		// simulation
		String gridFile = "src/test/resources/examples/overvoltageGrid.xml";
		String configFile = "src/test/resources/examples/configuration.glm";
		String scenarioFile = "src/test/resources/examples/scenario.csv";
		try {
				server = new SGServer("My simulation", calendar.getTime(), gridFile,
						configFile, scenarioFile, hoursOfSimulation,
						cycleTimeInMinutes, momentsToShow, tarrif, true,100);
				server.runServer();
		} catch (Throwable t){t.printStackTrace();}
				return server;
	}
}
