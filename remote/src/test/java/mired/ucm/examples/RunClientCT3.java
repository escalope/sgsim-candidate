/*
	This file is part of SGSim framework, a simulator for distributed smart electrical grid.

    Copyright (C) 2014 N. Cuartero-Soler, S. Garcia-Rodriguez, J.J Gomez-Sanz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mired.ucm.examples;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import mired.ucm.remote.client.SGClient;
import mired.ucm.remote.orders.RemoteBatteryOrder;
import mired.ucm.remote.orders.RemoteSwitchOn;
import mired.ucm.remote.server.SGServer;
import mired.ucm.remote.server.RemoteSGServer;
import mired.ucm.simulator.GridlabException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import junit.framework.TestCase;

/**
 * Runs a client sample
 * 
 * @author Nuria Cuartero-Soler
 * @author Jorge J. Gomez-Sanz
 *
 */
public class RunClientCT3 {
	private static Registry registry;
	private static Thread serverThread;

	@BeforeClass
	public static void setup() throws RemoteException {

		try {
			registry = LocateRegistry.createRegistry(SGServer.getDefaultPort());
		} catch (RemoteException e) {
			registry = LocateRegistry.getRegistry(SGServer.getDefaultPort());
		}
	}

	public static Registry getRegistry() {
		return registry;
	};

	@AfterClass
	public static void teardown() throws RemoteException {
		UnicastRemoteObject.unexportObject(registry, true);
	}

	private class SampleClient implements SGClient, Serializable {
		private static final long serialVersionUID = 1L;

		public SampleClient(final RemoteSGServer stub) {
		}

		// Client task

		@Override
		public void serverStopped() throws RemoteException {
		}

		@Override
		public String getName() throws RemoteException {
			return "transformerCT3";
		}

		@Override
		public void serverStarted(RemoteSGServer server) throws RemoteException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isFinished() {
			// TODO Auto-generated method stub
			return false;
		}
	};

	@Test
	public void test() throws InterruptedException {
		// Creates a client
		System.out.println("Launching client...");
		boolean failed = true;
		for (int k = 0; k < 20 && failed; k++)
			try {
				// Gets the remote server reference and registers the client
				tryLaunchingClient();
				failed = false;
			} catch (Exception e) {
				e.printStackTrace();
				try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		assertTrue("The client could not connect to the server", !failed);
		serverThread.join(ManageServer.hoursOfSimulation * 60 * 10);
	}

	private void tryLaunchingClient() throws RemoteException,
			NotBoundException, AccessException, AlreadyBoundException,
			InterruptedException {
		
		final RemoteSGServer stub = (RemoteSGServer) getRegistry().lookup(
				SGServer.getDefaultRemoteServerName());
		final SampleClient transformerCT3client = new SampleClient(stub); // after
																			// creating
																			// it,
																			// it
																			// starts
																			// delivering
																			// orders
		// stub.addPlug(transformerCT1client);
		stub.executeOrder(transformerCT3client.getName(),
				new RemoteBatteryOrder("Battery_31", 10000));// Sends an
															// order
															// to the
															// server (charge battery)
		Thread.sleep(30000);
		stub.executeOrder(transformerCT3client.getName(),
				new RemoteBatteryOrder("Battery_31", 0));
		// Wait for simulation
		while (stub.isServerActive()) {
			Thread.sleep(2000);
		}
	}

}
